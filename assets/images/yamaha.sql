-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2019 at 06:44 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yamaha`
--

-- --------------------------------------------------------

--
-- Table structure for table `sma_addresses`
--

CREATE TABLE `sma_addresses` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `line1` varchar(50) NOT NULL,
  `line2` varchar(50) DEFAULT NULL,
  `city` varchar(25) NOT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `state` varchar(25) NOT NULL,
  `country` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustments`
--

CREATE TABLE `sma_adjustments` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` text,
  `attachment` varchar(55) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `count_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_adjustment_items`
--

CREATE TABLE `sma_adjustment_items` (
  `id` int(11) NOT NULL,
  `adjustment_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_brands`
--

CREATE TABLE `sma_brands` (
  `id` int(11) NOT NULL,
  `code` varchar(20) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(50) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_calendar`
--

CREATE TABLE `sma_calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL,
  `color` varchar(7) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_captcha`
--

CREATE TABLE `sma_captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(16) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `word` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_categories`
--

CREATE TABLE `sma_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL,
  `image` varchar(55) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_categories`
--

INSERT INTO `sma_categories` (`id`, `code`, `name`, `image`, `parent_id`, `slug`, `description`) VALUES
(1, 'C1', 'Category 1', NULL, NULL, NULL, NULL),
(2, '2TB1', '2TB1', NULL, 0, '2tb1', '2TB1'),
(3, '2TB2', '2TB2', NULL, 0, '2tb2', '2TB2'),
(4, '2TB5', '2TB5', NULL, 0, '2tb5', '2TB5'),
(5, '2TB6', '2TB6', NULL, 0, '2tb6', '2TB6'),
(6, 'B831', 'B831', NULL, 0, 'b831', 'B831'),
(7, 'B832', 'B832', NULL, 0, 'b832', 'B832'),
(8, 'B835', 'B835', NULL, 0, 'b835', 'B835'),
(9, 'B836', 'B836', NULL, 0, 'b836', 'B836'),
(10, 'BC81', 'BC81', NULL, 0, 'bc81', 'BC81'),
(11, 'BC84', 'BC84', NULL, 0, 'bc84', 'BC84');

-- --------------------------------------------------------

--
-- Table structure for table `sma_combo_items`
--

CREATE TABLE `sma_combo_items` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `quantity` decimal(12,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_companies`
--

CREATE TABLE `sma_companies` (
  `id` int(11) NOT NULL,
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `group_name` varchar(20) NOT NULL,
  `customer_group_id` int(11) DEFAULT NULL,
  `customer_group_name` varchar(100) DEFAULT NULL,
  `name` varchar(55) NOT NULL,
  `company` varchar(255) NOT NULL,
  `vat_no` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(55) DEFAULT NULL,
  `state` varchar(55) DEFAULT NULL,
  `postal_code` varchar(8) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `cf1` varchar(100) DEFAULT NULL,
  `cf2` varchar(100) DEFAULT NULL,
  `cf3` varchar(100) DEFAULT NULL,
  `cf4` varchar(100) DEFAULT NULL,
  `cf5` varchar(100) DEFAULT NULL,
  `cf6` varchar(100) DEFAULT NULL,
  `invoice_footer` text,
  `payment_term` int(11) DEFAULT '0',
  `logo` varchar(255) DEFAULT 'logo.png',
  `award_points` int(11) DEFAULT '0',
  `deposit_amount` decimal(25,4) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL,
  `price_group_name` varchar(50) DEFAULT NULL,
  `gst_no` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_companies`
--

INSERT INTO `sma_companies` (`id`, `group_id`, `group_name`, `customer_group_id`, `customer_group_name`, `name`, `company`, `vat_no`, `address`, `city`, `state`, `postal_code`, `country`, `phone`, `email`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `invoice_footer`, `payment_term`, `logo`, `award_points`, `deposit_amount`, `price_group_id`, `price_group_name`, `gst_no`) VALUES
(1, 3, 'customer', 1, 'General', 'Walk-in Customer', 'Walk-in Customer', '', 'Customer Address', 'Petaling Jaya', 'Selangor', '46000', 'Malaysia', '0123456789', 'customer@tecdiary.com', '', '', '', '', '', '', NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(2, 4, 'supplier', NULL, NULL, 'Test Supplier', 'Supplier Company Name', NULL, 'Supplier Address', 'Petaling Jaya', 'Selangor', '46050', 'Malaysia', '0123456789', 'supplier@tecdiary.com', '-', '-', '-', '-', '-', '-', NULL, 0, 'logo.png', 0, NULL, NULL, NULL, NULL),
(3, NULL, 'biller', NULL, NULL, 'Mian Saleem', 'Test Biller', '5555', 'Biller adddress', 'City', '', '', 'Country', '012345678', 'saleem@tecdiary.com', '', '', '', '', '', '', ' Thank you for shopping with us. Please come again', 0, 'logo1.png', 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_costing`
--

CREATE TABLE `sma_costing` (
  `id` int(11) NOT NULL,
  `date` date NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `sale_item_id` int(11) NOT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `purchase_net_unit_cost` decimal(25,4) DEFAULT NULL,
  `purchase_unit_cost` decimal(25,4) DEFAULT NULL,
  `sale_net_unit_price` decimal(25,4) NOT NULL,
  `sale_unit_price` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT NULL,
  `inventory` tinyint(1) DEFAULT '0',
  `overselling` tinyint(1) DEFAULT '0',
  `option_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_currencies`
--

CREATE TABLE `sma_currencies` (
  `id` int(11) NOT NULL,
  `code` varchar(5) NOT NULL,
  `name` varchar(55) NOT NULL,
  `rate` decimal(12,4) NOT NULL,
  `auto_update` tinyint(1) NOT NULL DEFAULT '0',
  `symbol` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_currencies`
--

INSERT INTO `sma_currencies` (`id`, `code`, `name`, `rate`, `auto_update`, `symbol`) VALUES
(1, 'PKR', 'Pakistani Rupee', '1.0000', 0, 'Rs');

-- --------------------------------------------------------

--
-- Table structure for table `sma_customer_groups`
--

CREATE TABLE `sma_customer_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `percent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_customer_groups`
--

INSERT INTO `sma_customer_groups` (`id`, `name`, `percent`) VALUES
(1, 'General', 0),
(2, 'Reseller', -5),
(3, 'Distributor', -15),
(4, 'New Customer (+10)', 10);

-- --------------------------------------------------------

--
-- Table structure for table `sma_date_format`
--

CREATE TABLE `sma_date_format` (
  `id` int(11) NOT NULL,
  `js` varchar(20) NOT NULL,
  `php` varchar(20) NOT NULL,
  `sql` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_date_format`
--

INSERT INTO `sma_date_format` (`id`, `js`, `php`, `sql`) VALUES
(1, 'mm-dd-yyyy', 'm-d-Y', '%m-%d-%Y'),
(2, 'mm/dd/yyyy', 'm/d/Y', '%m/%d/%Y'),
(3, 'mm.dd.yyyy', 'm.d.Y', '%m.%d.%Y'),
(4, 'dd-mm-yyyy', 'd-m-Y', '%d-%m-%Y'),
(5, 'dd/mm/yyyy', 'd/m/Y', '%d/%m/%Y'),
(6, 'dd.mm.yyyy', 'd.m.Y', '%d.%m.%Y');

-- --------------------------------------------------------

--
-- Table structure for table `sma_deliveries`
--

CREATE TABLE `sma_deliveries` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) NOT NULL,
  `do_reference_no` varchar(50) NOT NULL,
  `sale_reference_no` varchar(50) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `status` varchar(15) DEFAULT NULL,
  `attachment` varchar(50) DEFAULT NULL,
  `delivered_by` varchar(50) DEFAULT NULL,
  `received_by` varchar(50) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_deposits`
--

CREATE TABLE `sma_deposits` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `company_id` int(11) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `paid_by` varchar(50) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expenses`
--

CREATE TABLE `sma_expenses` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference` varchar(50) NOT NULL,
  `amount` decimal(25,4) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `created_by` varchar(55) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_expense_categories`
--

CREATE TABLE `sma_expense_categories` (
  `id` int(11) NOT NULL,
  `code` varchar(55) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_cards`
--

CREATE TABLE `sma_gift_cards` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `card_no` varchar(20) NOT NULL,
  `value` decimal(25,4) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `balance` decimal(25,4) NOT NULL,
  `expiry` date DEFAULT NULL,
  `created_by` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_gift_card_topups`
--

CREATE TABLE `sma_gift_card_topups` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `card_id` int(11) NOT NULL,
  `amount` decimal(15,4) NOT NULL,
  `created_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_groups`
--

CREATE TABLE `sma_groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_groups`
--

INSERT INTO `sma_groups` (`id`, `name`, `description`) VALUES
(1, 'owner', 'Owner'),
(2, 'admin', 'Administrator'),
(3, 'customer', 'Customer'),
(4, 'supplier', 'Supplier'),
(5, 'sales', 'Sales Staff');

-- --------------------------------------------------------

--
-- Table structure for table `sma_login_attempts`
--

CREATE TABLE `sma_login_attempts` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_login_attempts`
--

INSERT INTO `sma_login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(1, 0x3a3a31, 'Admin', 1568816720);

-- --------------------------------------------------------

--
-- Table structure for table `sma_migrations`
--

CREATE TABLE `sma_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_migrations`
--

INSERT INTO `sma_migrations` (`version`) VALUES
(315);

-- --------------------------------------------------------

--
-- Table structure for table `sma_notifications`
--

CREATE TABLE `sma_notifications` (
  `id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_date` datetime DEFAULT NULL,
  `till_date` datetime DEFAULT NULL,
  `scope` tinyint(1) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_notifications`
--

INSERT INTO `sma_notifications` (`id`, `comment`, `date`, `from_date`, `till_date`, `scope`) VALUES
(1, '<p>Thank you for purchasing Stock Manager Advance. Please do not forget to check the documentation in help folder. If you find any error/bug, please email to support@tecdiary.com with details. You can send us your valued suggestions/feedback too.</p><p>Please rate Stock Manager Advance on your download page of codecanyon.net</p>', '2014-08-15 01:00:57', '2015-01-01 00:00:00', '2017-01-01 00:00:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `sma_order_ref`
--

CREATE TABLE `sma_order_ref` (
  `ref_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `so` int(11) NOT NULL DEFAULT '1',
  `qu` int(11) NOT NULL DEFAULT '1',
  `po` int(11) NOT NULL DEFAULT '1',
  `to` int(11) NOT NULL DEFAULT '1',
  `pos` int(11) NOT NULL DEFAULT '1',
  `do` int(11) NOT NULL DEFAULT '1',
  `pay` int(11) NOT NULL DEFAULT '1',
  `re` int(11) NOT NULL DEFAULT '1',
  `rep` int(11) NOT NULL DEFAULT '1',
  `ex` int(11) NOT NULL DEFAULT '1',
  `ppay` int(11) NOT NULL DEFAULT '1',
  `qa` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_order_ref`
--

INSERT INTO `sma_order_ref` (`ref_id`, `date`, `so`, `qu`, `po`, `to`, `pos`, `do`, `pay`, `re`, `rep`, `ex`, `ppay`, `qa`) VALUES
(1, '2015-03-01', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sma_payments`
--

CREATE TABLE `sma_payments` (
  `id` int(11) NOT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `sale_id` int(11) DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `reference_no` varchar(50) NOT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `paid_by` varchar(20) NOT NULL,
  `cheque_no` varchar(20) DEFAULT NULL,
  `cc_no` varchar(20) DEFAULT NULL,
  `cc_holder` varchar(25) DEFAULT NULL,
  `cc_month` varchar(2) DEFAULT NULL,
  `cc_year` varchar(4) DEFAULT NULL,
  `cc_type` varchar(20) DEFAULT NULL,
  `amount` decimal(25,4) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `type` varchar(20) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `pos_paid` decimal(25,4) DEFAULT '0.0000',
  `pos_balance` decimal(25,4) DEFAULT '0.0000',
  `approval_code` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_paypal`
--

CREATE TABLE `sma_paypal` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL,
  `paypal_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '2.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '3.9000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '4.4000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_paypal`
--

INSERT INTO `sma_paypal` (`id`, `active`, `account_email`, `paypal_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'mypaypal@paypal.com', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_permissions`
--

CREATE TABLE `sma_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `products-index` tinyint(1) DEFAULT '0',
  `products-add` tinyint(1) DEFAULT '0',
  `products-edit` tinyint(1) DEFAULT '0',
  `products-delete` tinyint(1) DEFAULT '0',
  `products-cost` tinyint(1) DEFAULT '0',
  `products-price` tinyint(1) DEFAULT '0',
  `quotes-index` tinyint(1) DEFAULT '0',
  `quotes-add` tinyint(1) DEFAULT '0',
  `quotes-edit` tinyint(1) DEFAULT '0',
  `quotes-pdf` tinyint(1) DEFAULT '0',
  `quotes-email` tinyint(1) DEFAULT '0',
  `quotes-delete` tinyint(1) DEFAULT '0',
  `sales-index` tinyint(1) DEFAULT '0',
  `sales-add` tinyint(1) DEFAULT '0',
  `sales-edit` tinyint(1) DEFAULT '0',
  `sales-pdf` tinyint(1) DEFAULT '0',
  `sales-email` tinyint(1) DEFAULT '0',
  `sales-delete` tinyint(1) DEFAULT '0',
  `purchases-index` tinyint(1) DEFAULT '0',
  `purchases-add` tinyint(1) DEFAULT '0',
  `purchases-edit` tinyint(1) DEFAULT '0',
  `purchases-pdf` tinyint(1) DEFAULT '0',
  `purchases-email` tinyint(1) DEFAULT '0',
  `purchases-delete` tinyint(1) DEFAULT '0',
  `transfers-index` tinyint(1) DEFAULT '0',
  `transfers-add` tinyint(1) DEFAULT '0',
  `transfers-edit` tinyint(1) DEFAULT '0',
  `transfers-pdf` tinyint(1) DEFAULT '0',
  `transfers-email` tinyint(1) DEFAULT '0',
  `transfers-delete` tinyint(1) DEFAULT '0',
  `customers-index` tinyint(1) DEFAULT '0',
  `customers-add` tinyint(1) DEFAULT '0',
  `customers-edit` tinyint(1) DEFAULT '0',
  `customers-delete` tinyint(1) DEFAULT '0',
  `suppliers-index` tinyint(1) DEFAULT '0',
  `suppliers-add` tinyint(1) DEFAULT '0',
  `suppliers-edit` tinyint(1) DEFAULT '0',
  `suppliers-delete` tinyint(1) DEFAULT '0',
  `sales-deliveries` tinyint(1) DEFAULT '0',
  `sales-add_delivery` tinyint(1) DEFAULT '0',
  `sales-edit_delivery` tinyint(1) DEFAULT '0',
  `sales-delete_delivery` tinyint(1) DEFAULT '0',
  `sales-email_delivery` tinyint(1) DEFAULT '0',
  `sales-pdf_delivery` tinyint(1) DEFAULT '0',
  `sales-gift_cards` tinyint(1) DEFAULT '0',
  `sales-add_gift_card` tinyint(1) DEFAULT '0',
  `sales-edit_gift_card` tinyint(1) DEFAULT '0',
  `sales-delete_gift_card` tinyint(1) DEFAULT '0',
  `pos-index` tinyint(1) DEFAULT '0',
  `sales-return_sales` tinyint(1) DEFAULT '0',
  `reports-index` tinyint(1) DEFAULT '0',
  `reports-warehouse_stock` tinyint(1) DEFAULT '0',
  `reports-quantity_alerts` tinyint(1) DEFAULT '0',
  `reports-expiry_alerts` tinyint(1) DEFAULT '0',
  `reports-products` tinyint(1) DEFAULT '0',
  `reports-daily_sales` tinyint(1) DEFAULT '0',
  `reports-monthly_sales` tinyint(1) DEFAULT '0',
  `reports-sales` tinyint(1) DEFAULT '0',
  `reports-payments` tinyint(1) DEFAULT '0',
  `reports-purchases` tinyint(1) DEFAULT '0',
  `reports-profit_loss` tinyint(1) DEFAULT '0',
  `reports-customers` tinyint(1) DEFAULT '0',
  `reports-suppliers` tinyint(1) DEFAULT '0',
  `reports-staff` tinyint(1) DEFAULT '0',
  `reports-register` tinyint(1) DEFAULT '0',
  `sales-payments` tinyint(1) DEFAULT '0',
  `purchases-payments` tinyint(1) DEFAULT '0',
  `purchases-expenses` tinyint(1) DEFAULT '0',
  `products-adjustments` tinyint(1) NOT NULL DEFAULT '0',
  `bulk_actions` tinyint(1) NOT NULL DEFAULT '0',
  `customers-deposits` tinyint(1) NOT NULL DEFAULT '0',
  `customers-delete_deposit` tinyint(1) NOT NULL DEFAULT '0',
  `products-barcode` tinyint(1) NOT NULL DEFAULT '0',
  `purchases-return_purchases` tinyint(1) NOT NULL DEFAULT '0',
  `reports-expenses` tinyint(1) NOT NULL DEFAULT '0',
  `reports-daily_purchases` tinyint(1) DEFAULT '0',
  `reports-monthly_purchases` tinyint(1) DEFAULT '0',
  `products-stock_count` tinyint(1) DEFAULT '0',
  `edit_price` tinyint(1) DEFAULT '0',
  `returns-index` tinyint(1) DEFAULT '0',
  `returns-add` tinyint(1) DEFAULT '0',
  `returns-edit` tinyint(1) DEFAULT '0',
  `returns-delete` tinyint(1) DEFAULT '0',
  `returns-email` tinyint(1) DEFAULT '0',
  `returns-pdf` tinyint(1) DEFAULT '0',
  `reports-tax` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_permissions`
--

INSERT INTO `sma_permissions` (`id`, `group_id`, `products-index`, `products-add`, `products-edit`, `products-delete`, `products-cost`, `products-price`, `quotes-index`, `quotes-add`, `quotes-edit`, `quotes-pdf`, `quotes-email`, `quotes-delete`, `sales-index`, `sales-add`, `sales-edit`, `sales-pdf`, `sales-email`, `sales-delete`, `purchases-index`, `purchases-add`, `purchases-edit`, `purchases-pdf`, `purchases-email`, `purchases-delete`, `transfers-index`, `transfers-add`, `transfers-edit`, `transfers-pdf`, `transfers-email`, `transfers-delete`, `customers-index`, `customers-add`, `customers-edit`, `customers-delete`, `suppliers-index`, `suppliers-add`, `suppliers-edit`, `suppliers-delete`, `sales-deliveries`, `sales-add_delivery`, `sales-edit_delivery`, `sales-delete_delivery`, `sales-email_delivery`, `sales-pdf_delivery`, `sales-gift_cards`, `sales-add_gift_card`, `sales-edit_gift_card`, `sales-delete_gift_card`, `pos-index`, `sales-return_sales`, `reports-index`, `reports-warehouse_stock`, `reports-quantity_alerts`, `reports-expiry_alerts`, `reports-products`, `reports-daily_sales`, `reports-monthly_sales`, `reports-sales`, `reports-payments`, `reports-purchases`, `reports-profit_loss`, `reports-customers`, `reports-suppliers`, `reports-staff`, `reports-register`, `sales-payments`, `purchases-payments`, `purchases-expenses`, `products-adjustments`, `bulk_actions`, `customers-deposits`, `customers-delete_deposit`, `products-barcode`, `purchases-return_purchases`, `reports-expenses`, `reports-daily_purchases`, `reports-monthly_purchases`, `products-stock_count`, `edit_price`, `returns-index`, `returns-add`, `returns-edit`, `returns-delete`, `returns-email`, `returns-pdf`, `reports-tax`) VALUES
(1, 5, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_register`
--

CREATE TABLE `sma_pos_register` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_id` int(11) NOT NULL,
  `cash_in_hand` decimal(25,4) NOT NULL,
  `status` varchar(10) NOT NULL,
  `total_cash` decimal(25,4) DEFAULT NULL,
  `total_cheques` int(11) DEFAULT NULL,
  `total_cc_slips` int(11) DEFAULT NULL,
  `total_cash_submitted` decimal(25,4) DEFAULT NULL,
  `total_cheques_submitted` int(11) DEFAULT NULL,
  `total_cc_slips_submitted` int(11) DEFAULT NULL,
  `note` text,
  `closed_at` timestamp NULL DEFAULT NULL,
  `transfer_opened_bills` varchar(50) DEFAULT NULL,
  `closed_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_pos_settings`
--

CREATE TABLE `sma_pos_settings` (
  `pos_id` int(1) NOT NULL,
  `cat_limit` int(11) NOT NULL,
  `pro_limit` int(11) NOT NULL,
  `default_category` int(11) NOT NULL,
  `default_customer` int(11) NOT NULL,
  `default_biller` int(11) NOT NULL,
  `display_time` varchar(3) NOT NULL DEFAULT 'yes',
  `cf_title1` varchar(255) DEFAULT NULL,
  `cf_title2` varchar(255) DEFAULT NULL,
  `cf_value1` varchar(255) DEFAULT NULL,
  `cf_value2` varchar(255) DEFAULT NULL,
  `receipt_printer` varchar(55) DEFAULT NULL,
  `cash_drawer_codes` varchar(55) DEFAULT NULL,
  `focus_add_item` varchar(55) DEFAULT NULL,
  `add_manual_product` varchar(55) DEFAULT NULL,
  `customer_selection` varchar(55) DEFAULT NULL,
  `add_customer` varchar(55) DEFAULT NULL,
  `toggle_category_slider` varchar(55) DEFAULT NULL,
  `toggle_subcategory_slider` varchar(55) DEFAULT NULL,
  `cancel_sale` varchar(55) DEFAULT NULL,
  `suspend_sale` varchar(55) DEFAULT NULL,
  `print_items_list` varchar(55) DEFAULT NULL,
  `finalize_sale` varchar(55) DEFAULT NULL,
  `today_sale` varchar(55) DEFAULT NULL,
  `open_hold_bills` varchar(55) DEFAULT NULL,
  `close_register` varchar(55) DEFAULT NULL,
  `keyboard` tinyint(1) NOT NULL,
  `pos_printers` varchar(255) DEFAULT NULL,
  `java_applet` tinyint(1) NOT NULL,
  `product_button_color` varchar(20) NOT NULL DEFAULT 'default',
  `tooltips` tinyint(1) DEFAULT '1',
  `paypal_pro` tinyint(1) DEFAULT '0',
  `stripe` tinyint(1) DEFAULT '0',
  `rounding` tinyint(1) DEFAULT '0',
  `char_per_line` tinyint(4) DEFAULT '42',
  `pin_code` varchar(20) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT 'purchase_code',
  `envato_username` varchar(50) DEFAULT 'envato_username',
  `version` varchar(10) DEFAULT '3.4.18',
  `after_sale_page` tinyint(1) DEFAULT '0',
  `item_order` tinyint(1) DEFAULT '0',
  `authorize` tinyint(1) DEFAULT '0',
  `toggle_brands_slider` varchar(55) DEFAULT NULL,
  `remote_printing` tinyint(1) DEFAULT '1',
  `printer` int(11) DEFAULT NULL,
  `order_printers` varchar(55) DEFAULT NULL,
  `auto_print` tinyint(1) DEFAULT '0',
  `customer_details` tinyint(1) DEFAULT NULL,
  `local_printers` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_pos_settings`
--

INSERT INTO `sma_pos_settings` (`pos_id`, `cat_limit`, `pro_limit`, `default_category`, `default_customer`, `default_biller`, `display_time`, `cf_title1`, `cf_title2`, `cf_value1`, `cf_value2`, `receipt_printer`, `cash_drawer_codes`, `focus_add_item`, `add_manual_product`, `customer_selection`, `add_customer`, `toggle_category_slider`, `toggle_subcategory_slider`, `cancel_sale`, `suspend_sale`, `print_items_list`, `finalize_sale`, `today_sale`, `open_hold_bills`, `close_register`, `keyboard`, `pos_printers`, `java_applet`, `product_button_color`, `tooltips`, `paypal_pro`, `stripe`, `rounding`, `char_per_line`, `pin_code`, `purchase_code`, `envato_username`, `version`, `after_sale_page`, `item_order`, `authorize`, `toggle_brands_slider`, `remote_printing`, `printer`, `order_printers`, `auto_print`, `customer_details`, `local_printers`) VALUES
(1, 22, 20, 1, 1, 3, '1', 'GST Reg', 'VAT Reg', '123456789', '987654321', 'BIXOLON SRP-350II', 'x1C', 'Ctrl+F3', 'Ctrl+Shift+M', 'Ctrl+Shift+C', 'Ctrl+Shift+A', 'Ctrl+F11', 'Ctrl+F12', 'F4', 'F7', 'F9', 'F8', 'Ctrl+F1', 'Ctrl+F2', 'Ctrl+F10', 1, 'BIXOLON SRP-350II, BIXOLON SRP-350II', 0, 'default', 1, 0, 0, 0, 42, NULL, 'purchase_code', 'envato_username', '3.4.18', 0, 0, 0, NULL, 1, NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_price_groups`
--

CREATE TABLE `sma_price_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_price_groups`
--

INSERT INTO `sma_price_groups` (`id`, `name`) VALUES
(1, 'Default');

-- --------------------------------------------------------

--
-- Table structure for table `sma_printers`
--

CREATE TABLE `sma_printers` (
  `id` int(11) NOT NULL,
  `title` varchar(55) NOT NULL,
  `type` varchar(25) NOT NULL,
  `profile` varchar(25) NOT NULL,
  `char_per_line` tinyint(3) UNSIGNED DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `ip_address` varbinary(45) DEFAULT NULL,
  `port` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_products`
--

CREATE TABLE `sma_products` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `unit` int(11) DEFAULT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) NOT NULL,
  `alert_quantity` decimal(15,4) DEFAULT '20.0000',
  `image` varchar(255) DEFAULT 'no_image.png',
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) DEFAULT NULL,
  `cf1` varchar(255) DEFAULT NULL,
  `cf2` varchar(255) DEFAULT NULL,
  `cf3` varchar(255) DEFAULT NULL,
  `cf4` varchar(255) DEFAULT NULL,
  `cf5` varchar(255) DEFAULT NULL,
  `cf6` varchar(255) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `tax_rate` int(11) DEFAULT NULL,
  `track_quantity` tinyint(1) DEFAULT '1',
  `details` varchar(1000) DEFAULT NULL,
  `warehouse` int(11) DEFAULT NULL,
  `barcode_symbology` varchar(55) NOT NULL DEFAULT 'code128',
  `file` varchar(100) DEFAULT NULL,
  `product_details` text,
  `tax_method` tinyint(1) DEFAULT '0',
  `type` varchar(55) NOT NULL DEFAULT 'standard',
  `supplier1` int(11) DEFAULT NULL,
  `supplier1price` decimal(25,4) DEFAULT NULL,
  `supplier2` int(11) DEFAULT NULL,
  `supplier2price` decimal(25,4) DEFAULT NULL,
  `supplier3` int(11) DEFAULT NULL,
  `supplier3price` decimal(25,4) DEFAULT NULL,
  `supplier4` int(11) DEFAULT NULL,
  `supplier4price` decimal(25,4) DEFAULT NULL,
  `supplier5` int(11) DEFAULT NULL,
  `supplier5price` decimal(25,4) DEFAULT NULL,
  `promotion` tinyint(1) DEFAULT '0',
  `promo_price` decimal(25,4) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `supplier1_part_no` varchar(50) DEFAULT NULL,
  `supplier2_part_no` varchar(50) DEFAULT NULL,
  `supplier3_part_no` varchar(50) DEFAULT NULL,
  `supplier4_part_no` varchar(50) DEFAULT NULL,
  `supplier5_part_no` varchar(50) DEFAULT NULL,
  `sale_unit` int(11) DEFAULT NULL,
  `purchase_unit` int(11) DEFAULT NULL,
  `brand` int(11) DEFAULT NULL,
  `slug` varchar(55) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `weight` decimal(10,4) DEFAULT NULL,
  `hsn_code` int(11) DEFAULT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `hide` tinyint(1) NOT NULL DEFAULT '0',
  `second_name` varchar(255) DEFAULT NULL,
  `hide_pos` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_products`
--

INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(1, '5VLE11610000', 'ABSORBER 1', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'absorber-1', NULL, NULL, 0, 0, 0, '', 0),
(2, '2XPW14150000', 'ADJUSTER SET', 1, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'adjuster-set', NULL, NULL, 0, 0, 0, '', 0),
(3, '2YBE44100000', 'AIR CLEANER ASSY.', 1, '5449.0000', '6250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'air-cleaner-assy', NULL, NULL, 0, 0, 0, '', 0),
(4, '5VLE48030000', 'AIR INDUCTION SYSTEM ASSY', 1, '2249.0000', '2580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'air-induction-system-assy', NULL, NULL, 0, 0, 0, '', 0),
(5, '5VLE21510000', 'ARM, VALVE ROCKER', 1, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'arm-valve-rocker', NULL, NULL, 0, 0, 0, '', 0),
(6, '5VLE74110000', 'AXLE, MAIN (14T)', 1, '1534.0000', '1760.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'axle-main-14t', NULL, NULL, 0, 0, 0, '', 0),
(7, '5VLF53870100', 'AXLE, SPROCKET', 1, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'axle-sprocket', NULL, NULL, 0, 0, 0, '', 0),
(8, '2TBF51810000', 'AXLE, WHEEL', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'axle-wheel', NULL, NULL, 0, 0, 0, '', 0),
(9, '2TBF53810000', 'AXLE, WHEEL', 1, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'axle-wheel', NULL, NULL, 0, 0, 0, '', 0),
(10, '935031682400', 'BALL', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'ball', NULL, NULL, 0, 0, 0, '', 0),
(11, '1PXH55460000', 'BAND', 1, '464.0000', '560.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'band', NULL, NULL, 0, 0, 0, '', 0),
(12, '2TBF811G0000', 'BAND', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'band', NULL, NULL, 0, 0, 0, '', 0),
(13, '2TBH21310000', 'BAND, BATTERY', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'band-battery', NULL, NULL, 0, 0, 0, '', 0),
(14, '4TUH39360000', 'BAND, SWITCH CORD', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'band-switch-cord', NULL, NULL, 0, 0, 0, '', 0),
(15, '5VLH39360000', 'BAND, SWITCH CORD', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'band-switch-cord', NULL, NULL, 0, 0, 0, '', 0),
(16, '5VLE85310000', 'BAR, SHIFT FORK GUIDE 1', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bar-shift-fork-guide-1', NULL, NULL, 0, 0, 0, '', 0),
(17, '5VLE85350000', 'BAR, SHIFT FORK GUIDE 2', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bar-shift-fork-guide-2', NULL, NULL, 0, 0, 0, '', 0),
(18, '2TBF53710000', 'BAR, TENSION', 1, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bar-tension', NULL, NULL, 0, 0, 0, '', 0),
(19, '93306002YU00', 'BEARING', 1, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(20, '93306003X600', 'BEARING', 1, '406.0000', '490.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(21, '93306054X300', 'BEARING', 1, '365.0000', '440.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(22, '93306201Y800', 'BEARING', 1, '489.0000', '590.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(23, '93306202XN00', 'BEARING', 1, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(24, '93306202XP00', 'BEARING', 1, '713.0000', '860.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(25, '93306203YT00', 'BEARING', 1, '356.0000', '430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(26, '93306204YD00', 'BEARING', 1, '473.0000', '570.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(27, '93306205YP00', 'BEARING', 1, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(28, '93306252XA00', 'BEARING', 1, '431.0000', '520.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(29, '93306252YY00', 'BEARING', 1, '348.0000', '420.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(30, '93306253YJ00', 'BEARING', 1, '298.0000', '360.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(31, '93310629Y300', 'BEARING', 1, '1011.0000', '1160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(32, '4S5W00480100', 'BLEED SCREW KIT', 1, '2703.0000', '3100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bleed-screw-kit', NULL, NULL, 0, 0, 0, '', 0),
(33, '2TBF835100P3', 'BODY, COWLING D', 1, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'body-cowling-d', NULL, NULL, 0, 0, 0, '', 0),
(34, '2TBF835100P0', 'BODY, COWLING A', 1, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'body-cowling-a', NULL, NULL, 0, 0, 0, '', 0),
(35, '2TBF835100P1', 'BODY, COWLING B', 1, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'body-cowling-b', NULL, NULL, 0, 0, 0, '', 0),
(36, '2TBF835100P2', 'BODY, COWLING C', 1, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'body-cowling-c', NULL, NULL, 0, 0, 0, '', 0),
(37, '901010684800', 'BOLT', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(38, '901010882000', 'BOLT', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(39, '901011081600', 'BOLT', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(40, '901090685000', 'BOLT', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(41, '901090884700', 'BOLT', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(42, '901090884800', 'BOLT', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(43, '901090886400', 'BOLT', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(44, '901230881300', 'BOLT', 1, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(45, '913120501000', 'BOLT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(46, '913120601200', 'BOLT', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(47, '913120601400', 'BOLT', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(48, '913120601600', 'BOLT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(49, '913120602000', 'BOLT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(50, '913120602500', 'BOLT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(51, '913120801600', 'BOLT', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(52, '5VLF33460100', 'BOLT 1', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-1', NULL, NULL, 0, 0, 0, '', 0),
(53, '5LKF33560100', 'BOLT 2', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-2', NULL, NULL, 0, 0, 0, '', 0),
(54, '5VLF31112000', 'BOLT, CAP', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-cap', NULL, NULL, 0, 0, 0, '', 0),
(55, '901050684200', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(56, '901050872400', 'BOLT, FLANGE', 1, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(57, '901050887400', 'BOLT, FLANGE', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(58, '90105088D800', 'BOLT, FLANGE', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(59, '90105088E400', 'BOLT, FLANGE', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(60, '90105088E500', 'BOLT, FLANGE', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(61, '90105088E600', 'BOLT, FLANGE', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(62, '901051083100', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(63, '950220601000', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(64, '950220601200', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(65, '950220601600', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(66, '950220602000', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(67, '950220602500', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(68, '950220603000', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(69, '950220603500', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(70, '950220604500', 'BOLT, FLANGE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(71, '950220605000', 'BOLT, FLANGE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(72, '950220605500', 'BOLT, FLANGE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(73, '950220802000', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(74, '950220803500', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(75, '950220804000', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(76, '958020602000', 'BOLT, FLANGE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(77, '958120802000', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(78, '958120802500', 'BOLT, FLANGE', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(79, '958121008000', 'BOLT, FLANGE', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(80, '958170601200', 'BOLT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(81, '958220601200', 'BOLT, FLANGE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(82, '958220601600', 'BOLT, FLANGE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(83, '970170801600', 'BOLT, HEXAGON', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-hexagon', NULL, NULL, 0, 0, 0, '', 0),
(84, '970220602000', 'BOLT, HEXAGON', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-hexagon', NULL, NULL, 0, 0, 0, '', 0),
(85, '3D9F589F0100', 'BOLT, LEVER', 1, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-lever', NULL, NULL, 0, 0, 0, '', 0),
(86, '913140609000', 'BOLT, SOCKET', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-socket', NULL, NULL, 0, 0, 0, '', 0),
(87, '4FPE81770000', 'BOLT, STOPPER', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-stopper', NULL, NULL, 0, 0, 0, '', 0),
(88, '904011081100', 'BOLT, UNION', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-union', NULL, NULL, 0, 0, 0, '', 0),
(89, '901190502200', 'BOLT, WITH WASHER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(90, '901190681100', 'BOLT, WITH WASHER', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(91, '901190686900', 'BOLT, WITH WASHER', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(92, '975900652200', 'BOLT, WITH WASHER', 1, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolt-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(93, '901110580300', 'BOLT,HEX. SOCKET BUTTON', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolthex-socket-button', NULL, NULL, 0, 0, 0, '', 0),
(94, '901110880500', 'BOLT,HEX. SOCKET BUTTON', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bolthex-socket-button', NULL, NULL, 0, 0, 0, '', 0),
(95, '4S5F59370000', 'BOOT 2', 1, '323.0000', '390.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'boot-2', NULL, NULL, 0, 0, 0, '', 0),
(96, '4S5F59170000', 'BOOT, CALIPER', 1, '356.0000', '430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'boot-caliper', NULL, NULL, 0, 0, 0, '', 0),
(97, '1JPE63710000', 'BOSS, CLUTCH', 1, '1221.0000', '1400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'boss-clutch', NULL, NULL, 0, 0, 0, '', 0),
(98, '2TBF177G0000', 'BOX, BATTERY', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'box-battery', NULL, NULL, 0, 0, 0, '', 0),
(99, '2TBF11590000', 'BRACKET', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket', NULL, NULL, 0, 0, 0, '', 0),
(100, '5VLH350E0000', 'BRACKET', 1, '638.0000', '770.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket', NULL, NULL, 0, 0, 0, '', 0),
(101, '5LBE42920000', 'BRACKET 2', 1, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket-2', NULL, NULL, 0, 0, 0, '', 0),
(102, '2TBF74520000', 'BRACKET, FOOTREST1', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket-footrest1', NULL, NULL, 0, 0, 0, '', 0),
(103, '2TBF74620000', 'BRACKET, FOOTREST2', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket-footrest2', NULL, NULL, 0, 0, 0, '', 0),
(104, '5VLF58670000', 'BRACKET, MASTER CYLINDER', 1, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket-master-cylinder', NULL, NULL, 0, 0, 0, '', 0),
(105, '4S5F59210100', 'BRACKET, SUPPORT', 1, '1796.0000', '2060.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket-support', NULL, NULL, 0, 0, 0, '', 0),
(106, '5VLF331X0100', 'BRACKET, UNDER', 1, '1656.0000', '1900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bracket-under', NULL, NULL, 0, 0, 0, '', 0),
(107, '4S5W00450000', 'BRAKE PAD KIT', 1, '2049.0000', '2350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'brake-pad-kit', NULL, NULL, 0, 0, 0, '', 0),
(108, '3D9W25330000', 'BRAKE SHOE KIT', 1, '2049.0000', '2350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'brake-shoe-kit', NULL, NULL, 0, 0, 0, '', 0),
(109, '4FPH18010000', 'BRUSH SET', 1, '630.0000', '760.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'brush-set', NULL, NULL, 0, 0, 0, '', 0),
(110, '4FPH35171000', 'BULB (12V-1.7W)', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bulb-12v-17w', NULL, NULL, 0, 0, 0, '', 0),
(111, '3D9H47141000', 'BULB (12V-21/5W)', 1, '448.0000', '540.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bulb-12v-215w', NULL, NULL, 0, 0, 0, '', 0),
(112, '5VLH43471000', 'BULB (12V-5W)', 1, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bulb-12v-5w', NULL, NULL, 0, 0, 0, '', 0),
(113, '3D9H33111000', 'BULB, FLASHER (12V-10W)', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bulb-flasher-12v-10w', NULL, NULL, 0, 0, 0, '', 0),
(114, '5VLH43141000', 'BULB, HEADLIGHT (12V-35/35W)', 1, '655.0000', '790.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'bulb-headlight-12v-3535w', NULL, NULL, 0, 0, 0, '', 0),
(115, '1JPH55401000', 'C.D.I. UNIT ASSY', 1, '4777.0000', '5480.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cdi-unit-assy', NULL, NULL, 0, 0, 0, '', 0),
(116, '2TBF63110000', 'CABLE, THROTTLE 1', 1, '514.0000', '620.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cable-throttle-1', NULL, NULL, 0, 0, 0, '', 0),
(117, '3D9F580U0200', 'CALIPER ASSY (RIGHT)', 1, '8368.0000', '9150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'caliper-assy-right', NULL, NULL, 0, 0, 0, '', 0),
(118, '4S5W00470000', 'CALIPER SEAL KIT', 1, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'caliper-seal-kit', NULL, NULL, 0, 0, 0, '', 0),
(119, '1SFE85410000', 'CAM,SHIFT', 1, '1334.0000', '1530.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'camshift', NULL, NULL, 0, 0, 0, '', 0),
(120, '4GUF53510200', 'CAMSHAFT', 1, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'camshaft', NULL, NULL, 0, 0, 0, '', 0),
(121, '5VLE21711000', 'CAMSHAFT 1', 1, '3104.0000', '3560.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'camshaft-1', NULL, NULL, 0, 0, 0, '', 0),
(122, '2TBF23150000', 'CAP', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cap', NULL, NULL, 0, 0, 0, '', 0),
(123, '5VLF31540000', 'CAP', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cap', NULL, NULL, 0, 0, 0, '', 0),
(124, '5VLF46101200', 'CAP ASSY', 1, '779.0000', '940.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cap-assy', NULL, NULL, 0, 0, 0, '', 0),
(125, '2D0E44120000', 'CAP, CLEANER CASE1', 1, '555.0000', '670.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cap-cleaner-case1', NULL, NULL, 0, 0, 0, '', 0),
(126, '4KLF58520000', 'CAP, RESERVOIR', 1, '348.0000', '420.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cap-reservoir', NULL, NULL, 0, 0, 0, '', 0),
(127, '2XPE43010000', 'CARBURETOR ASSY 1', 1, '10060.0000', '11000.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'carburetor-assy-1', NULL, NULL, 0, 0, 0, '', 0),
(128, '2MPH358L0000', 'CASE UNIT', 1, '1046.0000', '1200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'case-unit', NULL, NULL, 0, 0, 0, '', 0),
(129, '27SE44110000', 'CASE, AIR CLEANER1', 1, '3199.0000', '3670.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'case-air-cleaner1', NULL, NULL, 0, 0, 0, '', 0),
(130, '2TBF23110000', 'CASE, CHAIN', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'case-chain', NULL, NULL, 0, 0, 0, '', 0),
(131, '2TBF23120000', 'CASE, CHAIN 2', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'case-chain-2', NULL, NULL, 0, 0, 0, '', 0),
(132, '18CH353H0000', 'CASE, UNDER', 1, '415.0000', '500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'case-under', NULL, NULL, 0, 0, 0, '', 0),
(133, '13RH353G0000', 'CASE, UPPER', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'case-upper', NULL, NULL, 0, 0, 0, '', 0),
(134, '2TBF51680033', 'CAST WHEEL, FRONT ABC', 1, '5667.0000', '6500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cast-wheel-front-abc', NULL, NULL, 0, 0, 0, '', 0),
(135, '2TBF53380033', 'CAST WHEEL, REAR ABC', 1, '5579.0000', '6400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cast-wheel-rear-abc', NULL, NULL, 0, 0, 0, '', 0),
(136, '94568H209000', 'CHAIN', 1, '1238.0000', '1420.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'chain', NULL, NULL, 0, 0, 0, '', 0),
(137, '5VLF53C01000', 'CHAIN (428H 118L)', 1, '1003.0000', '1150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'chain-428h-118l', NULL, NULL, 0, 0, 0, '', 0),
(138, '934102080900', 'CIRCLIP', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(139, '934300582300', 'CIRCLIP', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(140, '934401010900', 'CIRCLIP', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(141, '934501681200', 'CIRCLIP', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(142, '990091540000', 'CIRCLIP', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(143, '990092050000', 'CIRCLIP', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(144, '904621680100', 'CLAMP', 1, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clamp', NULL, NULL, 0, 0, 0, '', 0),
(145, '904643582000', 'CLAMP', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clamp', NULL, NULL, 0, 0, 0, '', 0),
(146, '4S5F59250000', 'CLIP', 1, '216.0000', '260.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(147, '904670880400', 'CLIP', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(148, '904670985400', 'CLIP', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(149, '904671282800', 'CLIP', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(150, '904671381500', 'CLIP', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(151, '904671501200', 'CLIP', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(152, '904671680700', 'CLIP', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(153, '904682681300', 'CLIP', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(154, '990021060000', 'CLIP', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(155, '5VLE44240100', 'CLIP, CAP FITTING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip-cap-fitting', NULL, NULL, 0, 0, 0, '', 0),
(156, '4S5F59390000', 'CLIP, PAD', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clip-pad', NULL, NULL, 0, 0, 0, '', 0),
(157, '1JPE63000000', 'CLUTCH ASSY', 1, '14632.0000', '16000.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clutch-assy', NULL, NULL, 0, 0, 0, '', 0),
(158, '5VLF53660000', 'CLUTCH, HUB', 1, '2267.0000', '2600.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'clutch-hub', NULL, NULL, 0, 0, 0, '', 0),
(159, '3S9F73130100', 'COLLAR', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(160, '4GUF511X0100', 'COLLAR', 1, '381.0000', '460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(161, '5VLE55980000', 'COLLAR', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(162, '903870687900', 'COLLAR', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(163, '90387068S900', 'COLLAR', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(164, '90387068W200', 'COLLAR', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(165, '903870781800', 'COLLAR', 1, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(166, '903871580900', 'COLLAR', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(167, '90387158K100', 'COLLAR', 1, '282.0000', '340.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(168, '903871680400', 'COLLAR', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(169, '5VLF53860100', 'COLLAR, SPROCKET AXLE', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar-sprocket-axle', NULL, NULL, 0, 0, 0, '', 0),
(170, '5VLF53770100', 'COLLAR, WHEEL', 1, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'collar-wheel', NULL, NULL, 0, 0, 0, '', 0),
(171, '18CH43120000', 'CORD ASSY', 1, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cord-assy', NULL, NULL, 0, 0, 0, '', 0),
(172, '5VLH183G0100', 'CORD COMP.', 1, '721.0000', '870.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cord-comp', NULL, NULL, 0, 0, 0, '', 0),
(173, '2D0E44170000', 'COVER 1', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-1', NULL, NULL, 0, 0, 0, '', 0),
(174, '5LBE42930000', 'COVER 1', 1, '804.0000', '970.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-1', NULL, NULL, 0, 0, 0, '', 0),
(175, '5VLE44170000', 'COVER 1', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-1', NULL, NULL, 0, 0, 0, '', 0),
(176, '2TBE54180000', 'COVER, CHAIN CASE', 1, '933.0000', '1070.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-chain-case', NULL, NULL, 0, 0, 0, '', 0),
(177, '2TBH25990000', 'COVER, CONNECTER', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-connecter', NULL, NULL, 0, 0, 0, '', 0),
(178, '3D9H25990000', 'COVER, CONNECTER', 1, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-connecter', NULL, NULL, 0, 0, 0, '', 0),
(179, '1SFE54110000', 'COVER, CRANKCASE 1', 1, '3156.0000', '3620.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-crankcase-1', NULL, NULL, 0, 0, 0, '', 0),
(180, '5VLE54212000', 'COVER, CRANKCASE 2', 1, '5571.0000', '6390.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-crankcase-2', NULL, NULL, 0, 0, 0, '', 0),
(181, '5VLE11861000', 'COVER, CYLINDER HEAD SIDE 2', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-cylinder-head-side-2', NULL, NULL, 0, 0, 0, '', 0),
(182, '5VLE11851000', 'COVER, CYLINDER HEAD SIDE 3', 1, '2092.0000', '2400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-cylinder-head-side-3', NULL, NULL, 0, 0, 0, '', 0),
(183, '5VLF74130000', 'COVER, FOOTREST', 1, '348.0000', '420.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-footrest', NULL, NULL, 0, 0, 0, '', 0),
(184, '5VLF63720100', 'COVER, HANDLE LEVER 1', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-handle-lever-1', NULL, NULL, 0, 0, 0, '', 0),
(185, '4GUF51180000', 'COVER, HUB DUST', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-hub-dust', NULL, NULL, 0, 0, 0, '', 0),
(186, '47VE56180000', 'COVER, KICK LEVER', 1, '174.0000', '210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-kick-lever', NULL, NULL, 0, 0, 0, '', 0),
(187, '2TBF74330000', 'COVER, REAR FOOTREST', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-rear-footrest', NULL, NULL, 0, 0, 0, '', 0),
(188, '1KLE81130000', 'COVER, SHIFT PEDAL', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-shift-pedal', NULL, NULL, 0, 0, 0, '', 0),
(189, '2TBXF17110P0', 'COVER, SIDE 1', 1, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-side-1', NULL, NULL, 0, 0, 0, '', 0),
(190, '2TBXF17210P0', 'COVER, SIDE 2', 1, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-side-2', NULL, NULL, 0, 0, 0, '', 0),
(191, '18CH43970000', 'COVER, SOCKET', 1, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-socket', NULL, NULL, 0, 0, 0, '', 0),
(192, '2TBXF47500P2', 'COVER, TAIL C', 1, '473.0000', '570.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-c', NULL, NULL, 0, 0, 0, '', 0),
(193, '2TBF471K20P1', 'COVER, TAIL 1 B', 1, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-1-b', NULL, NULL, 0, 0, 0, '', 0),
(194, '2TBF471K00P2', 'COVER, TAIL 1 C', 1, '713.0000', '860.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-1-c', NULL, NULL, 0, 0, 0, '', 0),
(195, '2TBF471K20P2', 'COVER, TAIL 1 C', 1, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-1-c', NULL, NULL, 0, 0, 0, '', 0),
(196, '2TBF472K20P1', 'COVER, TAIL 2 B', 1, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-2-b', NULL, NULL, 0, 0, 0, '', 0),
(197, '2TBF472K00P2', 'COVER, TAIL 2 C', 1, '713.0000', '860.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-2-c', NULL, NULL, 0, 0, 0, '', 0),
(198, '2TBF472K20P2', 'COVER, TAIL 2 C', 1, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cover-tail-2-c', NULL, NULL, 0, 0, 0, '', 0),
(199, '5VLE14220000', 'CRANK 2', 1, '3226.0000', '3700.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'crank-2', NULL, NULL, 0, 0, 0, '', 0),
(200, '5VLW11411000', 'CRANK SUB ASSY 1', 1, '7463.0000', '8160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'crank-sub-assy-1', NULL, NULL, 0, 0, 0, '', 0),
(201, '1SFW15100000', 'CRANKCASE ASSY', 1, '19434.0000', '21250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'crankcase-assy', NULL, NULL, 0, 0, 0, '', 0),
(202, '2TBW11400000', 'CRANKSHAFT ASSY', 1, '16233.0000', '17750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'crankshaft-assy', NULL, NULL, 0, 0, 0, '', 0),
(203, '1BKE13100000', 'CYLINDER', 1, '5117.0000', '5870.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cylinder', NULL, NULL, 0, 0, 0, '', 0),
(204, '5VLE13101000', 'CYLINDER', 1, '6103.0000', '7000.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cylinder', NULL, NULL, 0, 0, 0, '', 0),
(205, '2TBF31700000', 'CYLINDER COMP., FRONT FORK', 1, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cylinder-comp-front-fork', NULL, NULL, 0, 0, 0, '', 0),
(206, '3D9W00410200', 'CYLINDER KIT, MASTER', 1, '3662.0000', '4200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'cylinder-kit-master', NULL, NULL, 0, 0, 0, '', 0),
(207, '18CF83710000', 'DAMPER', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(208, '1KLF53640000', 'DAMPER', 1, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(209, '304253640100', 'DAMPER', 1, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(210, '5VLF17460000', 'DAMPER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(211, '903881480000', 'DAMPER', 1, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(212, '5VLH35430000', 'DAMPER, BRACKET', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper-bracket', NULL, NULL, 0, 0, 0, '', 0),
(213, '5VLF41820000', 'DAMPER, LOCATING 2', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper-locating-2', NULL, NULL, 0, 0, 0, '', 0),
(214, '5VLF41830000', 'DAMPER, LOCATING 3', 1, '323.0000', '390.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper-locating-3', NULL, NULL, 0, 0, 0, '', 0),
(215, '12BF411J0000', 'DAMPER, SIDE COVER', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damper-side-cover', NULL, NULL, 0, 0, 0, '', 0),
(216, '4H7241810000', 'DAMPER,LOCATING 1', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'damperlocating-1', NULL, NULL, 0, 0, 0, '', 0),
(217, '4P2W149A0000', 'DIAPHRAGM SET 1', 1, '1072.0000', '1230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'diaphragm-set-1', NULL, NULL, 0, 0, 0, '', 0),
(218, '4KLF58540000', 'DIAPHRAGM, RESERVOIR', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'diaphragm-reservoir', NULL, NULL, 0, 0, 0, '', 0),
(219, '2TBH19800000', 'DIODE ASSY', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'diode-assy', NULL, NULL, 0, 0, 0, '', 0),
(220, '3D9F582U1000', 'DISK, BRAKE (RIGHT)', 1, '4054.0000', '4650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'disk-brake-right', NULL, NULL, 0, 0, 0, '', 0),
(221, '18CF47300100', 'DOUBLE SEAT ASSY', 1, '5518.0000', '6330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'double-seat-assy', NULL, NULL, 0, 0, 0, '', 0),
(222, '5VLE74020000', 'DRIVE AXLE ASSY', 1, '1456.0000', '1670.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'drive-axle-assy', NULL, NULL, 0, 0, 0, '', 0),
(223, '2MPHA2510000', 'DRIVE UNIT 1', 1, '2737.0000', '3140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'drive-unit-1', NULL, NULL, 0, 0, 0, '', 0),
(224, '2MPHA2520000', 'DRIVE UNIT 2', 1, '2171.0000', '2490.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'drive-unit-2', NULL, NULL, 0, 0, 0, '', 0),
(225, '13RHA2531000', 'DRIVE UNIT 3', 1, '1194.0000', '1370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'drive-unit-3', NULL, NULL, 0, 0, 0, '', 0),
(226, '2D0E44370000', 'DUCT', 1, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'duct', NULL, NULL, 0, 0, 0, '', 0),
(227, '5VLE44501200', 'ELEMENT ASSY, AIRCLEANER', 1, '680.0000', '820.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'element-assy-aircleaner', NULL, NULL, 0, 0, 0, '', 0),
(228, '2TBF83150000', 'EMBLEM', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'emblem', NULL, NULL, 0, 0, 0, '', 0),
(229, '992360008000', 'EMBLEM, YAMAHA', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'emblem-yamaha', NULL, NULL, 0, 0, 0, '', 0),
(230, '992360010000', 'EMBLEM, YAMAHA', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'emblem-yamaha', NULL, NULL, 0, 0, 0, '', 0),
(231, '2TBF151120P2', 'FENDER, FRONT C', 1, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fender-front-c', NULL, NULL, 0, 0, 0, '', 0),
(232, '2TBF16110000', 'FENDER, REAR', 1, '497.0000', '600.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fender-rear', NULL, NULL, 0, 0, 0, '', 0),
(233, '5VLE34510000', 'FILTER, ROTARY', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'filter-rotary', NULL, NULL, 0, 0, 0, '', 0),
(234, '18CH33500100', 'FLASHER RELAY ASSY', 1, '688.0000', '830.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'flasher-relay-assy', NULL, NULL, 0, 0, 0, '', 0),
(235, '5LBE43850000', 'FLOAT', 1, '2092.0000', '2400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'float', NULL, NULL, 0, 0, 0, '', 0),
(236, '18CF74110000', 'FOOTREST 1', 1, '2293.0000', '2630.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'footrest-1', NULL, NULL, 0, 0, 0, '', 0),
(237, '2TBF74310000', 'FOOTREST, REAR 1', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'footrest-rear-1', NULL, NULL, 0, 0, 0, '', 0),
(238, '2D0E85110000', 'FORK, SHIFT 1', 1, '431.0000', '520.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fork-shift-1', NULL, NULL, 0, 0, 0, '', 0),
(239, '2D0E85120000', 'FORK, SHIFT 2', 1, '398.0000', '480.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fork-shift-2', NULL, NULL, 0, 0, 0, '', 0),
(240, '2D0E85130000', 'FORK, SHIFT 3', 1, '406.0000', '490.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fork-shift-3', NULL, NULL, 0, 0, 0, '', 0),
(241, '2TBF11100000', 'FRAME COMP.', 1, '13444.0000', '14700.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'frame-comp', NULL, NULL, 0, 0, 0, '', 0),
(242, '18CH33100000', 'FRONT FLASHER LIGHT ASSY 1', 1, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'front-flasher-light-assy-1', NULL, NULL, 0, 0, 0, '', 0),
(243, '18CH33200000', 'FRONT FLASHER LIGHT ASSY 2', 1, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'front-flasher-light-assy-2', NULL, NULL, 0, 0, 0, '', 0),
(244, '2TBF31020000', 'FRONT FORK ASSY (L.H)', 1, '4272.0000', '4900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'front-fork-assy-lh', NULL, NULL, 0, 0, 0, '', 0),
(245, '2TBF31030000', 'FRONT FORK ASSY (R.H)', 1, '4272.0000', '4900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'front-fork-assy-rh', NULL, NULL, 0, 0, 0, '', 0),
(246, '5VLH39800100', 'FRONT STOP SWITCHASSY', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'front-stop-switchassy', NULL, NULL, 0, 0, 0, '', 0),
(247, '5VLF45000100', 'FUEL COCK ASSY 1', 1, '1177.0000', '1350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fuel-cock-assy-1', NULL, NULL, 0, 0, 0, '', 0),
(248, '2TBXCF4100P0', 'FUEL TANK COMP. A', 1, '8322.0000', '9100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fuel-tank-comp-a', NULL, NULL, 0, 0, 0, '', 0),
(249, '2TBXCF4110P1', 'FUEL TANK COMP. B', 1, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fuel-tank-comp-b', NULL, NULL, 0, 0, 0, '', 0),
(250, '2TBXCF4120P2', 'FUEL TANK COMP. C', 1, '8322.0000', '9100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fuel-tank-comp-c', NULL, NULL, 0, 0, 0, '', 0),
(251, '2TBXCF4130P3', 'FUEL TANK COMP. D', 1, '8459.0000', '9250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fuel-tank-comp-d', NULL, NULL, 0, 0, 0, '', 0),
(252, '4FPH21510000', 'FUSE (15A)', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'fuse-15a', NULL, NULL, 0, 0, 0, '', 0),
(253, '4TUF34790000', 'GASKET', 1, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(254, '5VLE48150000', 'GASKET', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(255, '5VLF45120100', 'GASKET', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(256, '904300580000', 'GASKET', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(257, '904300681700', 'GASKET', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(258, '904301222700', 'GASKET', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(259, '5VLE54511000', 'GASKET, CRANKCASECOVER 1', 1, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-crankcasecover-1', NULL, NULL, 0, 0, 0, '', 0),
(260, '5VLE54611000', 'GASKET, CRANKCASECOVER 2', 1, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-crankcasecover-2', NULL, NULL, 0, 0, 0, '', 0),
(261, '5VLE13511000', 'GASKET, CYLINDER', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-cylinder', NULL, NULL, 0, 0, 0, '', 0),
(262, '5VLE11810000', 'GASKET, CYLINDER HEAD 1', 1, '365.0000', '440.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-cylinder-head-1', NULL, NULL, 0, 0, 0, '', 0),
(263, '5VLE46130000', 'GASKET, EXHAUST PIPE', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-exhaust-pipe', NULL, NULL, 0, 0, 0, '', 0),
(264, '5LBE43840000', 'GASKET, FLOAT CHAMBER', 1, '464.0000', '560.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-float-chamber', NULL, NULL, 0, 0, 0, '', 0),
(265, '5VLE33290000', 'GASKET, PUMP COVER', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-pump-cover', NULL, NULL, 0, 0, 0, '', 0),
(266, '4FPH57530100', 'GASKET, SENDER UNIT', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-sender-unit', NULL, NULL, 0, 0, 0, '', 0),
(267, '18CH47040000', 'GASKET, TAILLIGHT', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-taillight', NULL, NULL, 0, 0, 0, '', 0),
(268, '5VLE22131000', 'GASKET, TENSIONERCASE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gasket-tensionercase', NULL, NULL, 0, 0, 0, '', 0),
(269, '4GUF51901200', 'GEAR UNIT ASSY', 1, '1369.0000', '1570.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-unit-assy', NULL, NULL, 0, 0, 0, '', 0),
(270, '4GUF51902000', 'GEAR UNIT ASSY', 1, '1369.0000', '1570.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-unit-assy', NULL, NULL, 0, 0, 0, '', 0),
(271, '5VLE72110000', 'GEAR, 1ST WHEEL (37T)', 1, '1159.0000', '1330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-1st-wheel-37t', NULL, NULL, 0, 0, 0, '', 0),
(272, '5VLE71210000', 'GEAR, 2ND PINION (18T)', 1, '924.0000', '1060.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-2nd-pinion-18t', NULL, NULL, 0, 0, 0, '', 0),
(273, '5VLE72210000', 'GEAR, 2ND WHEEL (32T)', 1, '1011.0000', '1160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-2nd-wheel-32t', NULL, NULL, 0, 0, 0, '', 0),
(274, '5VLE71310000', 'GEAR, 3RD PINION (19T)', 1, '1020.0000', '1170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-3rd-pinion-19t', NULL, NULL, 0, 0, 0, '', 0),
(275, '5VLE72310000', 'GEAR, 3RD WHEEL (25T)', 1, '976.0000', '1120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-3rd-wheel-25t', NULL, NULL, 0, 0, 0, '', 0),
(276, '5VLE71410000', 'GEAR, 4TH PINION (22T)', 1, '1055.0000', '1210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-4th-pinion-22t', NULL, NULL, 0, 0, 0, '', 0),
(277, '5VLE72410000', 'GEAR, 4TH WHEEL (23T)', 1, '1282.0000', '1470.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-4th-wheel-23t', NULL, NULL, 0, 0, 0, '', 0),
(278, '5VLE71510000', 'GEAR, 5TH PINION (24T)', 1, '1020.0000', '1170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-5th-pinion-24t', NULL, NULL, 0, 0, 0, '', 0),
(279, '5VLE72510000', 'GEAR, 5TH WHEEL (21T)', 1, '1037.0000', '1190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-5th-wheel-21t', NULL, NULL, 0, 0, 0, '', 0),
(280, '5VLY11530000', 'GEAR, BALANCE WEIGHT ASSY.', 1, '2772.0000', '3180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-balance-weight-assy', NULL, NULL, 0, 0, 0, '', 0),
(281, '5VLE55120000', 'GEAR, IDLER 1', 1, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-idler-1', NULL, NULL, 0, 0, 0, '', 0),
(282, '5VLE55170000', 'GEAR, IDLER 2', 1, '3365.0000', '3860.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-idler-2', NULL, NULL, 0, 0, 0, '', 0),
(283, '4FPE56410000', 'GEAR, KICK', 1, '1177.0000', '1350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-kick', NULL, NULL, 0, 0, 0, '', 0),
(284, '5VLE56510000', 'GEAR, KICK IDLE', 1, '985.0000', '1130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-kick-idle', NULL, NULL, 0, 0, 0, '', 0),
(285, '1JPE61110000', 'GEAR, PRIMARY DRIVE', 1, '1133.0000', '1300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-primary-drive', NULL, NULL, 0, 0, 0, '', 0),
(286, '5VLE33240000', 'GEAR, PUMP DRIVE', 1, '506.0000', '610.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-pump-drive', NULL, NULL, 0, 0, 0, '', 0),
(287, '5VLE33250000', 'GEAR, PUMP DRIVEN', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'gear-pump-driven', NULL, NULL, 0, 0, 0, '', 0),
(288, '2TBF173E1000', 'GRAPHIC 1 C', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-1-c', NULL, NULL, 0, 0, 0, '', 0),
(289, '2TBF42440000', 'GRAPHIC, FUEL TANKA1', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tanka1', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(290, '2TBF42450000', 'GRAPHIC, FUEL TANKA2', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tanka2', NULL, NULL, 0, 0, 0, '', 0),
(291, '2TBF42550000', 'GRAPHIC, FUEL TANKA8', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tanka8', NULL, NULL, 0, 0, 0, '', 0),
(292, '2TBF42560000', 'GRAPHIC, FUEL TANKA9', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tanka9', NULL, NULL, 0, 0, 0, '', 0),
(293, '2TBF42441000', 'GRAPHIC, FUEL TANKB1', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankb1', NULL, NULL, 0, 0, 0, '', 0),
(294, '2TBF42451000', 'GRAPHIC, FUEL TANKB2', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankb2', NULL, NULL, 0, 0, 0, '', 0),
(295, '2TBF42551000', 'GRAPHIC, FUEL TANKB8', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankb8', NULL, NULL, 0, 0, 0, '', 0),
(296, '2TBF42561000', 'GRAPHIC, FUEL TANKB9', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankb9', NULL, NULL, 0, 0, 0, '', 0),
(297, '2TBF42442000', 'GRAPHIC, FUEL TANKC1', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankc1', NULL, NULL, 0, 0, 0, '', 0),
(298, '2TBF42452000', 'GRAPHIC, FUEL TANKC2', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankc2', NULL, NULL, 0, 0, 0, '', 0),
(299, '2TBF42552000', 'GRAPHIC, FUEL TANKC8', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankc8', NULL, NULL, 0, 0, 0, '', 0),
(300, '2TBF42562000', 'GRAPHIC, FUEL TANKC9', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankc9', NULL, NULL, 0, 0, 0, '', 0),
(301, '2TBF42443000', 'GRAPHIC, FUEL TANKD1', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankd1', NULL, NULL, 0, 0, 0, '', 0),
(302, '2TBF42453000', 'GRAPHIC, FUEL TANKD2', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankd2', NULL, NULL, 0, 0, 0, '', 0),
(303, '2TBF42553000', 'GRAPHIC, FUEL TANKD8', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankd8', NULL, NULL, 0, 0, 0, '', 0),
(304, '2TBF42563000', 'GRAPHIC, FUEL TANKD9', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'graphic-fuel-tankd9', NULL, NULL, 0, 0, 0, '', 0),
(305, '5VLF62410000', 'GRIP', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grip', NULL, NULL, 0, 0, 0, '', 0),
(306, '5VLF62400000', 'GRIP ASSY', 1, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grip-assy', NULL, NULL, 0, 0, 0, '', 0),
(307, '904800186800', 'GROMMET', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(308, '904801381000', 'GROMMET', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(309, '904801383700', 'GROMMET', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(310, '904801587800', 'GROMMET', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(311, '904802418600', 'GROMMET', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(312, '5VLE22311000', 'GUIDE, STOPPER 1', 1, '398.0000', '480.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'guide-stopper-1', NULL, NULL, 0, 0, 0, '', 0),
(313, '2MPE22410000', 'GUIDE, STOPPER 2', 1, '481.0000', '580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'guide-stopper-2', NULL, NULL, 0, 0, 0, '', 0),
(314, '2UJ111331000', 'GUIDE, VALVE 1', 1, '1552.0000', '1780.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'guide-valve-1', NULL, NULL, 0, 0, 0, '', 0),
(315, '22F111341000', 'GUIDE, VALVE 2', 1, '2406.0000', '2760.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'guide-valve-2', NULL, NULL, 0, 0, 0, '', 0),
(316, '2TBF47730000', 'HANDLE, SEAT', 1, '3016.0000', '3460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'handle-seat', NULL, NULL, 0, 0, 0, '', 0),
(317, '2TBF61110000', 'HANDLEBAR', 1, '489.0000', '590.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'handlebar', NULL, NULL, 0, 0, 0, '', 0),
(318, '5VLE11111100', 'HEAD, CYLINDER 1', 1, '14175.0000', '15500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'head-cylinder-1', NULL, NULL, 0, 0, 0, '', 0),
(319, '2TBH43001000', 'HEADLIGHT ASSY', 1, '4359.0000', '5000.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'headlight-assy', NULL, NULL, 0, 0, 0, '', 0),
(320, '5VLF58860000', 'HOLDER, BRAKE HOSE', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-brake-hose', NULL, NULL, 0, 0, 0, '', 0),
(321, '5VLF58870000', 'HOLDER, BRAKE HOSE', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-brake-hose', NULL, NULL, 0, 0, 0, '', 0),
(322, '5VLF58750100', 'HOLDER, BRAKE HOSE1', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-brake-hose1', NULL, NULL, 0, 0, 0, '', 0),
(323, '5VLF58760000', 'HOLDER, BRAKE HOSE2', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-brake-hose2', NULL, NULL, 0, 0, 0, '', 0),
(324, '5VLF15180100', 'HOLDER, CABLE', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-cable', NULL, NULL, 0, 0, 0, '', 0),
(325, '5VLE54410100', 'HOLDER, CLUTCH CABLE', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-clutch-cable', NULL, NULL, 0, 0, 0, '', 0),
(326, '5VLF34410000', 'HOLDER, HANDLE UPPER', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-handle-upper', NULL, NULL, 0, 0, 0, '', 0),
(327, '4FPH29110000', 'HOLDER, LEVER 1', 1, '506.0000', '610.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-lever-1', NULL, NULL, 0, 0, 0, '', 0),
(328, '3D9E74560100', 'HOLDER, SPROCKET', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'holder-sprocket', NULL, NULL, 0, 0, 0, '', 0),
(329, '18CH33711000', 'HORN', 1, '1194.0000', '1370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'horn', NULL, NULL, 0, 0, 0, '', 0),
(330, '2YBE49870000', 'HOSE', 1, '282.0000', '340.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose', NULL, NULL, 0, 0, 0, '', 0),
(331, '904450883000', 'HOSE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose', NULL, NULL, 0, 0, 0, '', 0),
(332, '904450883800', 'HOSE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose', NULL, NULL, 0, 0, 0, '', 0),
(333, '904504480400', 'HOSE CLAMP ASSY', 1, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose-clamp-assy', NULL, NULL, 0, 0, 0, '', 0),
(334, '904505180200', 'HOSE CLAMP ASSY', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose-clamp-assy', NULL, NULL, 0, 0, 0, '', 0),
(335, '2YBE48810000', 'HOSE, BEND 1', 1, '950.0000', '1090.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose-bend-1', NULL, NULL, 0, 0, 0, '', 0),
(336, '2YBE48820000', 'HOSE, BEND 2', 1, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose-bend-2', NULL, NULL, 0, 0, 0, '', 0),
(337, '3D9F58721100', 'HOSE, BRAKE 1', 1, '2310.0000', '2650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'hose-brake-1', NULL, NULL, 0, 0, 0, '', 0),
(338, '1JPH23100000', 'IGNITION COIL ASSY', 1, '933.0000', '1070.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'ignition-coil-assy', NULL, NULL, 0, 0, 0, '', 0),
(339, '2TBF31100000', 'INNER TUBE COMP.1', 1, '1508.0000', '1730.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'inner-tube-comp1', NULL, NULL, 0, 0, 0, '', 0),
(340, '5HHF31102200', 'INNER TUBE COMP.1', 1, '14724.0000', '16100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'inner-tube-comp1', NULL, NULL, 0, 0, 0, '', 0),
(341, '1SBE43436100', 'JET, MAIN', 1, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'jet-main', NULL, NULL, 0, 0, 0, '', 0),
(342, '5LBE43421700', 'JET, PILOT', 1, '356.0000', '430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'jet-pilot', NULL, NULL, 0, 0, 0, '', 0),
(343, '4P2E44530000', 'JOINT, AIR CLEANER1', 1, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'joint-air-cleaner1', NULL, NULL, 0, 0, 0, '', 0),
(344, '4P2E35861000', 'JOINT, CARBURETOR1', 1, '959.0000', '1100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'joint-carburetor1', NULL, NULL, 0, 0, 0, '', 0),
(345, '94668E200100', 'JOINT, CHAIN (428H)', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'joint-chain-428h', NULL, NULL, 0, 0, 0, '', 0),
(346, '5VLH21090000', 'JOINT, HOSE', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'joint-hose', NULL, NULL, 0, 0, 0, '', 0),
(347, '2TBH20210000', 'KEY SET', 1, '4446.0000', '5100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'key-set', NULL, NULL, 0, 0, 0, '', 0),
(348, '902820500100', 'KEY, STRAIGHT (5MMX16MM)', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'key-straight-5mmx16mm', NULL, NULL, 0, 0, 0, '', 0),
(349, '902800301700', 'KEY, WOODRUFF', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'key-woodruff', NULL, NULL, 0, 0, 0, '', 0),
(350, '5VLE56600100', 'KICK AXLE ASSY', 1, '1247.0000', '1430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'kick-axle-assy', NULL, NULL, 0, 0, 0, '', 0),
(351, '5VLE56200100', 'KICK CRANK ASSY', 1, '3121.0000', '3580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'kick-crank-assy', NULL, NULL, 0, 0, 0, '', 0),
(352, '2TBF15680000', 'LABEL, WARNING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'label-warning', NULL, NULL, 0, 0, 0, '', 0),
(353, '3D9H333A0000', 'LENS COMP 1', 1, '240.0000', '290.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lens-comp-1', NULL, NULL, 0, 0, 0, '', 0),
(354, '3D9H334A0000', 'LENS COMP 2', 1, '240.0000', '290.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lens-comp-2', NULL, NULL, 0, 0, 0, '', 0),
(355, '18CH47210100', 'LENS, TAILLIGHT', 1, '481.0000', '580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lens-taillight', NULL, NULL, 0, 0, 0, '', 0),
(356, '5VLH39120000', 'LEVER 1', 1, '605.0000', '730.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lever-1', NULL, NULL, 0, 0, 0, '', 0),
(357, '5VLH39222200', 'LEVER 2', 1, '605.0000', '730.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lever-2', NULL, NULL, 0, 0, 0, '', 0),
(358, '5VLF53550100', 'LEVER, CAMSHAFT', 1, '406.0000', '490.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lever-camshaft', NULL, NULL, 0, 0, 0, '', 0),
(359, '5VLF45240100', 'LEVER, COCK', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lever-cock', NULL, NULL, 0, 0, 0, '', 0),
(360, '2TBF71150000', 'LINK, MAIN STAND', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'link-main-stand', NULL, NULL, 0, 0, 0, '', 0),
(361, '5VLF17800100', 'LOCK ASSY', 1, '390.0000', '470.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'lock-assy', NULL, NULL, 0, 0, 0, '', 0),
(362, '2TBH25010000', 'MAIN SWITCH STEERING LOCK', 1, '3226.0000', '3700.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'main-switch-steering-lock', NULL, NULL, 0, 0, 0, '', 0),
(363, '2B9W583T0200', 'MASTER CYLINDER ASSY', 1, '5484.0000', '6290.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'master-cylinder-assy', NULL, NULL, 0, 0, 0, '', 0),
(364, '2MPH35000000', 'METER ASSY', 1, '8103.0000', '8860.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'meter-assy', NULL, NULL, 0, 0, 0, '', 0),
(365, '1JPH18900000', 'MOTOR ASSY', 1, '7499.0000', '8200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'motor-assy', NULL, NULL, 0, 0, 0, '', 0),
(366, '4P2W149J0000', 'NEEDLE SET', 1, '1273.0000', '1460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'needle-set', NULL, NULL, 0, 0, 0, '', 0),
(367, '1BVE43900000', 'NEEDLE VALVE ASSY', 1, '1613.0000', '1850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'needle-valve-assy', NULL, NULL, 0, 0, 0, '', 0),
(368, '5VLF45150000', 'NET, FILTER 2', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'net-filter-2', NULL, NULL, 0, 0, 0, '', 0),
(369, '13RH25400000', 'NEUTRAL SWITCH ASSY', 1, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'neutral-switch-assy', NULL, NULL, 0, 0, 0, '', 0),
(370, '5FYE42510000', 'NIPPLE', 1, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nipple', NULL, NULL, 0, 0, 0, '', 0),
(371, '2XPE43410000', 'NOZZLE, MAIN', 1, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nozzle-main', NULL, NULL, 0, 0, 0, '', 0),
(372, '5RSE43411000', 'NOZZLE, MAIN', 1, '415.0000', '500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nozzle-main', NULL, NULL, 0, 0, 0, '', 0),
(373, '50ME43550000', 'NOZZLE, MAIN 2', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nozzle-main-2', NULL, NULL, 0, 0, 0, '', 0),
(374, '1BKH354P0000', 'NUT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(375, '901701206000', 'NUT', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(376, '901790552300', 'NUT', 1, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(377, '901790580400', 'NUT', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(378, '901790684301', 'NUT', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(379, '901790863900', 'NUT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(380, '901790883000', 'NUT', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(381, '901790886200', 'NUT', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(382, '901791080700', 'NUT', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(383, '901791281300', 'NUT', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(384, '901792580600', 'NUT', 1, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(385, '953020660000', 'NUT', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(386, '953120860000', 'NUT', 1, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(387, '9570N1250000', 'NUT', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut', NULL, NULL, 0, 0, 0, '', 0),
(388, '901761081000', 'NUT, CROWN', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-crown', NULL, NULL, 0, 0, 0, '', 0),
(389, '901762280100', 'NUT, CROWN', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-crown', NULL, NULL, 0, 0, 0, '', 0),
(390, '957020550000', 'NUT, FLANGE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-flange', NULL, NULL, 0, 0, 0, '', 0),
(391, '957020850000', 'NUT, FLANGE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-flange', NULL, NULL, 0, 0, 0, '', 0),
(392, '901851081300', 'NUT, SELF-LOCKING', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-self-locking', NULL, NULL, 0, 0, 0, '', 0),
(393, '956021220000', 'NUT, SELF-LOCKING', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-self-locking', NULL, NULL, 0, 0, 0, '', 0),
(394, '956021420000', 'NUT, SELF-LOCKING', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-self-locking', NULL, NULL, 0, 0, 0, '', 0),
(395, '90183050A600', 'NUT, SPRING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-spring', NULL, NULL, 0, 0, 0, '', 0),
(396, '901830580700', 'NUT, SPRING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-spring', NULL, NULL, 0, 0, 0, '', 0),
(397, '3D9F586F0100', 'NUT, U', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-u', NULL, NULL, 0, 0, 0, '', 0),
(398, '956020510000', 'NUT, U', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-u', NULL, NULL, 0, 0, 0, '', 0),
(399, '956020610000', 'NUT, U', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-u', NULL, NULL, 0, 0, 0, '', 0),
(400, '956170610000', 'NUT, U', 1, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'nut-u', NULL, NULL, 0, 0, 0, '', 0),
(401, '5VLE33000100', 'OIL PUMP ASSY', 1, '1037.0000', '1190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-pump-assy', NULL, NULL, 0, 0, 0, '', 0),
(402, '1STF31450000', 'OIL SEAL', 1, '746.0000', '900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(403, '931011080000', 'OIL SEAL', 1, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(404, '931011280300', 'OIL SEAL', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(405, '931021281200', 'OIL SEAL', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(406, '931022081200', 'OIL SEAL', 1, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(407, '931022085700', 'OIL SEAL', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(408, '931042280200', 'OIL SEAL', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(409, '931054780800', 'OIL SEAL', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(410, '931062680400', 'OIL SEAL', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(411, '931092080200', 'OIL SEAL', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(412, '5APF31470000', 'O-RING', 1, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(413, '93210108B100', 'O-RING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(414, '932101180900', 'O-RING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(415, '932101481700', 'O-RING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(416, '932101912300', 'O-RING', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(417, '932102380100', 'O-RING', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(418, '932102666800', 'O-RING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(419, '932103061100', 'O-RING', 1, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(420, '932104210100', 'O-RING', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(421, '932104481200', 'O-RING', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(422, '932105566900', 'O-RING', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(423, '932107980200', 'O-RING', 1, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(424, '2TBF31060000', 'OUTER TUBE COMP.', 1, '1613.0000', '1850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'outer-tube-comp', NULL, NULL, 0, 0, 0, '', 0),
(425, '2TBF31070000', 'OUTER TUBE COMP. (R.H)', 1, '1656.0000', '1900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'outer-tube-comp-rh', NULL, NULL, 0, 0, 0, '', 0),
(426, '3D9F72110000', 'PEDAL, BRAKE', 1, '2310.0000', '2650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pedal-brake', NULL, NULL, 0, 0, 0, '', 0),
(427, '902491280800', 'PIN', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin', NULL, NULL, 0, 0, 0, '', 0),
(428, '902400680600', 'PIN, CLEVIS', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-clevis', NULL, NULL, 0, 0, 0, '', 0),
(429, '914022001200', 'PIN, COTTER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-cotter', NULL, NULL, 0, 0, 0, '', 0),
(430, '914023002500', 'PIN, COTTER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-cotter', NULL, NULL, 0, 0, 0, '', 0),
(431, '914901602000', 'PIN, COTTER', 1, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-cotter', NULL, NULL, 0, 0, 0, '', 0),
(432, '5VLE16810000', 'PIN, CRANK 1', 1, '829.0000', '1000.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-crank-1', NULL, NULL, 0, 0, 0, '', 0),
(433, '936041083700', 'PIN, DOWEL', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-dowel', NULL, NULL, 0, 0, 0, '', 0),
(434, '936083083900', 'PIN, DOWEL', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-dowel', NULL, NULL, 0, 0, 0, '', 0),
(435, '995301011400', 'PIN, DOWEL', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-dowel', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(436, '5LBE43860000', 'PIN, FLOAT', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-float', NULL, NULL, 0, 0, 0, '', 0),
(437, '4S5F59240100', 'PIN, PAD', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-pad', NULL, NULL, 0, 0, 0, '', 0),
(438, '5VLE16330000', 'PIN, PISTON', 1, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pin-piston', NULL, NULL, 0, 0, 0, '', 0),
(439, '4P2E41970000', 'PIPE', 1, '257.0000', '310.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe', NULL, NULL, 0, 0, 0, '', 0),
(440, '2XPE48720000', 'PIPE 2', 1, '1630.0000', '1870.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe-2', NULL, NULL, 0, 0, 0, '', 0),
(441, '5VLE11660000', 'PIPE, BREATHER 1', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe-breather-1', NULL, NULL, 0, 0, 0, '', 0),
(442, '4FPE11670000', 'PIPE, BREATHER 2', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe-breather-2', NULL, NULL, 0, 0, 0, '', 0),
(443, '2D0E443E0000', 'PIPE, DRAIN', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe-drain', NULL, NULL, 0, 0, 0, '', 0),
(444, '4P2F43110000', 'PIPE, FUEL 1', 1, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe-fuel-1', NULL, NULL, 0, 0, 0, '', 0),
(445, '1PXE41960000', 'PIPE, OVER FLOW 1', 1, '489.0000', '590.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'pipe-over-flow-1', NULL, NULL, 0, 0, 0, '', 0),
(446, '5VLE16311000', 'PISTON (STD)', 1, '985.0000', '1130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'piston-std', NULL, NULL, 0, 0, 0, '', 0),
(447, '4S5W00570000', 'PISTON ASSY, CALIPER', 1, '1264.0000', '1450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'piston-assy-caliper', NULL, NULL, 0, 0, 0, '', 0),
(448, '5VLW11610000', 'PISTON RING SET (STD)', 1, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'piston-ring-set-std', NULL, NULL, 0, 0, 0, '', 0),
(449, '18CF83360100', 'PLATE', 1, '763.0000', '920.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate', NULL, NULL, 0, 0, 0, '', 0),
(450, '5VLE111F0000', 'PLATE', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate', NULL, NULL, 0, 0, 0, '', 0),
(451, '5VLE51910000', 'PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate', NULL, NULL, 0, 0, 0, '', 0),
(452, '2TBF23480100', 'PLATE 2', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-2', NULL, NULL, 0, 0, 0, '', 0),
(453, '5VLF53210133', 'PLATE, BRAKE SHOE ABC', 1, '2537.0000', '2910.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-brake-shoe-abc', NULL, NULL, 0, 0, 0, '', 0),
(454, '2TBF23470000', 'PLATE, CHAIN CASERUBBER', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-chain-caserubber', NULL, NULL, 0, 0, 0, '', 0),
(455, '1JPE63240000', 'PLATE, CLUTCH 1', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-clutch-1', NULL, NULL, 0, 0, 0, '', 0),
(456, '5VLE74710000', 'PLATE, COVER', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-cover', NULL, NULL, 0, 0, 0, '', 0),
(457, '1JPE63210000', 'PLATE, FRICTION', 1, '439.0000', '530.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-friction', NULL, NULL, 0, 0, 0, '', 0),
(458, '5VLE55320000', 'PLATE, IDLE GEAR', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-idle-gear', NULL, NULL, 0, 0, 0, '', 0),
(459, '4GUF533A0100', 'PLATE, INDICATOR', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-indicator', NULL, NULL, 0, 0, 0, '', 0),
(460, '1JPE63510000', 'PLATE, PRESSURE 1', 1, '959.0000', '1100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-pressure-1', NULL, NULL, 0, 0, 0, '', 0),
(461, '1JPE61540000', 'PLATE, THRUST 1', 1, '655.0000', '790.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-thrust-1', NULL, NULL, 0, 0, 0, '', 0),
(462, '5VLE61641000', 'PLATE, THRUST 2', 1, '174.0000', '210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plate-thrust-2', NULL, NULL, 0, 0, 0, '', 0),
(463, '5LBE41530000', 'PLATE,BAFFLE', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'platebaffle', NULL, NULL, 0, 0, 0, '', 0),
(464, '903381614800', 'PLUG', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plug', NULL, NULL, 0, 0, 0, '', 0),
(465, '1JPH23700100', 'PLUG CAP ASSY', 1, '340.0000', '410.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plug-cap-assy', NULL, NULL, 0, 0, 0, '', 0),
(466, '947000086900', 'PLUG SPARK (NGK CR6HSA)', 1, '473.0000', '570.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plug-spark-ngk-cr6hsa', NULL, NULL, 0, 0, 0, '', 0),
(467, '5VLE53620000', 'PLUG, OIL LEVEL', 1, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plug-oil-level', NULL, NULL, 0, 0, 0, '', 0),
(468, '903401209700', 'PLUG, STRAIGHT SCREW', 1, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plug-straight-screw', NULL, NULL, 0, 0, 0, '', 0),
(469, '903401480500', 'PLUG,STRAIGHT SCREW', 1, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plugstraight-screw', NULL, NULL, 0, 0, 0, '', 0),
(470, '903401481000', 'PLUG,STRAIGHT SCREW', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plugstraight-screw', NULL, NULL, 0, 0, 0, '', 0),
(471, '4P2E43710000', 'PLUNGER, STARTER', 1, '1831.0000', '2100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'plunger-starter', NULL, NULL, 0, 0, 0, '', 0),
(472, '1JDE85420000', 'POINT, NEUTRAL', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'point-neutral', NULL, NULL, 0, 0, 0, '', 0),
(473, '1JPE61500000', 'PRIMARY DRIVEN GEAR COMP.', 1, '8139.0000', '8900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'primary-driven-gear-comp', NULL, NULL, 0, 0, 0, '', 0),
(474, '5VLE46280000', 'PROTECTOR, EXHAUSTPIPE', 1, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'protector-exhaustpipe', NULL, NULL, 0, 0, 0, '', 0),
(475, '5VLE47580000', 'PROTECTOR, SILENCER', 1, '1552.0000', '1780.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'protector-silencer', NULL, NULL, 0, 0, 0, '', 0),
(476, '2TBF53880000', 'PULLER, CHAIN 1', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'puller-chain-1', NULL, NULL, 0, 0, 0, '', 0),
(477, '5VLE63810200', 'PUSH LEVER COMP.', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'push-lever-comp', NULL, NULL, 0, 0, 0, '', 0),
(478, '5VLY16351000', 'PUSH LEVER COMP.', 1, '614.0000', '740.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'push-lever-comp', NULL, NULL, 0, 0, 0, '', 0),
(479, '4FPF34110000', 'RACE, BALL 1', 1, '199.0000', '240.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'race-ball-1', NULL, NULL, 0, 0, 0, '', 0),
(480, '4FPF34111000', 'RACE, BALL 1', 1, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'race-ball-1', NULL, NULL, 0, 0, 0, '', 0),
(481, '1KLF34120000', 'RACE, BALL 2', 1, '199.0000', '240.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'race-ball-2', NULL, NULL, 0, 0, 0, '', 0),
(482, '4FPF34120000', 'RACE, BALL 2', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'race-ball-2', NULL, NULL, 0, 0, 0, '', 0),
(483, '2TBF21100000', 'REAR ARM COMP.', 1, '1508.0000', '1730.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rear-arm-comp', NULL, NULL, 0, 0, 0, '', 0),
(484, '3D9H33301000', 'REAR FLASHER LIGHTASSY 1', 1, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rear-flasher-lightassy-1', NULL, NULL, 0, 0, 0, '', 0),
(485, '3D9H33401000', 'REAR FLASHER LIGHTASSY 2', 1, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rear-flasher-lightassy-2', NULL, NULL, 0, 0, 0, '', 0),
(486, '4FPH51302100', 'REAR REFLECTOR ASSY', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rear-reflector-assy', NULL, NULL, 0, 0, 0, '', 0),
(487, '5HMF62800000', 'REAR VIEW MIRROR ASSY (LEFT)', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rear-view-mirror-assy-left', NULL, NULL, 0, 0, 0, '', 0),
(488, '5HMF62900000', 'REAR VIEW MIRROR ASSY (RIGHT)', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rear-view-mirror-assy-right', NULL, NULL, 0, 0, 0, '', 0),
(489, '2D0H19601000', 'RECTIFIER & REGULATOR ASSY', 1, '1194.0000', '1370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rectifier-regulator-assy', NULL, NULL, 0, 0, 0, '', 0),
(490, '5VLH51104000', 'REFLECTOR ASSY', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'reflector-assy', NULL, NULL, 0, 0, 0, '', 0),
(491, '5VLF341E0000', 'RETAINER, BALL BEARING', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'retainer-ball-bearing', NULL, NULL, 0, 0, 0, '', 0),
(492, '5VLE21170100', 'RETAINER, VALVE SPRING', 1, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'retainer-valve-spring', NULL, NULL, 0, 0, 0, '', 0),
(493, '5APF31530000', 'RING, SNAP', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'ring-snap', NULL, NULL, 0, 0, 0, '', 0),
(494, '902673284500', 'RIVET, BLIND', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rivet-blind', NULL, NULL, 0, 0, 0, '', 0),
(495, '2MPE16510000', 'ROD, CONNECTING', 1, '2005.0000', '2300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rod-connecting', NULL, NULL, 0, 0, 0, '', 0),
(496, '5VLE63570100', 'ROD, PUSH 2', 1, '630.0000', '760.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rod-push-2', NULL, NULL, 0, 0, 0, '', 0),
(497, '1SFH14502000', 'ROTOR ASSY', 1, '2912.0000', '3340.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'rotor-assy', NULL, NULL, 0, 0, 0, '', 0),
(498, '2TBWF37W00P0', 'SCOOP, AIR 1 A', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-1-a', NULL, NULL, 0, 0, 0, '', 0),
(499, '2TBWF37W10P1', 'SCOOP, AIR 1 B', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-1-b', NULL, NULL, 0, 0, 0, '', 0),
(500, '2TBWF37W20P2', 'SCOOP, AIR 1 C', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-1-c', NULL, NULL, 0, 0, 0, '', 0),
(501, '2TBWF37W30P3', 'SCOOP, AIR 1 D', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-1-d', NULL, NULL, 0, 0, 0, '', 0),
(502, '2TBWF37X00P0', 'SCOOP, AIR 2 A', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-2-a', NULL, NULL, 0, 0, 0, '', 0),
(503, '2TBWF37X10P1', 'SCOOP, AIR 2 B', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-2-b', NULL, NULL, 0, 0, 0, '', 0),
(504, '2TBWF37X20P2', 'SCOOP, AIR 2 C', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-2-c', NULL, NULL, 0, 0, 0, '', 0),
(505, '2TBWF37X30P3', 'SCOOP, AIR 2 D', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'scoop-air-2-d', NULL, NULL, 0, 0, 0, '', 0),
(506, '4KLF589H0100', 'SCREW', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(507, '5LBE45920000', 'SCREW', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(508, '901490580000', 'SCREW', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(509, '901490683300', 'SCREW', 1, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(510, '4FPH18280100', 'SCREW 2', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-2', NULL, NULL, 0, 0, 0, '', 0),
(511, '4JPE43230000', 'SCREW, AIR ADJUSTING', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-air-adjusting', NULL, NULL, 0, 0, 0, '', 0),
(512, '989020500800', 'SCREW, BIND', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-bind', NULL, NULL, 0, 0, 0, '', 0),
(513, '989020601600', 'SCREW, BIND', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-bind', NULL, NULL, 0, 0, 0, '', 0),
(514, '989800601200', 'SCREW, BIND', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-bind', NULL, NULL, 0, 0, 0, '', 0),
(515, '989800601400', 'SCREW, BIND', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-bind', NULL, NULL, 0, 0, 0, '', 0),
(516, '901540580200', 'SCREW, BINDING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-binding', NULL, NULL, 0, 0, 0, '', 0),
(517, '901510681800', 'SCREW, COUNTERSUNK', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-countersunk', NULL, NULL, 0, 0, 0, '', 0),
(518, '901570681100', 'SCREW, CROSSRECESS', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-crossrecess', NULL, NULL, 0, 0, 0, '', 0),
(519, '901550680000', 'SCREW, FLAT FILLISTER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-flat-fillister', NULL, NULL, 0, 0, 0, '', 0),
(520, '901530680300', 'SCREW, HEXAGON', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-hexagon', NULL, NULL, 0, 0, 0, '', 0),
(521, '901530880400', 'SCREW, HEXAGON', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-hexagon', NULL, NULL, 0, 0, 0, '', 0),
(522, '5VLH353S0000', 'SCREW, KNOB FITTING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-knob-fitting', NULL, NULL, 0, 0, 0, '', 0),
(523, '3D9H33331000', 'SCREW, LENS FITTING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-lens-fitting', NULL, NULL, 0, 0, 0, '', 0),
(524, '3D9H47241000', 'SCREW, LENS FITTING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-lens-fitting', NULL, NULL, 0, 0, 0, '', 0),
(525, '30S143250000', 'SCREW, PAN HEAD', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(526, '5LBE42140000', 'SCREW, PAN HEAD', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(527, '901570380000', 'SCREW, PAN HEAD', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(528, '978010401000', 'SCREW, PAN HEAD', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(529, '985020501400', 'SCREW, PAN HEAD', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(530, '985020505000', 'SCREW, PAN HEAD', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(531, '985070502500', 'SCREW, PAN HEAD', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(532, '985070504300', 'SCREW, PAN HEAD', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(533, '985120601000', 'SCREW, PAN HEAD', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(534, '985120601200', 'SCREW, PAN HEAD', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(535, '985170503000', 'SCREW, PAN HEAD', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(536, '985800300800', 'SCREW, PAN HEAD', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head', NULL, NULL, 0, 0, 0, '', 0),
(537, '976020622000', 'SCREW, PAN HEAD W/WASHER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-pan-head-wwasher', NULL, NULL, 0, 0, 0, '', 0),
(538, '1JPE63370000', 'SCREW, SPRING', 1, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-spring', NULL, NULL, 0, 0, 0, '', 0),
(539, '977025001200', 'SCREW, TAPPING', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-tapping', NULL, NULL, 0, 0, 0, '', 0),
(540, '5VLW12150000', 'SCREW, VALVE ADJUSTING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-valve-adjusting', NULL, NULL, 0, 0, 0, '', 0),
(541, '901590583700', 'SCREW, WITH WASHER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(542, '901590682000', 'SCREW, WITH WASHER', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'screw-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(543, '2D0E44520000', 'SEAL', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seal', NULL, NULL, 0, 0, 0, '', 0),
(544, '2D0E44620000', 'SEAL', 1, '323.0000', '390.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seal', NULL, NULL, 0, 0, 0, '', 0),
(545, '5VLF45340100', 'SEAL, COCK', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seal-cock', NULL, NULL, 0, 0, 0, '', 0),
(546, '1STF31440000', 'SEAL, DUST', 1, '423.0000', '510.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seal-dust', NULL, NULL, 0, 0, 0, '', 0),
(547, '5VLF21511000', 'SEAL, GUARD', 1, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seal-guard', NULL, NULL, 0, 0, 0, '', 0),
(548, '5VLE21190000', 'SEAL, VALVE STEM', 1, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seal-valve-stem', NULL, NULL, 0, 0, 0, '', 0),
(549, '5VLE21160100', 'SEAT, VALVE SPRING', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'seat-valve-spring', NULL, NULL, 0, 0, 0, '', 0),
(550, '1BKE81850000', 'SEGMENT', 1, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'segment', NULL, NULL, 0, 0, 0, '', 0),
(551, '18CH57520100', 'SENDER UNIT ASSY,FUEL METER', 1, '746.0000', '900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'sender-unit-assyfuel-meter', NULL, NULL, 0, 0, 0, '', 0),
(552, '5VLE55210000', 'SHAFT 1', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shaft-1', NULL, NULL, 0, 0, 0, '', 0),
(553, '2D0F71120200', 'SHAFT, MAIN STAND', 1, '406.0000', '490.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shaft-main-stand', NULL, NULL, 0, 0, 0, '', 0),
(554, '5VLF21410100', 'SHAFT, PIVOT', 1, '812.0000', '980.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shaft-pivot', NULL, NULL, 0, 0, 0, '', 0),
(555, '5VLE21560000', 'SHAFT, ROCKER 2', 1, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shaft-rocker-2', NULL, NULL, 0, 0, 0, '', 0),
(556, '5VLE81101200', 'SHIFT PEDAL ASSY', 1, '1159.0000', '1330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shift-pedal-assy', NULL, NULL, 0, 0, 0, '', 0),
(557, '5VLE81010200', 'SHIFT SHAFT ASSY', 1, '1255.0000', '1440.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shift-shaft-assy', NULL, NULL, 0, 0, 0, '', 0),
(558, '3S9F22102000', 'SHOCK ABSORBER ASSY, REAR', 1, '5318.0000', '6100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'shock-absorber-assy-rear', NULL, NULL, 0, 0, 0, '', 0),
(559, '13RH35090000', 'SOCKET CORD ASSY', 1, '1098.0000', '1260.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'socket-cord-assy', NULL, NULL, 0, 0, 0, '', 0),
(560, '4FPE56660000', 'SPACER', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spacer', NULL, NULL, 0, 0, 0, '', 0),
(561, '5HHF31181000', 'SPACER', 1, '1465.0000', '1680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spacer', NULL, NULL, 0, 0, 0, '', 0),
(562, '905601529300', 'SPACER', 1, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spacer', NULL, NULL, 0, 0, 0, '', 0),
(563, '4GUF31730100', 'SPINDLE, TAPER', 1, '282.0000', '340.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spindle-taper', NULL, NULL, 0, 0, 0, '', 0),
(564, '4P2E42860000', 'SPRING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring', NULL, NULL, 0, 0, 0, '', 0),
(565, '5VLE83370000', 'SPRING', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring', NULL, NULL, 0, 0, 0, '', 0),
(566, '1JPE63330000', 'SPRING, CLUTCH 1', 1, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-clutch-1', NULL, NULL, 0, 0, 0, '', 0),
(567, '905010680500', 'SPRING, COMPRESSION', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-compression', NULL, NULL, 0, 0, 0, '', 0),
(568, '905011080600', 'SPRING, COMPRESSION', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-compression', NULL, NULL, 0, 0, 0, '', 0),
(569, '5VLF31414000', 'SPRING, FRONT FORK', 1, '307.0000', '370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-front-fork', NULL, NULL, 0, 0, 0, '', 0),
(570, '3D9F58650100', 'SPRING, LEVER', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-lever', NULL, NULL, 0, 0, 0, '', 0),
(571, '905061280100', 'SPRING, TENSION', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-tension', NULL, NULL, 0, 0, 0, '', 0),
(572, '905061580200', 'SPRING, TENSION', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-tension', NULL, NULL, 0, 0, 0, '', 0),
(573, '905062980000', 'SPRING, TENSION', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-tension', NULL, NULL, 0, 0, 0, '', 0),
(574, '905063280000', 'SPRING, TENSION', 1, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-tension', NULL, NULL, 0, 0, 0, '', 0),
(575, '905081680000', 'SPRING, TORSION', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-torsion', NULL, NULL, 0, 0, 0, '', 0),
(576, '905082683600', 'SPRING, TORSION', 1, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-torsion', NULL, NULL, 0, 0, 0, '', 0),
(577, '905083283700', 'SPRING, TORSION', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-torsion', NULL, NULL, 0, 0, 0, '', 0),
(578, '1SFE21130000', 'SPRING, VALVE INNER', 1, '439.0000', '530.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'spring-valve-inner', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(579, '5VLE21761000', 'SPROCKET, CAM CHAIN', 1, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'sprocket-cam-chain', NULL, NULL, 0, 0, 0, '', 0),
(580, '5VLF54450100', 'SPROCKET, DRIVEN (45T)', 1, '1552.0000', '1780.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'sprocket-driven-45t', NULL, NULL, 0, 0, 0, '', 0),
(581, '2TBF71110000', 'STAND, MAIN', 1, '950.0000', '1090.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stand-main', NULL, NULL, 0, 0, 0, '', 0),
(582, '2TBF73110000', 'STAND, SIDE', 1, '340.0000', '410.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stand-side', NULL, NULL, 0, 0, 0, '', 0),
(583, '4FPE55800000', 'STARTER CLUTCH OUTER ASSY', 1, '3539.0000', '4060.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'starter-clutch-outer-assy', NULL, NULL, 0, 0, 0, '', 0),
(584, '5VLH19400100', 'STARTER RELAY ASSY', 1, '1177.0000', '1350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'starter-relay-assy', NULL, NULL, 0, 0, 0, '', 0),
(585, '1SFH14102000', 'STATOR ASSY', 1, '3278.0000', '3760.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stator-assy', NULL, NULL, 0, 0, 0, '', 0),
(586, '2TBH14100000', 'STATOR ASSY', 1, '3670.0000', '4210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stator-assy', NULL, NULL, 0, 0, 0, '', 0),
(587, '18CF42160000', 'STAY', 1, '738.0000', '890.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay', NULL, NULL, 0, 0, 0, '', 0),
(588, '18CF474K0100', 'STAY 3', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-3', NULL, NULL, 0, 0, 0, '', 0),
(589, '18CF474L0100', 'STAY 4', 1, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-4', NULL, NULL, 0, 0, 0, '', 0),
(590, '5VLF13160100', 'STAY, ENGINE 2', 1, '1299.0000', '1490.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-engine-2', NULL, NULL, 0, 0, 0, '', 0),
(591, '2TBF13170000', 'STAY, ENGINE 3', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-engine-3', NULL, NULL, 0, 0, 0, '', 0),
(592, '2TBF13180000', 'STAY, ENGINE 4', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-engine-4', NULL, NULL, 0, 0, 0, '', 0),
(593, '18CF15130100', 'STAY, FENDER 1', 1, '976.0000', '1120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-fender-1', NULL, NULL, 0, 0, 0, '', 0),
(594, '18CF31740000', 'STAY, HEADLIGHT', 1, '1918.0000', '2200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stay-headlight', NULL, NULL, 0, 0, 0, '', 0),
(595, '3S9H25300100', 'STOP SWITCH ASSY', 1, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stop-switch-assy', NULL, NULL, 0, 0, 0, '', 0),
(596, '5VLE81400000', 'STOPPER LEVER ASSY', 1, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stopper-lever-assy', NULL, NULL, 0, 0, 0, '', 0),
(597, '2TBF743H0000', 'STOPPER, 3', 1, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'stopper-3', NULL, NULL, 0, 0, 0, '', 0),
(598, '2D0E34110000', 'STRAINER, OIL', 1, '439.0000', '530.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'strainer-oil', NULL, NULL, 0, 0, 0, '', 0),
(599, '5LBE434A0000', 'SUCTION PISTON ASSY', 1, '1098.0000', '1260.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'suction-piston-assy', NULL, NULL, 0, 0, 0, '', 0),
(600, '4S5F59190000', 'SUPPORT, PAD', 1, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'support-pad', NULL, NULL, 0, 0, 0, '', 0),
(601, '2SBH29170000', 'SWITCH', 1, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'switch', NULL, NULL, 0, 0, 0, '', 0),
(602, '18CH39731100', 'SWITCH, HANDLE 3', 1, '2807.0000', '3220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'switch-handle-3', NULL, NULL, 0, 0, 0, '', 0),
(603, '5VLH39630200', 'SWITCH, HANDLE 3', 1, '1011.0000', '1160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'switch-handle-3', NULL, NULL, 0, 0, 0, '', 0),
(604, '18CH47000100', 'TAILLIGHT ASSY', 1, '3147.0000', '3610.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'taillight-assy', NULL, NULL, 0, 0, 0, '', 0),
(605, '2MPE22100000', 'TENSIONER ASSY, CAM CHAIN', 1, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'tensioner-assy-cam-chain', NULL, NULL, 0, 0, 0, '', 0),
(606, '4P2W149K0000', 'THROTTLE SCREW SET', 1, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'throttle-screw-set', NULL, NULL, 0, 0, 0, '', 0),
(607, '2TBF81000000', 'TOOL KIT', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'tool-kit', NULL, NULL, 0, 0, 0, '', 0),
(608, '942091880200', 'TUBE', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'tube', NULL, NULL, 0, 0, 0, '', 0),
(609, '942271882100', 'TUBE', 1, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'tube', NULL, NULL, 0, 0, 0, '', 0),
(610, '2TBF411B0000', 'TUNING FORK MARK', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'tuning-fork-mark', NULL, NULL, 0, 0, 0, '', 0),
(611, '5VLF45230100', 'VALVE', 1, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'valve', NULL, NULL, 0, 0, 0, '', 0),
(612, '5VLE21210000', 'VALVE, EXHAUST', 1, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'valve-exhaust', NULL, NULL, 0, 0, 0, '', 0),
(613, '5VLE21110000', 'VALVE, INTAKE', 1, '514.0000', '620.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'valve-intake', NULL, NULL, 0, 0, 0, '', 0),
(614, '5APF34270000', 'WASHER', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer', NULL, NULL, 0, 0, 0, '', 0),
(615, '5LKF54120100', 'WASHER', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer', NULL, NULL, 0, 0, 0, '', 0),
(616, '902080705200', 'WASHER, CONICAL SPRING', 1, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-conical-spring', NULL, NULL, 0, 0, 0, '', 0),
(617, '5VLF31650100', 'WASHER, FELT', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-felt', NULL, NULL, 0, 0, 0, '', 0),
(618, '902151221100', 'WASHER, LOCK', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-lock', NULL, NULL, 0, 0, 0, '', 0),
(619, '902010580200', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(620, '902010580300', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(621, '902010580400', 'WASHER, PLATE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(622, '902010686100', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(623, '90201068C200', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(624, '902010764100', 'WASHER, PLATE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(625, '902010884000', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(626, '902010887700', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(627, '90201088C300', 'WASHER, PLATE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(628, '90201088P200', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(629, '902011083100', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(630, '90201108C700', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(631, '90201108J900', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(632, '90201122H100', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(633, '902011480900', 'WASHER, PLATE', 1, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(634, '90201152H700', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(635, '90201154E800', 'WASHER, PLATE', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(636, '902011581500', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(637, '902012027200', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(638, '90201208D400', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(639, '90201208D500', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(640, '90201228F600', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(641, '90201258D700', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(642, '902020518000', 'WASHER, PLATE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(643, '902020680300', 'WASHER, PLATE', 1, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(644, '902020980000', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(645, '902022614201', 'WASHER, PLATE', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(646, '902012026600', 'WASHER, PLATE (T=1.0)', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-plate-t10', NULL, NULL, 0, 0, 0, '', 0),
(647, '13RH35240000', 'WASHER, SPECIAL', 1, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-special', NULL, NULL, 0, 0, 0, '', 0),
(648, '5VLF82150100', 'WASHER, SPECIAL', 1, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-special', NULL, NULL, 0, 0, 0, '', 0),
(649, '929900620000', 'WASHER, WAVE', 1, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'washer-wave', NULL, NULL, 0, 0, 0, '', 0),
(650, '5VLE14540000', 'WEIGHT 1', 1, '1988.0000', '2280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'weight-1', NULL, NULL, 0, 0, 0, '', 0),
(651, '2TBH25900000', 'WIRE HARNESS ASSY', 1, '1273.0000', '1460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'wire-harness-assy', NULL, NULL, 0, 0, 0, '', 0),
(652, '5VLH21160000', 'WIRE, MINUS LEAD', 1, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'wire-minus-lead', NULL, NULL, 0, 0, 0, '', 0),
(653, '2TBH21150000', 'WIRE, PLUS LEAD', 1, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 1, NULL, 'wire-plus-lead', NULL, NULL, 0, 0, 0, '', 0),
(654, '2TBXF47520P1', 'COVER, TAIL', 2, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 2, 2, NULL, 'cover-tail', NULL, NULL, 0, 0, 0, '', 0),
(655, '2TBH55460000', 'BAND', 3, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'band', NULL, NULL, 0, 0, 0, '', 0),
(656, '93306204XU00', 'BEARING', 3, '439.0000', '530.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(657, '2TBF835120P4', 'BODY, COWLING', 3, '1090.0000', '1250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'body-cowling', NULL, NULL, 0, 0, 0, '', 0),
(658, '975220652500', 'BOLT, WITH WASHER', 3, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'bolt-with-washer', NULL, NULL, 0, 0, 0, '', 0),
(659, '4S5W00470100', 'CALIPER SEAL KIT', 3, '705.0000', '850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'caliper-seal-kit', NULL, NULL, 0, 0, 0, '', 0),
(660, '2MPF58520000', 'CAP, RESERVOIR', 3, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cap-reservoir', NULL, NULL, 0, 0, 0, '', 0),
(661, '2TBF511X0000', 'COLLAR', 3, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(662, '2TBE11860000', 'COVER, CYLINDER HEAD SIDE 2', 3, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-cylinder-head-side-2', NULL, NULL, 0, 0, 0, '', 0),
(663, '2TBE11850000', 'COVER, CYLINDER HEAD SIDE 3', 3, '481.0000', '580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-cylinder-head-side-3', NULL, NULL, 0, 0, 0, '', 0),
(664, '2TBXF47520P0', 'COVER, TAIL', 3, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-tail', NULL, NULL, 0, 0, 0, '', 0),
(665, '2TBXF47520P3', 'COVER, TAIL', 3, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-tail', NULL, NULL, 0, 0, 0, '', 0),
(666, '2TBXF47520P4', 'COVER, TAIL', 3, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-tail', NULL, NULL, 0, 0, 0, '', 0),
(667, '2TBF471K20P4', 'COVER, TAIL 1', 3, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-tail-1', NULL, NULL, 0, 0, 0, '', 0),
(668, '2TBF472K20P4', 'COVER, TAIL 2', 3, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cover-tail-2', NULL, NULL, 0, 0, 0, '', 0),
(669, '2TBF34350033', 'CROWN, HANDLE', 3, '1055.0000', '1210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'crown-handle', NULL, NULL, 0, 0, 0, '', 0),
(670, '2MPW00410000', 'CYLINDER KIT, MASTER', 3, '580.0000', '700.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'cylinder-kit-master', NULL, NULL, 0, 0, 0, '', 0),
(671, '2TBF83710000', 'DAMPER', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(672, '2TBF151120P4', 'FENDER, FRONT', 3, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'fender-front', NULL, NULL, 0, 0, 0, '', 0),
(673, '2TBXCF4110P3', 'FUEL TANK COMP.', 3, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(674, '2TBXCF4110P4', 'FUEL TANK COMP.', 3, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(675, '2TBXCF4111P0', 'FUEL TANK COMP.', 3, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(676, '2TBE54510100', 'GASKET, CRANKCASECOVER 1', 3, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'gasket-crankcasecover-1', NULL, NULL, 0, 0, 0, '', 0),
(677, '2TBE54610000', 'GASKET, CRANKCASECOVER 2', 3, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'gasket-crankcasecover-2', NULL, NULL, 0, 0, 0, '', 0),
(678, '2TBE33290000', 'GASKET, PUMP COVER', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'gasket-pump-cover', NULL, NULL, 0, 0, 0, '', 0),
(679, '2TBH47040000', 'GASKET, TAILLIGHT', 3, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'gasket-taillight', NULL, NULL, 0, 0, 0, '', 0),
(680, '2TBE22130000', 'GASKET, TENSIONERCASE', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'gasket-tensionercase', NULL, NULL, 0, 0, 0, '', 0),
(681, '2TBF42554000', 'GRAPHIC, FUEL TANK8', 3, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(682, '2TBF42555000', 'GRAPHIC, FUEL TANK8', 3, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(683, '2TBF42556000', 'GRAPHIC, FUEL TANK8', 3, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(684, '2TBF42564000', 'GRAPHIC, FUEL TANK9', 3, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(685, '2TBF42565000', 'GRAPHIC, FUEL TANK9', 3, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(686, '2TBF42566000', 'GRAPHIC, FUEL TANK9', 3, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(687, '2RP217190000', 'GROMMET', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(688, '2TBH43032000', 'HEADLIGHT UNIT ASSY', 3, '1377.0000', '1580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'headlight-unit-assy', NULL, NULL, 0, 0, 0, '', 0),
(689, '2TBF34410000', 'HOLDER, HANDLE UPPER', 3, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'holder-handle-upper', NULL, NULL, 0, 0, 0, '', 0),
(690, '2TBH333A0000', 'LENS COMP 1', 3, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'lens-comp-1', NULL, NULL, 0, 0, 0, '', 0),
(691, '2TBH47210000', 'LENS, TAILLIGHT', 3, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'lens-taillight', NULL, NULL, 0, 0, 0, '', 0),
(692, '5VLE21181000', 'LOCK, VALVE SPRINGRETAINER', 3, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'lock-valve-springretainer', NULL, NULL, 0, 0, 0, '', 0),
(693, '2MPW583T0000', 'MASTER CYLINDER ASSY', 3, '4237.0000', '4860.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'master-cylinder-assy', NULL, NULL, 0, 0, 0, '', 0),
(694, '2TBH35000000', 'METER ASSY', 3, '3095.0000', '3550.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'meter-assy', NULL, NULL, 0, 0, 0, '', 0),
(695, '2TBE47032100', 'MUFFLER COMP. 1', 3, '8094.0000', '8850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'muffler-comp-1', NULL, NULL, 0, 0, 0, '', 0),
(696, '2TBE11660000', 'PIPE, BREATHER 1', 3, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'pipe-breather-1', NULL, NULL, 0, 0, 0, '', 0),
(697, '2TBF43110000', 'PIPE, FUEL 1', 3, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'pipe-fuel-1', NULL, NULL, 0, 0, 0, '', 0),
(698, '4S5W00570100', 'PISTON ASSY, CALIPER', 3, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'piston-assy-caliper', NULL, NULL, 0, 0, 0, '', 0),
(699, 'BC8F53210033', 'PLATE, BRAKE SHOE', 3, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'plate-brake-shoe', NULL, NULL, 0, 0, 0, '', 0),
(700, '4KLF585B0000', 'PLATE, DIAPHRAGM', 3, '149.0000', '180.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'plate-diaphragm', NULL, NULL, 0, 0, 0, '', 0),
(701, '2TBF41410000', 'PROTECTOR, FUEL TANK', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'protector-fuel-tank', NULL, NULL, 0, 0, 0, '', 0),
(702, '2TBWF37W24P3', 'SCOOP, AIR 1', 3, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(703, '2TBWF37W25P3', 'SCOOP, AIR 1', 3, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(704, '2TBWF37X24P3', 'SCOOP, AIR 2', 3, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(705, '2TBWF37X25P3', 'SCOOP, AIR 2', 3, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(706, '2MPF589H0000', 'SCREW', 3, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(707, '2TBH33330000', 'SCREW, LENS FITTING', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'screw-lens-fitting', NULL, NULL, 0, 0, 0, '', 0),
(708, '2TBH47240000', 'SCREW, LENS FITTING', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'screw-lens-fitting', NULL, NULL, 0, 0, 0, '', 0),
(709, '977025001600', 'SCREW, TAPPING', 3, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'screw-tapping', NULL, NULL, 0, 0, 0, '', 0),
(710, '2MPE55210000', 'SHAFT 1', 3, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'shaft-1', NULL, NULL, 0, 0, 0, '', 0),
(711, '2TBH35500000', 'SPEEDOMETER CABLEASSY', 3, '182.0000', '220.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'speedometer-cableassy', NULL, NULL, 0, 0, 0, '', 0),
(712, '938221483300', 'SPROCKET, DRIVE', 3, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'sprocket-drive', NULL, NULL, 0, 0, 0, '', 0),
(713, '2TBE34110000', 'STRAINER, OIL', 3, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'strainer-oil', NULL, NULL, 0, 0, 0, '', 0),
(714, '1JPE21210000', 'VALVE, EXHAUST', 3, '555.0000', '670.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'valve-exhaust', NULL, NULL, 0, 0, 0, '', 0),
(715, '1JPE21110000', 'VALVE, INTAKE', 3, '390.0000', '470.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'valve-intake', NULL, NULL, 0, 0, 0, '', 0),
(716, '2TBF83810000', 'WINDSHIELD', 3, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'windshield', NULL, NULL, 0, 0, 0, '', 0),
(717, '2TBH25901000', 'WIRE HARNESS ASSY', 3, '1377.0000', '1580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 3, 3, NULL, 'wire-harness-assy', NULL, NULL, 0, 0, 0, '', 0),
(718, '903871086300', 'COLLAR', 4, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(719, '903871500N00', 'COLLAR', 4, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(720, '2TBE54110000', 'COVER, CRANKCASE 1', 4, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'cover-crankcase-1', NULL, NULL, 0, 0, 0, '', 0),
(721, '2TBE54210000', 'COVER, CRANKCASE 2', 4, '1221.0000', '1400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'cover-crankcase-2', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(722, '2TBF51180000', 'COVER, HUB DUST', 4, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'cover-hub-dust', NULL, NULL, 0, 0, 0, '', 0),
(723, '2TBE55170000', 'GEAR, IDLER 2', 4, '415.0000', '500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'gear-idler-2', NULL, NULL, 0, 0, 0, '', 0),
(724, '2TBE56510000', 'GEAR, KICK IDLE', 4, '332.0000', '400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'gear-kick-idle', NULL, NULL, 0, 0, 0, '', 0),
(725, '2TBE33240000', 'GEAR, PUMP DRIVE', 4, '174.0000', '210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'gear-pump-drive', NULL, NULL, 0, 0, 0, '', 0),
(726, '2TBE48820000', 'HOSE, BEND 2', 4, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'hose-bend-2', NULL, NULL, 0, 0, 0, '', 0),
(727, '2TBE53620000', 'PLUG, OIL LEVEL', 4, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'plug-oil-level', NULL, NULL, 0, 0, 0, '', 0),
(728, '902690707700', 'RIVET', 4, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'rivet', NULL, NULL, 0, 0, 0, '', 0),
(729, '2TBH39750000', 'SWITCH, HANDLE 2', 4, '464.0000', '560.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'switch-handle-2', NULL, NULL, 0, 0, 0, '', 0),
(730, '2TBH39730000', 'SWITCH, HANDLE 3', 4, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 4, 4, NULL, 'switch-handle-3', NULL, NULL, 0, 0, 0, '', 0),
(731, '2B9F31920100', 'BAND', 5, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'band', NULL, NULL, 0, 0, 0, '', 0),
(732, '901090681800', 'BOLT', 5, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(733, '901090681900', 'BOLT', 5, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(734, '958020401600', 'BOLT, FLANGE', 5, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(735, '2B9F31910000', 'BOOT', 5, '605.0000', '730.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'boot', NULL, NULL, 0, 0, 0, '', 0),
(736, '4FPF63421000', 'BOOT, CABLE', 5, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'boot-cable', NULL, NULL, 0, 0, 0, '', 0),
(737, 'B83F334G1000', 'BRACKET 1', 5, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'bracket-1', NULL, NULL, 0, 0, 0, '', 0),
(738, '2TB843472000', 'BULB', 5, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'bulb', NULL, NULL, 0, 0, 0, '', 0),
(739, '2TB843142000', 'BULB, HEADLIGHT', 5, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'bulb-headlight', NULL, NULL, 0, 0, 0, '', 0),
(740, 'B83F63350000', 'CABLE, CLUTCH', 5, '240.0000', '290.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'cable-clutch', NULL, NULL, 0, 0, 0, '', 0),
(741, 'B83F63111100', 'CABLE, THROTTLE 1', 5, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'cable-throttle-1', NULL, NULL, 0, 0, 0, '', 0),
(742, '903870686400', 'COLLAR', 5, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(743, '90387068K000', 'COLLAR', 5, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(744, '2B9F15440100', 'COLLAR 1', 5, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'collar-1', NULL, NULL, 0, 0, 0, '', 0),
(745, '2B9F14720100', 'COLLAR, PROTECTOR', 5, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'collar-protector', NULL, NULL, 0, 0, 0, '', 0),
(746, '2TBH43122000', 'CORD ASSY', 5, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'cord-assy', NULL, NULL, 0, 0, 0, '', 0),
(747, '2B9F61431000', 'COVER, HANDLE UPPER 1', 5, '423.0000', '510.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'cover-handle-upper-1', NULL, NULL, 0, 0, 0, '', 0),
(748, 'B83XF17200P0', 'COVER, SIDE 2', 5, '1308.0000', '1500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'cover-side-2', NULL, NULL, 0, 0, 0, '', 0),
(749, '2TBH43972000', 'COVER, SOCKET', 5, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'cover-socket', NULL, NULL, 0, 0, 0, '', 0),
(750, '5VLF34350035', 'CROWN, HANDLE', 5, '2511.0000', '2880.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'crown-handle', NULL, NULL, 0, 0, 0, '', 0),
(751, 'B83F33950000', 'EMBLEM', 5, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'emblem', NULL, NULL, 0, 0, 0, '', 0),
(752, '992360012000', 'EMBLEM, YAMAHA', 5, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'emblem-yamaha', NULL, NULL, 0, 0, 0, '', 0),
(753, 'B83F15110000', 'FENDER, FRONT A', 5, '547.0000', '660.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'fender-front-a', NULL, NULL, 0, 0, 0, '', 0),
(754, 'B83F15111000', 'FENDER, FRONT B', 5, '547.0000', '660.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'fender-front-b', NULL, NULL, 0, 0, 0, '', 0),
(755, '46SF74110000', 'FOOTREST 1', 5, '2336.0000', '2680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'footrest-1', NULL, NULL, 0, 0, 0, '', 0),
(756, 'B83F74110000', 'FOOTREST 1', 5, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'footrest-1', NULL, NULL, 0, 0, 0, '', 0),
(757, 'B83F15560000', 'FRONT FENDER 2', 5, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'front-fender-2', NULL, NULL, 0, 0, 0, '', 0),
(758, 'B83F31020000', 'FRONT FORK ASSY (L.H)', 5, '5571.0000', '6390.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'front-fork-assy-lh', NULL, NULL, 0, 0, 0, '', 0),
(759, 'B83F31030000', 'FRONT FORK ASSY (R.H)', 5, '5571.0000', '6390.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'front-fork-assy-rh', NULL, NULL, 0, 0, 0, '', 0),
(760, 'B83XCF4100P0', 'FUEL TANK COMP.', 5, '7499.0000', '8200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(761, 'B83XCF4110P3', 'FUEL TANK COMP. D', 5, '7938.0000', '8680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'fuel-tank-comp-d', NULL, NULL, 0, 0, 0, '', 0),
(762, 'B83F42550000', 'GRAPHIC', 5, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic', NULL, NULL, 0, 0, 0, '', 0),
(763, 'B83F42551000', 'GRAPHIC', 5, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic', NULL, NULL, 0, 0, 0, '', 0),
(764, 'B83F42560000', 'GRAPHIC', 5, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic', NULL, NULL, 0, 0, 0, '', 0),
(765, 'B83F42561000', 'GRAPHIC', 5, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic', NULL, NULL, 0, 0, 0, '', 0),
(766, 'B83F42440000', 'GRAPHIC, FUEL TANK1', 5, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(767, 'B83F42441000', 'GRAPHIC, FUEL TANK1', 5, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(768, 'B83F42450000', 'GRAPHIC, FUEL TANK2', 5, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(769, 'B83F42451000', 'GRAPHIC, FUEL TANK2', 5, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(770, '2B9F61410000', 'GUARD, BRUSH 1', 5, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guard-brush-1', NULL, NULL, 0, 0, 0, '', 0),
(771, '2B9F61412000', 'GUARD, BRUSH 1', 5, '506.0000', '610.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guard-brush-1', NULL, NULL, 0, 0, 0, '', 0),
(772, '2B9F61420000', 'GUARD, BRUSH 2', 5, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guard-brush-2', NULL, NULL, 0, 0, 0, '', 0),
(773, '2B9F61424000', 'GUARD, BRUSH 2', 5, '497.0000', '600.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guard-brush-2', NULL, NULL, 0, 0, 0, '', 0),
(774, '46SH41690000', 'GUARD, HEADLIGHT', 5, '2110.0000', '2420.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guard-headlight', NULL, NULL, 0, 0, 0, '', 0),
(775, 'B83H41690000', 'GUARD, HEADLIGHT', 5, '572.0000', '690.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guard-headlight', NULL, NULL, 0, 0, 0, '', 0),
(776, 'B83F33890000', 'GUIDE, CABLE', 5, '174.0000', '210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'guide-cable', NULL, NULL, 0, 0, 0, '', 0),
(777, 'B83F61110000', 'HANDLEBAR', 5, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'handlebar', NULL, NULL, 0, 0, 0, '', 0),
(778, 'B83H43000000', 'HEADLIGHT ASSY', 5, '2049.0000', '2350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'headlight-assy', NULL, NULL, 0, 0, 0, '', 0),
(779, 'B83H43030000', 'HEADLIGHT UNIT ASSY', 5, '1613.0000', '1850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'headlight-unit-assy', NULL, NULL, 0, 0, 0, '', 0),
(780, '3S9F34410000', 'HOLDER, HANDLE UPPER', 5, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'holder-handle-upper', NULL, NULL, 0, 0, 0, '', 0),
(781, 'B83F34410000', 'HOLDER, HANDLE UPPER', 5, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'holder-handle-upper', NULL, NULL, 0, 0, 0, '', 0),
(782, '2B9F58722200', 'HOSE, BRAKE 1', 5, '1735.0000', '1990.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'hose-brake-1', NULL, NULL, 0, 0, 0, '', 0),
(783, 'B83F31100000', 'INNER TUBE COMP.1', 5, '2764.0000', '3170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'inner-tube-comp1', NULL, NULL, 0, 0, 0, '', 0),
(784, '2TBE47031100', 'MUFFLER COMP. 1', 5, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'muffler-comp-1', NULL, NULL, 0, 0, 0, '', 0),
(785, '2XPW149J0000', 'NEEDLE SET', 5, '1369.0000', '1570.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'needle-set', NULL, NULL, 0, 0, 0, '', 0),
(786, '953020880000', 'NUT, CROWN', 5, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'nut-crown', NULL, NULL, 0, 0, 0, '', 0),
(787, 'B83F31060000', 'OUTER TUBE COMP.', 5, '1613.0000', '1850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'outer-tube-comp', NULL, NULL, 0, 0, 0, '', 0),
(788, 'B83F31070000', 'OUTER TUBE COMP. (R.H)', 5, '1656.0000', '1900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'outer-tube-comp-rh', NULL, NULL, 0, 0, 0, '', 0),
(789, '2TBF47410000', 'PAD, SEAT', 5, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'pad-seat', NULL, NULL, 0, 0, 0, '', 0),
(790, 'B83F14710000', 'PROTECTOR, ENGINE', 5, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'protector-engine', NULL, NULL, 0, 0, 0, '', 0),
(791, '2B9F61240000', 'PROTECTOR, HANDLEBAR', 5, '1151.0000', '1320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'protector-handlebar', NULL, NULL, 0, 0, 0, '', 0),
(792, 'B83XF37W00P0', 'SCOOP, AIR 1', 5, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(793, 'B83XF37W10P3', 'SCOOP, AIR 1', 5, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(794, 'B83XF37X00P0', 'SCOOP, AIR 2', 5, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(795, 'B83XF37X10P3', 'SCOOP, AIR 2', 5, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(796, '901490580600', 'SCREW', 5, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(797, 'B83F15750000', 'SEAL 1', 5, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'seal-1', NULL, NULL, 0, 0, 0, '', 0),
(798, '5VLE81100100', 'SHIFT PEDAL ASSY', 5, '754.0000', '910.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'shift-pedal-assy', NULL, NULL, 0, 0, 0, '', 0),
(799, '2B9F31180000', 'SPACER', 5, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'spacer', NULL, NULL, 0, 0, 0, '', 0),
(800, '2B9H43280100', 'SPACER', 5, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'spacer', NULL, NULL, 0, 0, 0, '', 0),
(801, 'B83H35500000', 'SPEEDOMETER CABLE ASSY', 5, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'speedometer-cable-assy', NULL, NULL, 0, 0, 0, '', 0),
(802, 'B83F71110000', 'STAND, MAIN', 5, '950.0000', '1090.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'stand-main', NULL, NULL, 0, 0, 0, '', 0),
(803, 'B83F73110000', 'STAND, SIDE', 5, '381.0000', '460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'stand-side', NULL, NULL, 0, 0, 0, '', 0),
(804, '1BKE55800000', 'STARTER CLUTCH OUTER ASSY', 5, '2118.0000', '2430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'starter-clutch-outer-assy', NULL, NULL, 0, 0, 0, '', 0),
(805, '2B9F13160000', 'STAY, ENGINE 2', 5, '1046.0000', '1200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'stay-engine-2', NULL, NULL, 0, 0, 0, '', 0),
(806, 'B83F13160000', 'STAY, ENGINE 2', 5, '381.0000', '460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'stay-engine-2', NULL, NULL, 0, 0, 0, '', 0),
(807, 'B83F15140000', 'STAY, FENDER 2', 5, '158.0000', '190.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'stay-fender-2', NULL, NULL, 0, 0, 0, '', 0),
(808, 'B83F31740000', 'STAY, HEADLIGHT', 5, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'stay-headlight', NULL, NULL, 0, 0, 0, '', 0),
(809, '942301881100', 'TUBE', 5, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'tube', NULL, NULL, 0, 0, 0, '', 0),
(810, '902010684400', 'WASHER, PLATE', 5, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(811, '902010886700', 'WASHER, PLATE', 5, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(812, '46SF83810000', 'WINDSHIELD', 5, '3095.0000', '3550.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'windshield', NULL, NULL, 0, 0, 0, '', 0),
(813, 'B83H25900000', 'WIRE HARNESS ASSY', 5, '1282.0000', '1470.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 5, 5, NULL, 'wire-harness-assy', NULL, NULL, 0, 0, 0, '', 0),
(814, '2TBF41910000', 'BRKT.,FUEL TANK 1', 6, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'brktfuel-tank-1', NULL, NULL, 0, 0, 0, '', 0),
(815, '903870628700', 'COLLAR', 6, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(816, 'B83XF17110P0', 'COVER,SIDE 1', 6, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'coverside-1', NULL, NULL, 0, 0, 0, '', 0),
(817, 'B83XF17210P0', 'COVER,SIDE 2', 6, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'coverside-2', NULL, NULL, 0, 0, 0, '', 0),
(818, 'B83XF47520P0', 'COVER,TAIL', 6, '307.0000', '370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'covertail', NULL, NULL, 0, 0, 0, '', 0),
(819, 'B83XF47520P3', 'COVER,TAIL', 6, '307.0000', '370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'covertail', NULL, NULL, 0, 0, 0, '', 0),
(820, '2TBF471K20P0', 'COVER,TAIL 1', 6, '555.0000', '670.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'covertail-1', NULL, NULL, 0, 0, 0, '', 0),
(821, '2TBF471K20P3', 'COVER,TAIL 1', 6, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'covertail-1', NULL, NULL, 0, 0, 0, '', 0),
(822, '2TBF472K20P0', 'COVER,TAIL 2', 6, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'covertail-2', NULL, NULL, 0, 0, 0, '', 0),
(823, '2TBF472K20P3', 'COVER,TAIL 2', 6, '564.0000', '680.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'covertail-2', NULL, NULL, 0, 0, 0, '', 0),
(824, '2TBF411J0000', 'DAMPER,SIDE COVER', 6, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'damperside-cover', NULL, NULL, 0, 0, 0, '', 0),
(825, '2TBF47300100', 'DOUBLE SEAT ASSY', 6, '1831.0000', '2100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'double-seat-assy', NULL, NULL, 0, 0, 0, '', 0),
(826, '2TBF11101000', 'FRAME COMP.', 6, '10288.0000', '11250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'frame-comp', NULL, NULL, 0, 0, 0, '', 0),
(827, 'B83XCF4110P0', 'FUEL TANK COMP.', 6, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(828, 'B83XCF4111P3', 'FUEL TANK COMP.', 6, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(829, '904801482700', 'GROMMET', 6, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(830, '2TBF58860000', 'HOLDER,BRAKE HOSE', 6, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'holderbrake-hose', NULL, NULL, 0, 0, 0, '', 0),
(831, '2TBF58750000', 'HOLDER,BRAKE HOSE 1', 6, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'holderbrake-hose-1', NULL, NULL, 0, 0, 0, '', 0),
(832, '2TBF58760000', 'HOLDER,BRAKE HOSE 2', 6, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'holderbrake-hose-2', NULL, NULL, 0, 0, 0, '', 0),
(833, '2TBF58870000', 'HOLDER,BRAKE HOSE 3', 6, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'holderbrake-hose-3', NULL, NULL, 0, 0, 0, '', 0),
(834, '2TBH33300000', 'RR. FLASHER LIGHT ASSY.,1', 6, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'rr-flasher-light-assy1', NULL, NULL, 0, 0, 0, '', 0),
(835, '2TBH33400000', 'RR. FLASHER LIGHT ASSY.,2', 6, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'rr-flasher-light-assy2', NULL, NULL, 0, 0, 0, '', 0),
(836, 'B83XF37W20P0', 'SCOOP,AIR 1', 6, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'scoopair-1', NULL, NULL, 0, 0, 0, '', 0),
(837, 'B83XF37W21P3', 'SCOOP,AIR 1', 6, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'scoopair-1', NULL, NULL, 0, 0, 0, '', 0),
(838, 'B83XF37X20P0', 'SCOOP,AIR 2', 6, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'scoopair-2', NULL, NULL, 0, 0, 0, '', 0),
(839, 'B83XF37X21P3', 'SCOOP,AIR 2', 6, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'scoopair-2', NULL, NULL, 0, 0, 0, '', 0),
(840, '2TBF42161000', 'STAY', 6, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'stay', NULL, NULL, 0, 0, 0, '', 0),
(841, '2TBH47000000', 'TAIL LIGHT ASSY.', 6, '381.0000', '460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'tail-light-assy', NULL, NULL, 0, 0, 0, '', 0),
(842, 'B83H25901000', 'WIRE HARNESS ASSY.', 6, '1377.0000', '1580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 6, 6, NULL, 'wire-harness-assy', NULL, NULL, 0, 0, 0, '', 0),
(843, '950230803500', 'BOLT', 7, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(844, '90387068P200', 'COLLAR', 7, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(845, 'B83XF47520P4', 'COVER, TAIL', 7, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'cover-tail', NULL, NULL, 0, 0, 0, '', 0),
(846, 'B83F15112000', 'FENDER, FRONT', 7, '547.0000', '660.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'fender-front', NULL, NULL, 0, 0, 0, '', 0),
(847, 'B83XCF4112P0', 'FUEL TANK COMP.', 7, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(848, 'B83XCF4113P3', 'FUEL TANK COMP.', 7, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(849, 'B83XCF4114P4', 'FUEL TANK COMP.', 7, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(850, 'B83F42442000', 'GRAPHIC, FUEL TANK1', 7, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(851, 'B83F42443000', 'GRAPHIC, FUEL TANK1', 7, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(852, 'B83F42444000', 'GRAPHIC, FUEL TANK1', 7, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(853, 'B83F42452000', 'GRAPHIC, FUEL TANK2', 7, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(854, 'B83F42453000', 'GRAPHIC, FUEL TANK2', 7, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(855, 'B83F42454000', 'GRAPHIC, FUEL TANK2', 7, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(856, 'B83F42552000', 'GRAPHIC, FUEL TANK8', 7, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(857, 'B83F42553000', 'GRAPHIC, FUEL TANK8', 7, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(858, 'B83F42554000', 'GRAPHIC, FUEL TANK8', 7, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(859, 'B83F42562000', 'GRAPHIC, FUEL TANK9', 7, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(860, 'B83F42563000', 'GRAPHIC, FUEL TANK9', 7, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(861, 'B83F42564000', 'GRAPHIC, FUEL TANK9', 7, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(862, '903381480000', 'PLUG', 7, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'plug', NULL, NULL, 0, 0, 0, '', 0),
(863, 'B83XF37W22P0', 'SCOOP, AIR 1', 7, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(864, 'B83XF37W23P3', 'SCOOP, AIR 1', 7, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(865, 'B83XF37W24P4', 'SCOOP, AIR 1', 7, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(866, 'B83XF37X22P0', 'SCOOP, AIR 2', 7, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(867, 'B83XF37X23P3', 'SCOOP, AIR 2', 7, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(868, 'B83XF37X24P4', 'SCOOP, AIR 2', 7, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(869, 'BC8H33180000', 'STAY, FLASHER 1', 7, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 7, 7, NULL, 'stay-flasher-1', NULL, NULL, 0, 0, 0, '', 0),
(870, 'B83XF17110P2', 'COVER, SIDE 1', 8, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'cover-side-1', NULL, NULL, 0, 0, 0, '', 0),
(871, 'B83XF17210P2', 'COVER, SIDE 2', 8, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'cover-side-2', NULL, NULL, 0, 0, 0, '', 0),
(872, 'B83XF47520P2', 'COVER, TAIL', 8, '315.0000', '380.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'cover-tail', NULL, NULL, 0, 0, 0, '', 0),
(873, 'B83XCF4115P2', 'FUEL TANK COMP.', 8, '6538.0000', '7500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(874, 'B83F42445000', 'GRAPHIC, FUEL TANK1', 8, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(875, 'B83F42455000', 'GRAPHIC, FUEL TANK2', 8, '133.0000', '160.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(876, 'B83F42555000', 'GRAPHIC, FUEL TANK8', 8, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'graphic-fuel-tank8', NULL, NULL, 0, 0, 0, '', 0),
(877, 'B83F42565000', 'GRAPHIC, FUEL TANK9', 8, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'graphic-fuel-tank9', NULL, NULL, 0, 0, 0, '', 0),
(878, 'B83XF37W25P2', 'SCOOP, AIR 1', 8, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'scoop-air-1', NULL, NULL, 0, 0, 0, '', 0),
(879, 'B83XF37X25P2', 'SCOOP, AIR 2', 8, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 8, 8, NULL, 'scoop-air-2', NULL, NULL, 0, 0, 0, '', 0),
(880, '2TBW14150000', 'ADJUSTER SET', 9, '481.0000', '580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'adjuster-set', NULL, NULL, 0, 0, 0, '', 0),
(881, '2TBE44100000', 'AIR CLEANER ASSY.', 9, '1221.0000', '1400.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'air-cleaner-assy', NULL, NULL, 0, 0, 0, '', 0),
(882, '2TBE48030100', 'AIR INDUCTION SYSTEM ASSY', 9, '2249.0000', '2580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'air-induction-system-assy', NULL, NULL, 0, 0, 0, '', 0),
(883, 'BC8F53870000', 'AXLE, SPROCKET', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'axle-sprocket', NULL, NULL, 0, 0, 0, '', 0),
(884, '5VLF51812000', 'AXLE, WHEEL', 9, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'axle-wheel', NULL, NULL, 0, 0, 0, '', 0),
(885, 'BC8H55460000', 'BAND', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'band', NULL, NULL, 0, 0, 0, '', 0),
(886, '943251880000', 'BAND, RIM', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'band-rim', NULL, NULL, 0, 0, 0, '', 0),
(887, '93306252YM00', 'BEARING', 9, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(888, '933063010200', 'BEARING', 9, '788.0000', '950.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bearing', NULL, NULL, 0, 0, 0, '', 0),
(889, 'B83H43300000', 'BODY ASSY', 9, '307.0000', '370.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'body-assy', NULL, NULL, 0, 0, 0, '', 0),
(890, '950240803000', 'BOLT', 9, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bolt', NULL, NULL, 0, 0, 0, '', 0),
(891, '2D0F41760100', 'BOLT 1', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bolt-1', NULL, NULL, 0, 0, 0, '', 0),
(892, '950220803000', 'BOLT, FLANGE', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(893, '958170601400', 'BOLT, FLANGE', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(894, '958170801600', 'BOLT, FLANGE', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(895, '958020601400', 'BOLT,FLG.', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'boltflg', NULL, NULL, 0, 0, 0, '', 0),
(896, '958220801600', 'BOLT,FLG.', 9, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'boltflg', NULL, NULL, 0, 0, 0, '', 0),
(897, '950230803000', 'BOLT,FLG. (SMALL HEAD)', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'boltflg-small-head', NULL, NULL, 0, 0, 0, '', 0),
(898, '970170501200', 'BOLT,HEX.', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bolthex', NULL, NULL, 0, 0, 0, '', 0),
(899, '2TBH350E0000', 'BRACKET', 9, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bracket', NULL, NULL, 0, 0, 0, '', 0),
(900, 'BC8F11590000', 'BRACKET', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bracket', NULL, NULL, 0, 0, 0, '', 0),
(901, 'BC8F334G1000', 'BRACKET 1', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bracket-1', NULL, NULL, 0, 0, 0, '', 0),
(902, 'BC8F74520000', 'BRACKET, FOOTREST1', 9, '174.0000', '210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bracket-footrest1', NULL, NULL, 0, 0, 0, '', 0),
(903, 'BC8F74620000', 'BRACKET, FOOTREST2', 9, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bracket-footrest2', NULL, NULL, 0, 0, 0, '', 0),
(904, 'BC8WF53A0000', 'BRAKE SHOE KIT', 9, '589.0000', '710.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'brake-shoe-kit', NULL, NULL, 0, 0, 0, '', 0),
(905, '93306301Y600', 'BRG.', 9, '497.0000', '600.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'brg', NULL, NULL, 0, 0, 0, '', 0),
(906, '2TBH33110000', 'BULB, FLASHER (12V-10W)', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bulb-flasher-12v-10w', NULL, NULL, 0, 0, 0, '', 0),
(907, '2TBH43142000', 'BULB, HEADLIGHT (12V-35/35W)', 9, '705.0000', '850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bulb-headlight-12v-3535w', NULL, NULL, 0, 0, 0, '', 0),
(908, 'BC8F51360000', 'BUSH', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'bush', NULL, NULL, 0, 0, 0, '', 0),
(909, '2TBF63350000', 'CABLE, CLUTCH', 9, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cable-clutch', NULL, NULL, 0, 0, 0, '', 0),
(910, '2TBF63111100', 'CABLE, THROTTLE 1', 9, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cable-throttle-1', NULL, NULL, 0, 0, 0, '', 0),
(911, 'BC8F53510000', 'CAMSHAFT', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'camshaft', NULL, NULL, 0, 0, 0, '', 0),
(912, '2TBE44120000', 'CAP, CLEANER CASE1', 9, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cap-cleaner-case1', NULL, NULL, 0, 0, 0, '', 0),
(913, '2TBE43010000', 'CARB. ASSY.,1', 9, '9603.0000', '10500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'carb-assy1', NULL, NULL, 0, 0, 0, '', 0),
(914, '2TBH358L0000', 'CASE UNIT', 9, '232.0000', '280.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'case-unit', NULL, NULL, 0, 0, 0, '', 0),
(915, '2TBE44110000', 'CASE, AIR CLEANER1', 9, '390.0000', '470.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'case-air-cleaner1', NULL, NULL, 0, 0, 0, '', 0),
(916, '2TBH353H0000', 'CASE, UNDER', 9, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'case-under', NULL, NULL, 0, 0, 0, '', 0),
(917, '2TBH353G0000', 'CASE, UPPER', 9, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'case-upper', NULL, NULL, 0, 0, 0, '', 0),
(918, '934101983100', 'CIRCLIP', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'circlip', NULL, NULL, 0, 0, 0, '', 0),
(919, '904671182700', 'CLIP', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'clip', NULL, NULL, 0, 0, 0, '', 0),
(920, 'BC8F53660000', 'CLUTCH, HUB', 9, '373.0000', '450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'clutch-hub', NULL, NULL, 0, 0, 0, '', 0),
(921, '1KLF51490100', 'CLUTCH, METER', 9, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'clutch-meter', NULL, NULL, 0, 0, 0, '', 0),
(922, '903870683700', 'COLLAR', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(923, '903871002000', 'COLLAR', 9, '415.0000', '500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(924, '90387121L900', 'COLLAR', 9, '439.0000', '530.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(925, '903871284000', 'COLLAR', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(926, '903871583200', 'COLLAR', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(927, '903871583300', 'COLLAR', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar', NULL, NULL, 0, 0, 0, '', 0),
(928, 'B83H41450000', 'COLLAR, BODY FITTING', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'collar-body-fitting', NULL, NULL, 0, 0, 0, '', 0),
(929, 'B83H43120000', 'CORD ASSY', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cord-assy', NULL, NULL, 0, 0, 0, '', 0),
(930, '2TBF34160000', 'COVER, BALL RACE 2', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-ball-race-2', NULL, NULL, 0, 0, 0, '', 0),
(931, '2TBE54181000', 'COVER, CHAIN CASE', 9, '356.0000', '430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-chain-case', NULL, NULL, 0, 0, 0, '', 0),
(932, 'BC8F74130000', 'COVER, FOOTREST', 9, '116.0000', '140.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-footrest', NULL, NULL, 0, 0, 0, '', 0),
(933, '2TBF63720000', 'COVER, HANDLE LEVER 1', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-handle-lever-1', NULL, NULL, 0, 0, 0, '', 0),
(934, 'BC8F51180000', 'COVER, HUB DUST', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-hub-dust', NULL, NULL, 0, 0, 0, '', 0),
(935, '2TBE56180000', 'COVER, KICK LEVER', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-kick-lever', NULL, NULL, 0, 0, 0, '', 0),
(936, '2TBE81130000', 'COVER, SHIFT PEDAL', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-shift-pedal', NULL, NULL, 0, 0, 0, '', 0),
(937, 'BC8XF17100P0', 'COVER, SIDE 1 A', 9, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-side-1-a', NULL, NULL, 0, 0, 0, '', 0),
(938, 'BC8XF17110P0', 'COVER, SIDE 1 B', 9, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-side-1-b', NULL, NULL, 0, 0, 0, '', 0),
(939, 'BC8XF17200P0', 'COVER, SIDE 2 A', 9, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-side-2-a', NULL, NULL, 0, 0, 0, '', 0),
(940, 'BC8XF17210P0', 'COVER, SIDE 2 B', 9, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-side-2-b', NULL, NULL, 0, 0, 0, '', 0),
(941, 'BC8F471K00P0', 'COVER, TAIL 1 A', 9, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-tail-1-a', NULL, NULL, 0, 0, 0, '', 0),
(942, 'BC8F471K00P1', 'COVER, TAIL 1 B', 9, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-tail-1-b', NULL, NULL, 0, 0, 0, '', 0),
(943, 'BC8F472K00P0', 'COVER, TAIL 2 A', 9, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-tail-2-a', NULL, NULL, 0, 0, 0, '', 0),
(944, 'BC8F472K00P1', 'COVER, TAIL 2 B', 9, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'cover-tail-2-b', NULL, NULL, 0, 0, 0, '', 0),
(945, '2TBF34350035', 'CROWN, HANDLE', 9, '1055.0000', '1210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'crown-handle', NULL, NULL, 0, 0, 0, '', 0),
(946, 'B83H41630000', 'DAMPER', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(947, 'B83H41640000', 'DAMPER', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(948, 'BC8F53640000', 'DAMPER', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper', NULL, NULL, 0, 0, 0, '', 0),
(949, '2TBF317J0000', 'DAMPER 1', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-1', NULL, NULL, 0, 0, 0, '', 0),
(950, '2TBF17780000', 'DAMPER, LOCATING 1', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-locating-1', NULL, NULL, 0, 0, 0, '', 0),
(951, '2TBF41810000', 'DAMPER, LOCATING 1', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-locating-1', NULL, NULL, 0, 0, 0, '', 0),
(952, 'BC8F41830000', 'DAMPER, LOCATING 3', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-locating-3', NULL, NULL, 0, 0, 0, '', 0),
(953, 'BC8F41831000', 'DAMPER, LOCATING 3', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-locating-3', NULL, NULL, 0, 0, 0, '', 0),
(954, '905200382900', 'DAMPER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-plate', NULL, NULL, 0, 0, 0, '', 0),
(955, '905200582700', 'DAMPER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-plate', NULL, NULL, 0, 0, 0, '', 0),
(956, '905201002600', 'DAMPER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'damper-plate', NULL, NULL, 0, 0, 0, '', 0),
(957, 'BC8F47300000', 'DOUBLE SEAT ASSY', 9, '1613.0000', '1850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'double-seat-assy', NULL, NULL, 0, 0, 0, '', 0),
(958, '2TBE44370000', 'DUCT', 9, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'duct', NULL, NULL, 0, 0, 0, '', 0),
(959, '2TBE44500000', 'ELEMENT ASSY, AIRCLEANER', 9, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'element-assy-aircleaner', NULL, NULL, 0, 0, 0, '', 0),
(960, '2TBE54350000', 'EMBLEM', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'emblem', NULL, NULL, 0, 0, 0, '', 0),
(961, '2TBF151120P0', 'FENDER, FRONT', 9, '1003.0000', '1150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'fender-front', NULL, NULL, 0, 0, 0, '', 0),
(962, '2TBF151120P3', 'FENDER, FRONT', 9, '1003.0000', '1150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'fender-front', NULL, NULL, 0, 0, 0, '', 0),
(963, 'BC8F16110000', 'FENDER, REAR', 9, '415.0000', '500.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'fender-rear', NULL, NULL, 0, 0, 0, '', 0),
(964, 'BC8F16210000', 'FLAP', 9, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'flap', NULL, NULL, 0, 0, 0, '', 0),
(965, '2TBH33500000', 'FLASHER RELAY ASSY', 9, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'flasher-relay-assy', NULL, NULL, 0, 0, 0, '', 0),
(966, '2TBF74110000', 'FOOTREST 1', 9, '746.0000', '900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'footrest-1', NULL, NULL, 0, 0, 0, '', 0),
(967, 'BC8F11100000', 'FRAME COMP.', 9, '10288.0000', '11250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'frame-comp', NULL, NULL, 0, 0, 0, '', 0),
(968, '2TBH33100000', 'FRONT FLASHER LIGHT ASSY 1', 9, '199.0000', '240.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'front-flasher-light-assy-1', NULL, NULL, 0, 0, 0, '', 0),
(969, '2TBH33200000', 'FRONT FLASHER LIGHT ASSY 2', 9, '199.0000', '240.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'front-flasher-light-assy-2', NULL, NULL, 0, 0, 0, '', 0),
(970, 'BC8F31020000', 'FRONT FORK ASSY (L.H)', 9, '4272.0000', '4900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'front-fork-assy-lh', NULL, NULL, 0, 0, 0, '', 0),
(971, 'BC8F31030000', 'FRONT FORK ASSY (R.H)', 9, '4272.0000', '4900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'front-fork-assy-rh', NULL, NULL, 0, 0, 0, '', 0),
(972, '5VLH39802200', 'FRONT STOP SWITCHASSY', 9, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'front-stop-switchassy', NULL, NULL, 0, 0, 0, '', 0),
(973, '1BKF45000100', 'FUEL COCK ASSY 1', 9, '889.0000', '1020.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'fuel-cock-assy-1', NULL, NULL, 0, 0, 0, '', 0),
(974, 'BC8XCF4100P0', 'FUEL TANK COMP.', 9, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(975, 'BC8XCF4100P1', 'FUEL TANK COMP.', 9, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(976, '2D0F45120000', 'GASKET', 9, '50.0000', '60.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'gasket', NULL, NULL, 0, 0, 0, '', 0),
(977, '1BKH57530100', 'GASKET, SENDER UNIT', 9, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'gasket-sender-unit', NULL, NULL, 0, 0, 0, '', 0),
(978, 'BC8H47120000', 'GASKET, TAIL LIGHT', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'gasket-tail-light', NULL, NULL, 0, 0, 0, '', 0),
(979, '248251350000', 'GEAR, DRIVE', 9, '738.0000', '890.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'gear-drive', NULL, NULL, 0, 0, 0, '', 0),
(980, '214251380000', 'GEAR, METER', 9, '746.0000', '900.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'gear-meter', NULL, NULL, 0, 0, 0, '', 0),
(981, '1KLF51350000', 'GEAR,DRIVE', 9, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'geardrive', NULL, NULL, 0, 0, 0, '', 0),
(982, '1KLF51380000', 'GEAR,METER', 9, '638.0000', '770.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'gearmeter', NULL, NULL, 0, 0, 0, '', 0),
(983, 'BC8F173E0000', 'GRAPHIC 1', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-1', NULL, NULL, 0, 0, 0, '', 0),
(984, 'BC8F173E1000', 'GRAPHIC 1', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-1', NULL, NULL, 0, 0, 0, '', 0),
(985, 'BC8F173F0000', 'GRAPHIC 2', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-2', NULL, NULL, 0, 0, 0, '', 0),
(986, 'BC8F173F1000', 'GRAPHIC 2', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-2', NULL, NULL, 0, 0, 0, '', 0),
(987, 'BC8F42440000', 'GRAPHIC, FUEL TANK1', 9, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(988, 'BC8F42441000', 'GRAPHIC, FUEL TANK1', 9, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(989, 'BC8F42450000', 'GRAPHIC, FUEL TANK2', 9, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(990, 'BC8F42451000', 'GRAPHIC, FUEL TANK2', 9, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(991, '2TBF62410000', 'GRIP', 9, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'grip', NULL, NULL, 0, 0, 0, '', 0),
(992, '2TBF62400000', 'GRIP ASSY', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'grip-assy', NULL, NULL, 0, 0, 0, '', 0),
(993, '904801481200', 'GROMMET', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'grommet', NULL, NULL, 0, 0, 0, '', 0),
(994, '2TBF33890000', 'GUIDE, CABLE', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'guide-cable', NULL, NULL, 0, 0, 0, '', 0),
(995, 'BC8F47730000', 'HANDLE, SEAT', 9, '622.0000', '750.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'handle-seat', NULL, NULL, 0, 0, 0, '', 0),
(996, 'BC8F61110000', 'HANDLEBAR', 9, '481.0000', '580.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'handlebar', NULL, NULL, 0, 0, 0, '', 0),
(997, '2TBF15180000', 'HOLDER, CABLE', 9, '66.0000', '80.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'holder-cable', NULL, NULL, 0, 0, 0, '', 0),
(998, 'BC8H29110000', 'HOLDER, LEVER 1', 9, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'holder-lever-1', NULL, NULL, 0, 0, 0, '', 0),
(999, 'BC8H29210000', 'HOLDER, LEVER 2', 9, '141.0000', '170.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'holder-lever-2', NULL, NULL, 0, 0, 0, '', 0),
(1000, '2TBH33710000', 'HORN', 9, '290.0000', '350.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'horn', NULL, NULL, 0, 0, 0, '', 0),
(1001, '5VLE49870000', 'HOSE', 9, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'hose', NULL, NULL, 0, 0, 0, '', 0),
(1002, '2XPE48810000', 'HOSE, BEND 1', 9, '915.0000', '1050.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'hose-bend-1', NULL, NULL, 0, 0, 0, '', 0),
(1003, 'BC8E48820000', 'HOSE, BEND 2', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'hose-bend-2', NULL, NULL, 0, 0, 0, '', 0),
(1004, 'BC8F51110000', 'HUB, FRONT', 9, '1107.0000', '1270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'hub-front', NULL, NULL, 0, 0, 0, '', 0),
(1005, 'BC8F53110000', 'HUB, REAR', 9, '1107.0000', '1270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'hub-rear', NULL, NULL, 0, 0, 0, '', 0),
(1006, '2TBE43435K00', 'JET, MAIN', 9, '249.0000', '300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'jet-main', NULL, NULL, 0, 0, 0, '', 0),
(1007, '2TBE43426800', 'JET, PILOT (#136)', 9, '340.0000', '410.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'jet-pilot-136', NULL, NULL, 0, 0, 0, '', 0),
(1008, '2TBE44530000', 'JOINT, AIR CLEANER1', 9, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'joint-air-cleaner1', NULL, NULL, 0, 0, 0, '', 0),
(1009, 'BC8H21090000', 'JOINT, HOSE', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'joint-hose', NULL, NULL, 0, 0, 0, '', 0),
(1010, 'BC8E56200000', 'KICK CRANK ASSY', 9, '456.0000', '550.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'kick-crank-assy', NULL, NULL, 0, 0, 0, '', 0);
INSERT INTO `sma_products` (`id`, `code`, `name`, `unit`, `cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `cf1`, `cf2`, `cf3`, `cf4`, `cf5`, `cf6`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `barcode_symbology`, `file`, `product_details`, `tax_method`, `type`, `supplier1`, `supplier1price`, `supplier2`, `supplier2price`, `supplier3`, `supplier3price`, `supplier4`, `supplier4price`, `supplier5`, `supplier5price`, `promotion`, `promo_price`, `start_date`, `end_date`, `supplier1_part_no`, `supplier2_part_no`, `supplier3_part_no`, `supplier4_part_no`, `supplier5_part_no`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`, `hsn_code`, `views`, `hide`, `second_name`, `hide_pos`) VALUES
(1011, '2TBH357H0000', 'KNOB', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'knob', NULL, NULL, 0, 0, 0, '', 0),
(1012, 'BC8H47210000', 'LENS, TAIL LIGHT', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'lens-tail-light', NULL, NULL, 0, 0, 0, '', 0),
(1013, 'BC8H39120000', 'LEVER 1', 9, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'lever-1', NULL, NULL, 0, 0, 0, '', 0),
(1014, 'BC8H39220000', 'LEVER 2', 9, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'lever-2', NULL, NULL, 0, 0, 0, '', 0),
(1015, 'BC8F53550000', 'LEVER, CAMSHAFT', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'lever-camshaft', NULL, NULL, 0, 0, 0, '', 0),
(1016, 'BC8F51550000', 'LEVER, CAMSHAFT 1', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'lever-camshaft-1', NULL, NULL, 0, 0, 0, '', 0),
(1017, 'BC8H35000000', 'METER ASSY', 9, '3095.0000', '3550.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'meter-assy', NULL, NULL, 0, 0, 0, '', 0),
(1018, 'BC8E47030100', 'MUFFLER COMP. 1', 9, '8094.0000', '8850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'muffler-comp-1', NULL, NULL, 0, 0, 0, '', 0),
(1019, '2TBW149J0000', 'NEEDLE SET', 9, '2005.0000', '2300.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'needle-set', NULL, NULL, 0, 0, 0, '', 0),
(1020, '2TBE43411000', 'NOZZLE, MAIN', 9, '580.0000', '700.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'nozzle-main', NULL, NULL, 0, 0, 0, '', 0),
(1021, '5RSE43410000', 'NOZZLE, MAIN', 9, '589.0000', '710.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'nozzle-main', NULL, NULL, 0, 0, 0, '', 0),
(1022, '901762280200', 'NUT, CROWN', 9, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'nut-crown', NULL, NULL, 0, 0, 0, '', 0),
(1023, '901851080700', 'NUT, SELF-LOCKING', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'nut-self-locking', NULL, NULL, 0, 0, 0, '', 0),
(1024, '957041050000', 'NUT,FLG.', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'nutflg', NULL, NULL, 0, 0, 0, '', 0),
(1025, '931021800800', 'OIL SEAL', 9, '216.0000', '260.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(1026, '931040700300', 'OIL SEAL', 9, '91.0000', '110.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'oil-seal', NULL, NULL, 0, 0, 0, '', 0),
(1027, '932101457900', 'O-RING', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(1028, '932101481800', 'O-RING', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'o-ring', NULL, NULL, 0, 0, 0, '', 0),
(1029, 'BC8F31060000', 'OUTER TUBE COMP.', 9, '1046.0000', '1200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'outer-tube-comp', NULL, NULL, 0, 0, 0, '', 0),
(1030, 'BC8F31070000', 'OUTER TUBE COMP.', 9, '1046.0000', '1200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'outer-tube-comp', NULL, NULL, 0, 0, 0, '', 0),
(1031, '2TBF72110000', 'PEDAL, BRAKE', 9, '356.0000', '430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'pedal-brake', NULL, NULL, 0, 0, 0, '', 0),
(1032, '2TBE41970000', 'PIPE', 9, '298.0000', '360.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'pipe', NULL, NULL, 0, 0, 0, '', 0),
(1033, 'BC8E48720000', 'PIPE 2', 9, '959.0000', '1100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'pipe-2', NULL, NULL, 0, 0, 0, '', 0),
(1034, '2TBE443E0000', 'PIPE, DRAIN', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'pipe-drain', NULL, NULL, 0, 0, 0, '', 0),
(1035, 'BC8F43110000', 'PIPE, FUEL 1', 9, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'pipe-fuel-1', NULL, NULL, 0, 0, 0, '', 0),
(1036, 'BC8F51210035', 'PLATE, BRAKE SHOE', 9, '889.0000', '1020.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'plate-brake-shoe', NULL, NULL, 0, 0, 0, '', 0),
(1037, 'BC8F53210035', 'PLATE, BRAKE SHOE', 9, '889.0000', '1020.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'plate-brake-shoe', NULL, NULL, 0, 0, 0, '', 0),
(1038, 'BC8F533A0000', 'PLATE, INDICATOR', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'plate-indicator', NULL, NULL, 0, 0, 0, '', 0),
(1039, '2TBE47280000', 'PROTECTOR, MUFFLER2', 9, '398.0000', '480.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'protector-muffler2', NULL, NULL, 0, 0, 0, '', 0),
(1040, '2TBE47580000', 'PROTECTOR, SILENCER', 9, '547.0000', '660.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'protector-silencer', NULL, NULL, 0, 0, 0, '', 0),
(1041, 'BC8H33300000', 'REAR FLASHER LIGHTASSY 1', 9, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rear-flasher-lightassy-1', NULL, NULL, 0, 0, 0, '', 0),
(1042, 'BC8H33400000', 'REAR FLASHER LIGHTASSY 2', 9, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rear-flasher-lightassy-2', NULL, NULL, 0, 0, 0, '', 0),
(1043, 'BC8H51300000', 'REAR REFLECTOR ASSY', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rear-reflector-assy', NULL, NULL, 0, 0, 0, '', 0),
(1044, '944161881300', 'RIM', 9, '1186.0000', '1360.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rim', NULL, NULL, 0, 0, 0, '', 0),
(1045, '944181880800', 'RIM', 9, '1273.0000', '1460.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rim', NULL, NULL, 0, 0, 0, '', 0),
(1046, 'B83H43150000', 'RIM, HEADLIGHT', 9, '216.0000', '260.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rim-headlight', NULL, NULL, 0, 0, 0, '', 0),
(1047, 'BC8F51370000', 'RING, STOP', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'ring-stop', NULL, NULL, 0, 0, 0, '', 0),
(1048, '2TBF72310000', 'ROD, BRAKE', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'rod-brake', NULL, NULL, 0, 0, 0, '', 0),
(1049, 'B83H45260000', 'SCREW', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'screw', NULL, NULL, 0, 0, 0, '', 0),
(1050, '989020602000', 'SCREW, BIND', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'screw-bind', NULL, NULL, 0, 0, 0, '', 0),
(1051, '989800602000', 'SCREW, BIND', 9, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'screw-bind', NULL, NULL, 0, 0, 0, '', 0),
(1052, 'BC8H47240000', 'SCREW, LENS FITTING', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'screw-lens-fitting', NULL, NULL, 0, 0, 0, '', 0),
(1053, '2TBE44520000', 'SEAL', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'seal', NULL, NULL, 0, 0, 0, '', 0),
(1054, '2TBE44620000', 'SEAL', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'seal', NULL, NULL, 0, 0, 0, '', 0),
(1055, '2D0F45340000', 'SEAL, COCK', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'seal-cock', NULL, NULL, 0, 0, 0, '', 0),
(1056, 'BC8F21510000', 'SEAL, GUARD', 9, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'seal-guard', NULL, NULL, 0, 0, 0, '', 0),
(1057, '931021880600', 'SEAL,OIL', 9, '99.0000', '120.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'sealoil', NULL, NULL, 0, 0, 0, '', 0),
(1058, '931040781100', 'SEAL,OIL', 9, '75.0000', '90.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'sealoil', NULL, NULL, 0, 0, 0, '', 0),
(1059, '1BKH57520100', 'SENDER UNIT ASSY,FUEL METER', 9, '705.0000', '850.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'sender-unit-assyfuel-meter', NULL, NULL, 0, 0, 0, '', 0),
(1060, '2TBF71120000', 'SHAFT, MAIN STAND', 9, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'shaft-main-stand', NULL, NULL, 0, 0, 0, '', 0),
(1061, '2TBE81100000', 'SHIFT PEDAL ASSY', 9, '224.0000', '270.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'shift-pedal-assy', NULL, NULL, 0, 0, 0, '', 0),
(1062, '5VLE81012000', 'SHIFT SHAFT ASSY', 9, '1055.0000', '1210.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'shift-shaft-assy', NULL, NULL, 0, 0, 0, '', 0),
(1063, '905601282800', 'SPACER', 9, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'spacer', NULL, NULL, 0, 0, 0, '', 0),
(1064, 'BC8H35500000', 'SPEEDOMETER CABLEASSY', 9, '191.0000', '230.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'speedometer-cableassy', NULL, NULL, 0, 0, 0, '', 0),
(1065, 'BC8WF51H0000', 'SPOKE SET (36 pcs)', 9, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'spoke-set-36-pcs', NULL, NULL, 0, 0, 0, '', 0),
(1066, 'B83H43630000', 'SPRG. LAMP SET', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'sprg-lamp-set', NULL, NULL, 0, 0, 0, '', 0),
(1067, '2TBE42860000', 'SPRING', 9, '33.0000', '40.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'spring', NULL, NULL, 0, 0, 0, '', 0),
(1068, '905010880300', 'SPRING, COMPRESSION', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'spring-compression', NULL, NULL, 0, 0, 0, '', 0),
(1069, '905084081200', 'SPRING, TORSION', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'spring-torsion', NULL, NULL, 0, 0, 0, '', 0),
(1070, 'BC8F54450000', 'SPROCKET, DRIVEN', 9, '812.0000', '980.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'sprocket-driven', NULL, NULL, 0, 0, 0, '', 0),
(1071, '2MPE55800000', 'STARTER CLUTCH OUTER ASSY', 9, '1918.0000', '2200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'starter-clutch-outer-assy', NULL, NULL, 0, 0, 0, '', 0),
(1072, '2TBF13160000', 'STAY, ENGINE 2', 9, '356.0000', '430.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'stay-engine-2', NULL, NULL, 0, 0, 0, '', 0),
(1073, '2TBF15130000', 'STAY, FENDER 1', 9, '166.0000', '200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'stay-fender-1', NULL, NULL, 0, 0, 0, '', 0),
(1074, 'BC8F31740000', 'STAY, HEADLIGHT', 9, '456.0000', '550.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'stay-headlight', NULL, NULL, 0, 0, 0, '', 0),
(1075, 'BC8H47100000', 'TAILLIGHT UNIT ASSY', 9, '539.0000', '650.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'taillight-unit-assy', NULL, NULL, 0, 0, 0, '', 0),
(1076, 'BC8F54120000', 'WASHER', 9, '108.0000', '130.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer', NULL, NULL, 0, 0, 0, '', 0),
(1077, '902010685000', 'WASHER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(1078, '902010783600', 'WASHER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(1079, '902010883000', 'WASHER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(1080, '902012084400', 'WASHER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(1081, '902012280400', 'WASHER, PLATE', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-plate', NULL, NULL, 0, 0, 0, '', 0),
(1082, '2TBF34180000', 'WASHER, SPECIAL', 9, '25.0000', '30.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-special', NULL, NULL, 0, 0, 0, '', 0),
(1083, '4FPF41860100', 'WASHER, SPECIAL', 9, '83.0000', '100.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'washer-special', NULL, NULL, 0, 0, 0, '', 0),
(1084, 'BC8H25900000', 'WIRE HARNESS ASSY', 9, '1264.0000', '1450.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'wire-harness-assy', NULL, NULL, 0, 0, 0, '', 0),
(1085, 'BC8F63410000', 'WIRE, BRAKE 1', 9, '207.0000', '250.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 9, 9, NULL, 'wire-brake-1', NULL, NULL, 0, 0, 0, '', 0),
(1086, '901050686900', 'BOLT, FLANGE', 10, '58.0000', '70.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 10, 10, NULL, 'bolt-flange', NULL, NULL, 0, 0, 0, '', 0),
(1087, '2TBE74020000', 'DRIVE AXLE ASSY', 10, '1003.0000', '1150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 10, 10, NULL, 'drive-axle-assy', NULL, NULL, 0, 0, 0, '', 0),
(1088, 'BC8XF17120P1', 'COVER, SIDE 1', 11, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-side-1', NULL, NULL, 0, 0, 0, '', 0),
(1089, 'BC8XF17130P0', 'COVER, SIDE 1', 11, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-side-1', NULL, NULL, 0, 0, 0, '', 0),
(1090, 'BC8XF17140P0', 'COVER, SIDE 1', 11, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-side-1', NULL, NULL, 0, 0, 0, '', 0),
(1091, 'BC8XF17220P1', 'COVER, SIDE 2', 11, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-side-2', NULL, NULL, 0, 0, 0, '', 0),
(1092, 'BC8XF17230P0', 'COVER, SIDE 2', 11, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-side-2', NULL, NULL, 0, 0, 0, '', 0),
(1093, 'BC8XF17240P0', 'COVER, SIDE 2', 11, '663.0000', '800.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-side-2', NULL, NULL, 0, 0, 0, '', 0),
(1094, 'BC8F471K00P2', 'COVER, TAIL 1', 11, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-tail-1', NULL, NULL, 0, 0, 0, '', 0),
(1095, 'BC8F472K00P2', 'COVER, TAIL 2', 11, '796.0000', '960.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'cover-tail-2', NULL, NULL, 0, 0, 0, '', 0),
(1096, '2TBF151120P1', 'FENDER, FRONT', 11, '1003.0000', '1150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'fender-front', NULL, NULL, 0, 0, 0, '', 0),
(1097, 'BC8XCF4110P0', 'FUEL TANK COMP.', 11, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(1098, 'BC8XCF4110P1', 'FUEL TANK COMP.', 11, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(1099, 'BC8XCF4110P2', 'FUEL TANK COMP.', 11, '8414.0000', '9200.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'fuel-tank-comp', NULL, NULL, 0, 0, 0, '', 0),
(1100, 'BC8H47040000', 'GASKET, TAILLIGHT', 11, '41.0000', '50.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'gasket-taillight', NULL, NULL, 0, 0, 0, '', 0),
(1101, 'BC8F173E2000', 'GRAPHIC 1', 11, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-1', NULL, NULL, 0, 0, 0, '', 0),
(1102, 'BC8F173E3000', 'GRAPHIC 1', 11, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-1', NULL, NULL, 0, 0, 0, '', 0),
(1103, 'BC8F173E4000', 'GRAPHIC 1', 11, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-1', NULL, NULL, 0, 0, 0, '', 0),
(1104, 'BC8F173F2000', 'GRAPHIC 2', 11, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-2', NULL, NULL, 0, 0, 0, '', 0),
(1105, 'BC8F173F3000', 'GRAPHIC 2', 11, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-2', NULL, NULL, 0, 0, 0, '', 0),
(1106, 'BC8F173F4000', 'GRAPHIC 2', 11, '124.0000', '150.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-2', NULL, NULL, 0, 0, 0, '', 0),
(1107, 'BC8F42442000', 'GRAPHIC, FUEL TANK1', 11, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(1108, 'BC8F42443000', 'GRAPHIC, FUEL TANK1', 11, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(1109, 'BC8F42444000', 'GRAPHIC, FUEL TANK1', 11, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-fuel-tank1', NULL, NULL, 0, 0, 0, '', 0),
(1110, 'BC8F42452000', 'GRAPHIC, FUEL TANK2', 11, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(1111, 'BC8F42453000', 'GRAPHIC, FUEL TANK2', 11, '274.0000', '330.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0),
(1112, 'BC8F42454000', 'GRAPHIC, FUEL TANK2', 11, '265.0000', '320.0000', '0.0000', '', 1, NULL, '', '', '', '', '', '', '0.0000', 5, 1, NULL, NULL, '', NULL, NULL, 0, 'standard', NULL, '0.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, 11, 11, NULL, 'graphic-fuel-tank2', NULL, NULL, 0, 0, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_photos`
--

CREATE TABLE `sma_product_photos` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_prices`
--

CREATE TABLE `sma_product_prices` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_group_id` int(11) NOT NULL,
  `price` decimal(25,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_product_variants`
--

CREATE TABLE `sma_product_variants` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `cost` decimal(25,4) DEFAULT NULL,
  `price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_promos`
--

CREATE TABLE `sma_promos` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `product2buy` int(11) NOT NULL,
  `product2get` int(11) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchases`
--

CREATE TABLE `sma_purchases` (
  `id` int(11) NOT NULL,
  `reference_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `supplier_id` int(11) NOT NULL,
  `supplier` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `product_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_discount` decimal(25,4) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT NULL,
  `product_tax` decimal(25,4) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `paid` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `status` varchar(55) DEFAULT '',
  `payment_status` varchar(20) DEFAULT 'pending',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `return_purchase_ref` varchar(55) DEFAULT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `return_purchase_total` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_purchase_items`
--

CREATE TABLE `sma_purchase_items` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(20) DEFAULT NULL,
  `discount` varchar(20) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `quantity_balance` decimal(15,4) DEFAULT '0.0000',
  `date` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `quantity_received` decimal(15,4) DEFAULT NULL,
  `supplier_part_no` varchar(50) DEFAULT NULL,
  `purchase_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quotes`
--

CREATE TABLE `sma_quotes` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `internal_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(55) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `supplier` varchar(55) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_quote_items`
--

CREATE TABLE `sma_quote_items` (
  `id` int(11) NOT NULL,
  `quote_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_returns`
--

CREATE TABLE `sma_returns` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `paid` decimal(25,4) DEFAULT '0.0000',
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `shipping` decimal(25,4) DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_return_items`
--

CREATE TABLE `sma_return_items` (
  `id` int(11) NOT NULL,
  `return_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sales`
--

CREATE TABLE `sma_sales` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) NOT NULL,
  `biller_id` int(11) NOT NULL,
  `biller` varchar(55) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `staff_note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `product_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount_id` varchar(20) DEFAULT NULL,
  `total_discount` decimal(25,4) DEFAULT '0.0000',
  `order_discount` decimal(25,4) DEFAULT '0.0000',
  `product_tax` decimal(25,4) DEFAULT '0.0000',
  `order_tax_id` int(11) DEFAULT NULL,
  `order_tax` decimal(25,4) DEFAULT '0.0000',
  `total_tax` decimal(25,4) DEFAULT '0.0000',
  `shipping` decimal(25,4) DEFAULT '0.0000',
  `grand_total` decimal(25,4) NOT NULL,
  `sale_status` varchar(20) DEFAULT NULL,
  `payment_status` varchar(20) DEFAULT NULL,
  `payment_term` tinyint(4) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total_items` smallint(6) DEFAULT NULL,
  `pos` tinyint(1) NOT NULL DEFAULT '0',
  `paid` decimal(25,4) DEFAULT '0.0000',
  `return_id` int(11) DEFAULT NULL,
  `surcharge` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `return_sale_ref` varchar(55) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `return_sale_total` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `rounding` decimal(10,4) DEFAULT NULL,
  `suspend_note` varchar(255) DEFAULT NULL,
  `api` tinyint(1) DEFAULT '0',
  `shop` tinyint(1) DEFAULT '0',
  `address_id` int(11) DEFAULT NULL,
  `reserve_id` int(11) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `manual_payment` varchar(55) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL,
  `payment_method` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sale_items`
--

CREATE TABLE `sma_sale_items` (
  `id` int(11) NOT NULL,
  `sale_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `sale_item_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_sessions`
--

CREATE TABLE `sma_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_sessions`
--

INSERT INTO `sma_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('0futa4h4771hm5eoe20p437n4r5v2emj', '::1', 1568821282, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832313238323b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('3pj65oauvr1esd8ie8kvmbhlc34f2rhr', '::1', 1568824496, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832343439353b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6c6173745f61637469766974797c693a313536383832343439363b6572726f727c733a38323a225265676973746572206973206e6f74206f70656e2c20506c6561736520656e74657220746865206361736820696e2068616e6420616d6f756e7420616e6420636c69636b206f70656e207265676973746572223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('4cfsqtn38gnhr4hlh7nvkam19hpj0sip', '::1', 1568819809, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383831393830393b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('64b7011ld58811rpdns5fbuigkhjojc8', '::1', 1568819495, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383831393439353b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b6572726f727c733a37303a22506c6561736520636865636b2063617465676f727920636f64652028292e2043617465676f727920436f646520646f6573206e6f742065786973742e204c696e65204e6f2031223b5f5f63695f766172737c613a313a7b733a353a226572726f72223b733a333a226f6c64223b7d),
('b92ldsd5defbfn6m791rbbd8o87ssdn4', '::1', 1568820166, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832303131313b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('enb7khcebi0oe1qiea9tnkctjopqnjc8', '::1', 1568824070, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832343037303b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('f826uvkja56vls8gdrdrstnlmdpedup2', '::1', 1568824495, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832343439353b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('fao2jcrlrrrt6e0ffhj9cvopg8sl7ip4', '::1', 1568820412, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832303431323b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('grhdu0eom27vs7rr87bgh21nt8kncfvj', '::1', 1568819138, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383831393133383b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b),
('tkfeaceh4ii3q25lhar762o3bujiafcj', '::1', 1568820756, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536383832303735363b7265717565737465645f706167657c733a353a2261646d696e223b6964656e746974797c733a353a226f776e6572223b757365726e616d657c733a353a226f776e6572223b656d61696c7c733a31383a226f776e65724074656364696172792e636f6d223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c733a31303a2231353631303330393430223b6c6173745f69707c733a333a223a3a31223b6176617461727c4e3b67656e6465727c733a343a226d616c65223b67726f75705f69647c733a313a2231223b77617265686f7573655f69647c4e3b766965775f72696768747c733a313a2230223b656469745f72696768747c733a313a2230223b616c6c6f775f646973636f756e747c733a313a2230223b62696c6c65725f69647c4e3b636f6d70616e795f69647c4e3b73686f775f636f73747c733a313a2230223b73686f775f70726963657c733a313a2230223b);

-- --------------------------------------------------------

--
-- Table structure for table `sma_settings`
--

CREATE TABLE `sma_settings` (
  `setting_id` int(1) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `logo2` varchar(255) NOT NULL,
  `site_name` varchar(55) NOT NULL,
  `language` varchar(20) NOT NULL,
  `default_warehouse` int(2) NOT NULL,
  `accounting_method` tinyint(4) NOT NULL DEFAULT '0',
  `default_currency` varchar(3) NOT NULL,
  `default_tax_rate` int(2) NOT NULL,
  `rows_per_page` int(2) NOT NULL,
  `version` varchar(10) NOT NULL DEFAULT '1.0',
  `default_tax_rate2` int(11) NOT NULL DEFAULT '0',
  `dateformat` int(11) NOT NULL,
  `sales_prefix` varchar(20) DEFAULT NULL,
  `quote_prefix` varchar(20) DEFAULT NULL,
  `purchase_prefix` varchar(20) DEFAULT NULL,
  `transfer_prefix` varchar(20) DEFAULT NULL,
  `delivery_prefix` varchar(20) DEFAULT NULL,
  `payment_prefix` varchar(20) DEFAULT NULL,
  `return_prefix` varchar(20) DEFAULT NULL,
  `returnp_prefix` varchar(20) DEFAULT NULL,
  `expense_prefix` varchar(20) DEFAULT NULL,
  `item_addition` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL,
  `product_serial` tinyint(4) NOT NULL,
  `default_discount` int(11) NOT NULL,
  `product_discount` tinyint(1) NOT NULL DEFAULT '0',
  `discount_method` tinyint(4) NOT NULL,
  `tax1` tinyint(4) NOT NULL,
  `tax2` tinyint(4) NOT NULL,
  `overselling` tinyint(1) NOT NULL DEFAULT '0',
  `restrict_user` tinyint(4) NOT NULL DEFAULT '0',
  `restrict_calendar` tinyint(4) NOT NULL DEFAULT '0',
  `timezone` varchar(100) DEFAULT NULL,
  `iwidth` int(11) NOT NULL DEFAULT '0',
  `iheight` int(11) NOT NULL,
  `twidth` int(11) NOT NULL,
  `theight` int(11) NOT NULL,
  `watermark` tinyint(1) DEFAULT NULL,
  `reg_ver` tinyint(1) DEFAULT NULL,
  `allow_reg` tinyint(1) DEFAULT NULL,
  `reg_notification` tinyint(1) DEFAULT NULL,
  `auto_reg` tinyint(1) DEFAULT NULL,
  `protocol` varchar(20) NOT NULL DEFAULT 'mail',
  `mailpath` varchar(55) DEFAULT '/usr/sbin/sendmail',
  `smtp_host` varchar(100) DEFAULT NULL,
  `smtp_user` varchar(100) DEFAULT NULL,
  `smtp_pass` varchar(255) DEFAULT NULL,
  `smtp_port` varchar(10) DEFAULT '25',
  `smtp_crypto` varchar(10) DEFAULT NULL,
  `corn` datetime DEFAULT NULL,
  `customer_group` int(11) NOT NULL,
  `default_email` varchar(100) NOT NULL,
  `mmode` tinyint(1) NOT NULL,
  `bc_fix` tinyint(4) NOT NULL DEFAULT '0',
  `auto_detect_barcode` tinyint(1) NOT NULL DEFAULT '0',
  `captcha` tinyint(1) NOT NULL DEFAULT '1',
  `reference_format` tinyint(1) NOT NULL DEFAULT '1',
  `racks` tinyint(1) DEFAULT '0',
  `attributes` tinyint(1) NOT NULL DEFAULT '0',
  `product_expiry` tinyint(1) NOT NULL DEFAULT '0',
  `decimals` tinyint(2) NOT NULL DEFAULT '2',
  `qty_decimals` tinyint(2) NOT NULL DEFAULT '2',
  `decimals_sep` varchar(2) NOT NULL DEFAULT '.',
  `thousands_sep` varchar(2) NOT NULL DEFAULT ',',
  `invoice_view` tinyint(1) DEFAULT '0',
  `default_biller` int(11) DEFAULT NULL,
  `envato_username` varchar(50) DEFAULT NULL,
  `purchase_code` varchar(100) DEFAULT NULL,
  `rtl` tinyint(1) DEFAULT '0',
  `each_spent` decimal(15,4) DEFAULT NULL,
  `ca_point` tinyint(4) DEFAULT NULL,
  `each_sale` decimal(15,4) DEFAULT NULL,
  `sa_point` tinyint(4) DEFAULT NULL,
  `update` tinyint(1) DEFAULT '0',
  `sac` tinyint(1) DEFAULT '0',
  `display_all_products` tinyint(1) DEFAULT '0',
  `display_symbol` tinyint(1) DEFAULT NULL,
  `symbol` varchar(50) DEFAULT NULL,
  `remove_expired` tinyint(1) DEFAULT '0',
  `barcode_separator` varchar(2) NOT NULL DEFAULT '-',
  `set_focus` tinyint(1) NOT NULL DEFAULT '0',
  `price_group` int(11) DEFAULT NULL,
  `barcode_img` tinyint(1) NOT NULL DEFAULT '1',
  `ppayment_prefix` varchar(20) DEFAULT 'POP',
  `disable_editing` smallint(6) DEFAULT '90',
  `qa_prefix` varchar(55) DEFAULT NULL,
  `update_cost` tinyint(1) DEFAULT NULL,
  `apis` tinyint(1) NOT NULL DEFAULT '0',
  `state` varchar(100) DEFAULT NULL,
  `pdf_lib` varchar(20) DEFAULT 'dompdf'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_settings`
--

INSERT INTO `sma_settings` (`setting_id`, `logo`, `logo2`, `site_name`, `language`, `default_warehouse`, `accounting_method`, `default_currency`, `default_tax_rate`, `rows_per_page`, `version`, `default_tax_rate2`, `dateformat`, `sales_prefix`, `quote_prefix`, `purchase_prefix`, `transfer_prefix`, `delivery_prefix`, `payment_prefix`, `return_prefix`, `returnp_prefix`, `expense_prefix`, `item_addition`, `theme`, `product_serial`, `default_discount`, `product_discount`, `discount_method`, `tax1`, `tax2`, `overselling`, `restrict_user`, `restrict_calendar`, `timezone`, `iwidth`, `iheight`, `twidth`, `theight`, `watermark`, `reg_ver`, `allow_reg`, `reg_notification`, `auto_reg`, `protocol`, `mailpath`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `smtp_crypto`, `corn`, `customer_group`, `default_email`, `mmode`, `bc_fix`, `auto_detect_barcode`, `captcha`, `reference_format`, `racks`, `attributes`, `product_expiry`, `decimals`, `qty_decimals`, `decimals_sep`, `thousands_sep`, `invoice_view`, `default_biller`, `envato_username`, `purchase_code`, `rtl`, `each_spent`, `ca_point`, `each_sale`, `sa_point`, `update`, `sac`, `display_all_products`, `display_symbol`, `symbol`, `remove_expired`, `barcode_separator`, `set_focus`, `price_group`, `barcode_img`, `ppayment_prefix`, `disable_editing`, `qa_prefix`, `update_cost`, `apis`, `state`, `pdf_lib`) VALUES
(1, 'logo2.png', 'logo3.png', 'Triobyte Inventory', 'english', 1, 0, 'USD', 1, 10, '3.4.18', 1, 5, 'SALE', 'QUOTE', 'PO', 'TR', 'DO', 'IPAY', 'SR', 'PR', '', 0, 'default', 1, 1, 1, 1, 1, 1, 0, 1, 0, 'Asia/Karachi', 800, 800, 150, 150, 0, 0, 0, 0, NULL, 'mail', '/usr/sbin/sendmail', 'pop.gmail.com', 'contact@sma.tecdiary.org', '12345678', '25', NULL, NULL, 1, 'info@triobyte.com', 0, 4, 1, 0, 2, 1, 1, 0, 2, 2, '.', ',', 0, 3, 'iWEBCRUX', '5d6fc57d-99ea-4127-8757-7110e365f846', 0, NULL, NULL, NULL, NULL, 0, 0, 0, 0, 'owner', 0, '-', 0, 1, 1, 'POP', 90, '', 0, 0, 'AN', 'dompdf');

-- --------------------------------------------------------

--
-- Table structure for table `sma_skrill`
--

CREATE TABLE `sma_skrill` (
  `id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL,
  `account_email` varchar(255) NOT NULL DEFAULT 'testaccount2@moneybookers.com',
  `secret_word` varchar(20) NOT NULL DEFAULT 'mbtest',
  `skrill_currency` varchar(3) NOT NULL DEFAULT 'USD',
  `fixed_charges` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_my` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `extra_charges_other` decimal(25,4) NOT NULL DEFAULT '0.0000'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_skrill`
--

INSERT INTO `sma_skrill` (`id`, `active`, `account_email`, `secret_word`, `skrill_currency`, `fixed_charges`, `extra_charges_my`, `extra_charges_other`) VALUES
(1, 1, 'testaccount2@moneybookers.com', 'mbtest', 'USD', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock_counts`
--

CREATE TABLE `sma_stock_counts` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reference_no` varchar(55) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `type` varchar(10) NOT NULL,
  `initial_file` varchar(50) NOT NULL,
  `final_file` varchar(50) DEFAULT NULL,
  `brands` varchar(50) DEFAULT NULL,
  `brand_names` varchar(100) DEFAULT NULL,
  `categories` varchar(50) DEFAULT NULL,
  `category_names` varchar(100) DEFAULT NULL,
  `note` text,
  `products` int(11) DEFAULT NULL,
  `rows` int(11) DEFAULT NULL,
  `differences` int(11) DEFAULT NULL,
  `matches` int(11) DEFAULT NULL,
  `missing` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `finalized` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_stock_count_items`
--

CREATE TABLE `sma_stock_count_items` (
  `id` int(11) NOT NULL,
  `stock_count_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_variant` varchar(55) DEFAULT NULL,
  `product_variant_id` int(11) DEFAULT NULL,
  `expected` decimal(15,4) NOT NULL,
  `counted` decimal(15,4) NOT NULL,
  `cost` decimal(25,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_bills`
--

CREATE TABLE `sma_suspended_bills` (
  `id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(11) NOT NULL,
  `customer` varchar(55) DEFAULT NULL,
  `count` int(11) NOT NULL,
  `order_discount_id` varchar(20) DEFAULT NULL,
  `order_tax_id` int(11) DEFAULT NULL,
  `total` decimal(25,4) NOT NULL,
  `biller_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `suspend_note` varchar(255) DEFAULT NULL,
  `shipping` decimal(15,4) DEFAULT '0.0000',
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_suspended_items`
--

CREATE TABLE `sma_suspended_items` (
  `id` int(11) NOT NULL,
  `suspend_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `net_unit_price` decimal(25,4) NOT NULL,
  `unit_price` decimal(25,4) NOT NULL,
  `quantity` decimal(15,4) DEFAULT '0.0000',
  `warehouse_id` int(11) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `discount` varchar(55) DEFAULT NULL,
  `item_discount` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) NOT NULL,
  `serial_no` varchar(255) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_type` varchar(20) DEFAULT NULL,
  `real_unit_price` decimal(25,4) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_tax_rates`
--

CREATE TABLE `sma_tax_rates` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `code` varchar(10) DEFAULT NULL,
  `rate` decimal(12,4) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_tax_rates`
--

INSERT INTO `sma_tax_rates` (`id`, `name`, `code`, `rate`, `type`) VALUES
(1, 'No Tax', 'NT', '0.0000', '2'),
(2, 'VAT @10%', 'VAT10', '10.0000', '1'),
(3, 'GST @6%', 'GST', '6.0000', '1'),
(4, 'VAT @20%', 'VT20', '20.0000', '1'),
(5, '17', '17', '17.0000', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfers`
--

CREATE TABLE `sma_transfers` (
  `id` int(11) NOT NULL,
  `transfer_no` varchar(55) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `from_warehouse_id` int(11) NOT NULL,
  `from_warehouse_code` varchar(55) NOT NULL,
  `from_warehouse_name` varchar(55) NOT NULL,
  `to_warehouse_id` int(11) NOT NULL,
  `to_warehouse_code` varchar(55) NOT NULL,
  `to_warehouse_name` varchar(55) NOT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `total` decimal(25,4) DEFAULT NULL,
  `total_tax` decimal(25,4) DEFAULT NULL,
  `grand_total` decimal(25,4) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `status` varchar(55) NOT NULL DEFAULT 'pending',
  `shipping` decimal(25,4) NOT NULL DEFAULT '0.0000',
  `attachment` varchar(55) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_transfer_items`
--

CREATE TABLE `sma_transfer_items` (
  `id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_code` varchar(55) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `expiry` date DEFAULT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `tax_rate_id` int(11) DEFAULT NULL,
  `tax` varchar(55) DEFAULT NULL,
  `item_tax` decimal(25,4) DEFAULT NULL,
  `net_unit_cost` decimal(25,4) DEFAULT NULL,
  `subtotal` decimal(25,4) DEFAULT NULL,
  `quantity_balance` decimal(15,4) NOT NULL,
  `unit_cost` decimal(25,4) DEFAULT NULL,
  `real_unit_cost` decimal(25,4) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `product_unit_id` int(11) DEFAULT NULL,
  `product_unit_code` varchar(10) DEFAULT NULL,
  `unit_quantity` decimal(15,4) NOT NULL,
  `gst` varchar(20) DEFAULT NULL,
  `cgst` decimal(25,4) DEFAULT NULL,
  `sgst` decimal(25,4) DEFAULT NULL,
  `igst` decimal(25,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_units`
--

CREATE TABLE `sma_units` (
  `id` int(11) NOT NULL,
  `code` varchar(10) NOT NULL,
  `name` varchar(55) NOT NULL,
  `base_unit` int(11) DEFAULT NULL,
  `operator` varchar(1) DEFAULT NULL,
  `unit_value` varchar(55) DEFAULT NULL,
  `operation_value` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_units`
--

INSERT INTO `sma_units` (`id`, `code`, `name`, `base_unit`, `operator`, `unit_value`, `operation_value`) VALUES
(1, '2TB1', '2TB1', NULL, NULL, NULL, NULL),
(2, '2TB2', '2TB2', NULL, NULL, NULL, NULL),
(3, '2TB5', '2TB5', NULL, NULL, NULL, NULL),
(4, '2TB6', '2TB6', NULL, NULL, NULL, NULL),
(5, 'B831', 'B831', NULL, NULL, NULL, NULL),
(6, 'B832', 'B832', NULL, NULL, NULL, NULL),
(7, 'B835', 'B835', NULL, NULL, NULL, NULL),
(8, 'B836', 'B836', NULL, NULL, NULL, NULL),
(9, 'BC81', 'BC81', NULL, NULL, NULL, NULL),
(10, 'BC84', 'BC84', NULL, NULL, NULL, NULL),
(11, 'BC85', 'BC85', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_users`
--

CREATE TABLE `sma_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `last_ip_address` varbinary(45) DEFAULT NULL,
  `ip_address` varbinary(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(40) NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` varchar(55) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  `warehouse_id` int(10) UNSIGNED DEFAULT NULL,
  `biller_id` int(10) UNSIGNED DEFAULT NULL,
  `company_id` int(11) DEFAULT NULL,
  `show_cost` tinyint(1) DEFAULT '0',
  `show_price` tinyint(1) DEFAULT '0',
  `award_points` int(11) DEFAULT '0',
  `view_right` tinyint(1) NOT NULL DEFAULT '0',
  `edit_right` tinyint(1) NOT NULL DEFAULT '0',
  `allow_discount` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_users`
--

INSERT INTO `sma_users` (`id`, `last_ip_address`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`, `gender`, `group_id`, `warehouse_id`, `biller_id`, `company_id`, `show_cost`, `show_price`, `award_points`, `view_right`, `edit_right`, `allow_discount`) VALUES
(1, 0x3a3a31, 0x0000, 'owner', '2c8ab736b2ccab4f50e72d5fd7d21020cbb77ae7', NULL, 'owner@tecdiary.com', NULL, NULL, NULL, NULL, 1351661704, 1568816725, 1, 'Owner', 'Owner', 'Stock Manager', '012345678', NULL, 'male', 1, NULL, NULL, NULL, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sma_user_logins`
--

CREATE TABLE `sma_user_logins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `ip_address` varbinary(16) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_user_logins`
--

INSERT INTO `sma_user_logins` (`id`, `user_id`, `company_id`, `ip_address`, `login`, `time`) VALUES
(1, 1, NULL, 0x3a3a31, 'owner', '2019-06-15 12:50:40'),
(2, 1, NULL, 0x3a3a31, 'owner', '2019-06-15 13:06:09'),
(3, 1, NULL, 0x3a3a31, 'owner', '2019-06-17 11:14:40'),
(4, 1, NULL, 0x3a3a31, 'owner', '2019-06-20 11:42:20'),
(5, 1, NULL, 0x3a3a31, 'owner', '2019-09-18 14:25:25');

-- --------------------------------------------------------

--
-- Table structure for table `sma_variants`
--

CREATE TABLE `sma_variants` (
  `id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses`
--

CREATE TABLE `sma_warehouses` (
  `id` int(11) NOT NULL,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `map` varchar(255) DEFAULT NULL,
  `phone` varchar(55) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `price_group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sma_warehouses`
--

INSERT INTO `sma_warehouses` (`id`, `code`, `name`, `address`, `map`, `phone`, `email`, `price_group_id`) VALUES
(1, 'WHI', 'Warehouse 1', '<p>Address, City</p>', NULL, '012345678', 'whi@tecdiary.com', NULL),
(2, 'WHII', 'Warehouse 2', '<p>Warehouse 2, Jalan Sultan Ismail, 54000, Kuala Lumpur</p>', NULL, '0105292122', 'whii@tecdiary.com', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products`
--

CREATE TABLE `sma_warehouses_products` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL,
  `avg_cost` decimal(25,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sma_warehouses_products_variants`
--

CREATE TABLE `sma_warehouses_products_variants` (
  `id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `warehouse_id` int(11) NOT NULL,
  `quantity` decimal(15,4) NOT NULL,
  `rack` varchar(55) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `sma_addresses`
--
ALTER TABLE `sma_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `company_id` (`company_id`);

--
-- Indexes for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_adjustment_items`
--
ALTER TABLE `sma_adjustment_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `adjustment_id` (`adjustment_id`);

--
-- Indexes for table `sma_brands`
--
ALTER TABLE `sma_brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  ADD PRIMARY KEY (`captcha_id`),
  ADD KEY `word` (`word`);

--
-- Indexes for table `sma_categories`
--
ALTER TABLE `sma_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_companies`
--
ALTER TABLE `sma_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `group_id_2` (`group_id`);

--
-- Indexes for table `sma_costing`
--
ALTER TABLE `sma_costing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_deposits`
--
ALTER TABLE `sma_deposits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_expense_categories`
--
ALTER TABLE `sma_expense_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `card_no` (`card_no`);

--
-- Indexes for table `sma_gift_card_topups`
--
ALTER TABLE `sma_gift_card_topups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `card_id` (`card_id`);

--
-- Indexes for table `sma_groups`
--
ALTER TABLE `sma_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  ADD PRIMARY KEY (`ref_id`);

--
-- Indexes for table `sma_payments`
--
ALTER TABLE `sma_payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_paypal`
--
ALTER TABLE `sma_paypal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_pos_settings`
--
ALTER TABLE `sma_pos_settings`
  ADD PRIMARY KEY (`pos_id`);

--
-- Indexes for table `sma_price_groups`
--
ALTER TABLE `sma_price_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `sma_printers`
--
ALTER TABLE `sma_printers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_products`
--
ALTER TABLE `sma_products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `id` (`id`),
  ADD KEY `id_2` (`id`),
  ADD KEY `category_id_2` (`category_id`),
  ADD KEY `unit` (`unit`),
  ADD KEY `brand` (`brand`);

--
-- Indexes for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_product_prices`
--
ALTER TABLE `sma_product_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `price_group_id` (`price_group_id`);

--
-- Indexes for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_product_id_name` (`product_id`,`name`);

--
-- Indexes for table `sma_promos`
--
ALTER TABLE `sma_promos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_id` (`purchase_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quote_id` (`quote_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_returns`
--
ALTER TABLE `sma_returns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `return_id` (`return_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`return_id`),
  ADD KEY `return_id_2` (`return_id`,`product_id`);

--
-- Indexes for table `sma_sales`
--
ALTER TABLE `sma_sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sale_id` (`sale_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_id_2` (`product_id`,`sale_id`),
  ADD KEY `sale_id_2` (`sale_id`,`product_id`);

--
-- Indexes for table `sma_sessions`
--
ALTER TABLE `sma_sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `sma_settings`
--
ALTER TABLE `sma_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `sma_skrill`
--
ALTER TABLE `sma_skrill`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_stock_counts`
--
ALTER TABLE `sma_stock_counts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_stock_count_items`
--
ALTER TABLE `sma_stock_count_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_count_id` (`stock_count_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `sma_units`
--
ALTER TABLE `sma_units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `base_unit` (`base_unit`);

--
-- Indexes for table `sma_users`
--
ALTER TABLE `sma_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`,`warehouse_id`,`biller_id`),
  ADD KEY `group_id_2` (`group_id`,`company_id`);

--
-- Indexes for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_variants`
--
ALTER TABLE `sma_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- Indexes for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `option_id` (`option_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `warehouse_id` (`warehouse_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `sma_addresses`
--
ALTER TABLE `sma_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_adjustments`
--
ALTER TABLE `sma_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_adjustment_items`
--
ALTER TABLE `sma_adjustment_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_brands`
--
ALTER TABLE `sma_brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_calendar`
--
ALTER TABLE `sma_calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_captcha`
--
ALTER TABLE `sma_captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_categories`
--
ALTER TABLE `sma_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sma_combo_items`
--
ALTER TABLE `sma_combo_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_companies`
--
ALTER TABLE `sma_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sma_costing`
--
ALTER TABLE `sma_costing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_currencies`
--
ALTER TABLE `sma_currencies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_customer_groups`
--
ALTER TABLE `sma_customer_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sma_date_format`
--
ALTER TABLE `sma_date_format`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sma_deliveries`
--
ALTER TABLE `sma_deliveries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_deposits`
--
ALTER TABLE `sma_deposits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_expenses`
--
ALTER TABLE `sma_expenses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_expense_categories`
--
ALTER TABLE `sma_expense_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_gift_cards`
--
ALTER TABLE `sma_gift_cards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_gift_card_topups`
--
ALTER TABLE `sma_gift_card_topups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_groups`
--
ALTER TABLE `sma_groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sma_login_attempts`
--
ALTER TABLE `sma_login_attempts`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_notifications`
--
ALTER TABLE `sma_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_order_ref`
--
ALTER TABLE `sma_order_ref`
  MODIFY `ref_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_payments`
--
ALTER TABLE `sma_payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_permissions`
--
ALTER TABLE `sma_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_pos_register`
--
ALTER TABLE `sma_pos_register`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_price_groups`
--
ALTER TABLE `sma_price_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_printers`
--
ALTER TABLE `sma_printers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_products`
--
ALTER TABLE `sma_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1113;

--
-- AUTO_INCREMENT for table `sma_product_photos`
--
ALTER TABLE `sma_product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_product_prices`
--
ALTER TABLE `sma_product_prices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_product_variants`
--
ALTER TABLE `sma_product_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_promos`
--
ALTER TABLE `sma_promos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_purchases`
--
ALTER TABLE `sma_purchases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_purchase_items`
--
ALTER TABLE `sma_purchase_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_quotes`
--
ALTER TABLE `sma_quotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_quote_items`
--
ALTER TABLE `sma_quote_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_returns`
--
ALTER TABLE `sma_returns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_return_items`
--
ALTER TABLE `sma_return_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_sales`
--
ALTER TABLE `sma_sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_sale_items`
--
ALTER TABLE `sma_sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_stock_counts`
--
ALTER TABLE `sma_stock_counts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_stock_count_items`
--
ALTER TABLE `sma_stock_count_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_suspended_bills`
--
ALTER TABLE `sma_suspended_bills`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_suspended_items`
--
ALTER TABLE `sma_suspended_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_tax_rates`
--
ALTER TABLE `sma_tax_rates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sma_transfers`
--
ALTER TABLE `sma_transfers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_transfer_items`
--
ALTER TABLE `sma_transfer_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_units`
--
ALTER TABLE `sma_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `sma_users`
--
ALTER TABLE `sma_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `sma_user_logins`
--
ALTER TABLE `sma_user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sma_variants`
--
ALTER TABLE `sma_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_warehouses`
--
ALTER TABLE `sma_warehouses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sma_warehouses_products`
--
ALTER TABLE `sma_warehouses_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sma_warehouses_products_variants`
--
ALTER TABLE `sma_warehouses_products_variants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
