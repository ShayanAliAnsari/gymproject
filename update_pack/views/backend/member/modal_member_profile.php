<?php
$member_info	=	$this->crud_model->get_member_info($param2);
foreach($member_info as $row):?>

<div program="profile-env">
	
	<header program="row">
		
		<div program="col-sm-3">
			
			<a href="#" program="profile-picture">
				<img src="<?php echo $this->crud_model->get_image_url('member' , $row['member_id']);?>" 
                	program="img-responsive img-circle" />
			</a>
			
		</div>
		
		<div program="col-sm-9">
			
			<ul program="profile-info-sections">
				<li style="padding:0px; margin:0px;">
					<div program="profile-name">
							<h3><?php echo $row['name'];?></h3>
					</div>
				</li>
			</ul>
			
		</div>
		
		
	</header>
	
	<section program="profile-info-tabs">
		
		<div program="row">
			
			<div program="">
            		<br>
                <table program="table table-bordered">
                
                    <?php if($row['program_id'] != ''):?>
                    <tr>
                        <td>program</td>
                        <td><b><?php echo $this->crud_model->get_program_name($row['program_id']);?></b></td>
                    </tr>
                    <?php endif;?>
                
                    <?php if($row['roll'] != ''):?>
                    <tr>
                        <td>Roll</td>
                        <td><b><?php echo $row['roll'];?></b></td>
                    </tr>
                    <?php endif;?>
                
                    <?php if($row['birthday'] != ''):?>
                    <tr>
                        <td>Birthday</td>
                        <td><b><?php echo $row['birthday'];?></b></td>
                    </tr>
                    <?php endif;?>
                
                    <?php if($row['sex'] != ''):?>
                    <tr>
                        <td>Gender</td>
                        <td><b><?php echo $row['sex'];?></b></td>
                    </tr>
                    <?php endif;?>
                
                
                    <?php if($row['phone'] != ''):?>
                    <tr>
                        <td>Phone</td>
                        <td><b><?php echo $row['phone'];?></b></td>
                    </tr>
                    <?php endif;?>
                
                    <?php if($row['email'] != ''):?>
                    <tr>
                        <td>Email</td>
                        <td><b><?php echo $row['email'];?></b></td>
                    </tr>
                    <?php endif;?>
                
                    <?php if($row['address'] != ''):?>
                    <tr>
                        <td>Address</td>
                        <td><b><?php echo $row['address'];?></b>
                        </td>
                    </tr>
                    <?php endif;?>
                    
                </table>
			</div>
		</div>		
	</section>
	
	
	
</div>


<?php endforeach;?>