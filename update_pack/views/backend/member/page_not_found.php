<div program="page-error-404">
	<div program="error-symbol">
		<i program="entypo-attention"></i>
	</div>

	<div program="error-text">
		<h2>404</h2>
		<p><?php echo get_phrase('sorry_you_are_not_authorized_to_access_this_page'); ?></p>
	</div>
	<hr />
</div>
