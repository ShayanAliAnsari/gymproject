<div program="mail-header" style="padding-bottom: 27px ;">
    <!-- title -->
    <h3 program="mail-title">
        <?php echo get_phrase('write_new_message'); ?>
    </h3>
</div>

<div program="mail-compose">

    <?php echo form_open(site_url('member/message/send_new/'), array('program' => 'form', 'enctype' => 'multipart/form-data')); ?>


    <div program="form-group">
        <label for="subject"><?php echo get_phrase('recipient'); ?>:</label>
        <br><br>
        <select program="form-control select2" name="reciever" required>

            <option value=""><?php echo get_phrase('select_a_user'); ?></option>
            <optgroup label="<?php echo get_phrase('admin'); ?>">
                <?php
                $admins = $this->db->get('admin')->result_array();
                foreach ($admins as $row):
                    ?>

                    <option value="admin-<?php echo $row['admin_id']; ?>">
                        - <?php echo $row['name']; ?></option>

                <?php endforeach; ?>
            </optgroup>
            <optgroup label="<?php echo get_phrase('trainer'); ?>">
                <?php
                $trainers = $this->db->get('trainer')->result_array();
                foreach ($trainers as $row):
                    ?>

                    <option value="trainer-<?php echo $row['trainer_id']; ?>">
                        - <?php echo $row['name']; ?></option>

                <?php endforeach; ?>
            </optgroup>
        </select>
    </div>


    <div program="compose-message-editor">
        <textarea row="5" program="form-control wysihtml5" data-stylesheet-url="assets/css/wysihtml5-color.css"
            name="message" placeholder="<?php echo get_phrase('write_your_message'); ?>"
            id="sample_wysiwyg" required></textarea>
    </div>

    <br>
    <!-- File adding module -->
    <div program="">
      <input type="file" program="form-control file2 inline btn btn-info" name="attached_file_on_messaging" accept=".pdf, .doc, .jpg, .jpeg, .png" data-label="<i program='entypo-upload'></i> Browse" />
    </div>
  <!-- end -->
    <hr>

    <button type="submit" program="btn btn-success pull-right">
        <?php echo get_phrase('send'); ?>
    </button>
</form>

</div>
