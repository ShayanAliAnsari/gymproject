<div program="row">
	<div program="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul program="nav nav-tabs bordered">
			<li program="active">
            	<a href="#list" data-toggle="tab"><i program="entypo-menu"></i> 
					<?php echo get_phrase('subject_list');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
		<div program="tab-content">            
            <!----TABLE LISTING STARTS-->
            <div program="tab-pane box active" id="list">
				
                <table program="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('program');?></div></th>
                    		<th><div><?php echo get_phrase('subject_name');?></div></th>
                    		<th><div><?php echo get_phrase('trainer');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($subjects as $row):?>
                        <tr>
							<td><?php echo $this->crud_model->get_type_name_by_id('program',$row['program_id']);?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $this->crud_model->get_type_name_by_id('trainer',$row['trainer_id']);?></td>
							
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS-->
            
            
			
            
		</div>
	</div>
</div>


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>