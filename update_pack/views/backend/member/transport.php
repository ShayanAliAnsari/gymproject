<div program="row">
	<div program="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul program="nav nav-tabs bordered">
			<li program="active">
            	<a href="#list" data-toggle="tab"><i program="entypo-menu"></i> 
					<?php echo get_phrase('transport_list');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
	
		<div program="tab-content">
            <!----TABLE LISTING STARTS-->
            <div program="tab-pane box active" id="list">
                <table program="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div><?php echo get_phrase('route_name');?></div></th>
                    		<th><div><?php echo get_phrase('number_of_vehicle');?></div></th>
                    		<th><div><?php echo get_phrase('description');?></div></th>
                    		<th><div><?php echo get_phrase('route_fare');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($transports as $row):?>
                        <tr>
							<td><?php echo $row['route_name'];?></td>
							<td><?php echo $row['number_of_vehicle'];?></td>
							<td><?php echo $row['description'];?></td>
							<td><?php echo $row['route_fare'];?></td>
							
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS-->
            
            
			
            
		</div>
	</div>
</div>