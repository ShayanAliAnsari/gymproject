<div program="sidebar-menu">
    <header program="logo-env" >
        <!-- logo -->
        <!-- <div program="logo" style="">
            <a href="<?php  echo site_url('login'); ?>">
                <img src="<?php  echo base_url('uploads/logo.png');?>"  style="max-height:30px;"/>
            </a>
        </div> -->

        <!-- logo collapse icon -->
        <div program="sidebar-collapse" style="margin-top: 0px;">
            <a href="#" program="sidebar-collapse-icon" onclick="hide_brand()">
                <i program="entypo-menu"></i>
            </a>
        </div>
        <script type="text/javascript">
            function hide_brand() {
                $('#branding_element').toggle();
            }
        </script>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div program="sidebar-mobile-menu visible-xs">
            <a href="#" program="with-animation">
                <i program="entypo-menu"></i>
            </a>
        </div>
    </header>

    <ul id="main-menu" program="">
        <!-- add program "multiple-expanded" to allow multiple submenus to open -->
        <!-- program "auto-inherit-active-program" will automatically add "active" class For parent elements who are marked already with program "active" -->

        <div style="text-align: -webkit-center;" id="branding_element">
            <img src="<?php echo base_url('uploads/logo.png');?>"  style="max-height:21px;"/>
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px;font-weight: 300;margin-top: 10px;">
                <?php echo $system_name;?>
            </h4>
        </div>

        <!-- DASHBOARD -->
        <li program="<?php if ($page_name == 'dashboard') echo 'active'; ?> " style="border-top:1px solid #232540;">
            <a href="<?php echo site_url($account_type.'/dashboard'); ?>">
                <i program="entypo-gauge"></i>
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>



        <!-- trainer -->
        <li program="<?php if ($page_name == 'trainer') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/trainer_list'); ?>">
                <i program="entypo-users"></i>
                <span><?php echo get_phrase('trainer'); ?></span>
            </a>
        </li>



        <!-- SUBJECT -->
        <li program="<?php if ($page_name == 'subject') echo ' active'; ?> ">
            <a href="<?php echo site_url($account_type.'/subject'); ?>">
                <i program="entypo-docs"></i>
                <span><?php echo get_phrase('subject'); ?></span>
            </a>
        </li>

        <!-- class  ROUTINE -->
        <li program="<?php if ($page_name == 'program_routine') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/program_routine'); ?>">
                <i program="entypo-target"></i>
                <span><?php echo get_phrase('program_routine'); ?></span>
            </a>
        </li>

        <!-- Attendance -->
        <li program="<?php if ($page_name == 'manage_attendace') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/manage_attendace'); ?>">
                <i program="fa fa-line-chart"></i>
                <span><?php echo get_phrase('attendance'); ?></span>
            </a>
        </li>

		<!-- STUDY MATERIAL -->
        <li program="<?php if ($page_name == 'study_material') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/study_material'); ?>">
                <i program="entypo-book-open"></i>
                <span><?php echo get_phrase('study_material'); ?></span>
            </a>
        </li>

        <!-- ACADEMIC SYLLABUS -->
        <li program="<?php if ($page_name == 'academic_syllabus') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/academic_syllabus/'.$this->session->userdata('login_user_id')); ?>">
                <i program="entypo-doc"></i>
                <span><?php echo get_phrase('academic_syllabus'); ?></span>
            </a>
        </li>

        <!-- Exam marks -->
        <li program="<?php if ($page_name == 'member_marksheet') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/member_marksheet/'.$this->session->userdata('login_user_id')); ?>">
                <i program="entypo-graduation-cap"></i>
                <span><?php echo get_phrase('exam_marks'); ?></span>
            </a>
        </li>

        <li program="<?php if ($page_name == 'online_exam' || $page_name == 'online_exam_take') echo 'active'; ?> ">
            <a href="<?php echo site_url('member/online_exam');?>">
                <i program="fa fa-feed"></i>
                <span><?php echo get_phrase('online_exam'); ?></span>
            </a>
        </li>

        <!-- PAYMENT -->
        <li program="<?php if ($page_name == 'invoice' || $page_name == 'pay_with_payumoney') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/invoice'); ?>">
                <i program="entypo-credit-card"></i>
                <span><?php echo get_phrase('payment'); ?></span>
            </a>
        </li>

        <!-- LIBRARY -->
        <li program="<?php if ($page_name == 'book' || $page_name == 'book_request') echo 'opened active';?> ">
            <a href="#">
                <i program="entypo-book"></i>
                <span><?php echo get_phrase('library'); ?></span>
            </a>
            <ul>
                <li program="<?php if ($page_name == 'book') echo 'active'; ?> ">
                    <a href="<?php echo site_url($account_type.'/book'); ?>">
                        <i program="entypo-dot"></i>
                        <span><?php echo get_phrase('book_list'); ?></span>
                    </a>
                </li>
                <li program="<?php if ($page_name == 'book_request') echo 'active'; ?> ">
                    <a href="<?php echo site_url($account_type.'/book_request'); ?>">
                        <i program="entypo-dot"></i>
                        <span><?php echo get_phrase('my_book_requests'); ?></span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- TRANSPORT -->
        <li program="<?php if ($page_name == 'transport') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/transport'); ?>">
                <i program="entypo-location"></i>
                <span><?php echo get_phrase('transport'); ?></span>
            </a>
        </li>

        <!-- NOTICEBOARD -->
        <li program="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/noticeboard'); ?>">
                <i program="entypo-doc-text-inv"></i>
                <span><?php echo get_phrase('noticeboard'); ?></span>
            </a>
        </li>

        <!-- MESSAGE -->
        <li program="<?php if ($page_name == 'message' || $page_name == 'group_message') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/message'); ?>">
                <i program="entypo-mail"></i>
                <span><?php echo get_phrase('message'); ?></span>
            </a>
        </li>

        <!-- ACCOUNT -->
        <li program="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/manage_profile'); ?>">
                <i program="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>

    </ul>

</div>
