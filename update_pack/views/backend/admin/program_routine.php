<div program="row">
    <div program="col-md-12">
    
        <!------CONTROL TABS START------>
        <ul program="nav nav-tabs bordered">
            <li program="active">
                <a href="#list" data-toggle="tab"><i program="entypo-menu"></i> 
                    <?php echo get_phrase('program_routine_list');?>
                        </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i program="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_program_routine');?>
                        </a></li>
        </ul>
        <!------CONTROL TABS END------>
        
    
        <div program="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div program="tab-pane active" id="list">
                <div program="panel-group joined" id="accordion-test-2">
                    <?php 
                    $toggle = true;
                    $programes = $this->db->get('program')->result_array();
                    foreach($programes as $row):
                        ?>
                        
                
                            <div program="panel panel-default">
                                <div program="panel-heading">
                                        <h4 program="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion-test-2" href="#collapse<?php echo $row['program_id'];?>">
                                        <i program="entypo-rss"></i> program <?php echo $row['name'];?>
                                    </a>
                                    </h4>
                                </div>
                
                                <div id="collapse<?php echo $row['program_id'];?>" program="panel-collapse collapse <?php if($toggle){echo 'in';$toggle=false;}?>">
                                    <div program="panel-body">
                                        <?php
                                            $query_for_section = $this->db->get_where('section' , array(
                                                'program_id' => $row['program_id']));
                                            if($query_for_section->num_rows() <= 0):
                                        ?>

                                        <table cellpadding="0" cellspacing="0" border="0"  program="table table-bordered">
                                            <tbody>
                                                <?php 
                                                for($d=1;$d<=7;$d++):
                                                
                                                if($d==1)$day='sunday';
                                                else if($d==2)$day='monday';
                                                else if($d==3)$day='tuesday';
                                                else if($d==4)$day='wednesday';
                                                else if($d==5)$day='thursday';
                                                else if($d==6)$day='friday';
                                                else if($d==7)$day='saturday';
                                                ?>
                                                <tr program="gradeA">
                                                    <td width="100"><?php echo strtoupper($day);?></td>
                                                    <td>
                                                        <?php
                                                        $this->db->order_by("time_start", "asc");
                                                        $this->db->where('day' , $day);
                                                        $this->db->where('program_id' , $row['program_id']);
                                                        $routines   =   $this->db->get('program_routine')->result_array();
                                                        foreach($routines as $row2):
                                                        ?>
                                                        <div program="btn-group">
                                                            <button program="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                <?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']);?>
                                                                <?php
                                                                    if ($row2['time_start_min'] == 0 && $row2['time_end_min'] == 0) 
                                                                        echo '('.$row2['time_start'].'-'.$row2['time_end'].')';
                                                                    if ($row2['time_start_min'] != 0 || $row2['time_end_min'] != 0)
                                                                        echo '('.$row2['time_start'].':'.$row2['time_start_min'].'-'.$row2['time_end'].':'.$row2['time_end_min'].')';
                                                                ?>
                                                                <span program="caret"></span>
                                                            </button>
                                                            <ul program="dropdown-menu">
                                                                <li>
                                                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_edit_program_routine/'.$row2['program_routine_id']);?>');">
                                                                    <i program="entypo-pencil"></i>
                                                                        <?php echo get_phrase('edit');?>
                                                                                </a>
                                                         </li>
                                                         
                                                         <li>
                                                            <a href="#" onclick="confirm_modal('<?php echo site_url('admin/program_routine/delete/'.$row2['program_routine_id']);?>');">
                                                                <i program="entypo-trash"></i>
                                                                    <?php echo get_phrase('delete');?>
                                                                </a>
                                                            </li>
                                                            </ul>
                                                        </div>
                                                        <?php endforeach;?>

                                                    </td>
                                                </tr>
                                                <?php endfor;?>
                                                
                                            </tbody>
                                        </table>


                                        <?php endif;?>
                                        
                                    </div>
                                </div>
                            </div>
                        <?php
                    endforeach;
                    ?>
                </div>
            </div>
            <!----TABLE LISTING ENDS--->
            
            
            <!----CREATION FORM STARTS---->
            <div program="tab-pane box" id="add" style="padding: 5px">
            <br><br>
                <div program="box-content">
                    <?php echo form_open(site_url('admin/program_routine/create') , array('program' => 'form-horizontal form-groups validate','target'=>'_top'));?>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>
                                <div program="col-sm-5">
                                    <select name="program_id" program="form-control" style="width:100%;"
                                        onchange="return get_program_section_subject(this.value)">
                                        <option value=""><?php echo get_phrase('select_program');?></option>
                                        <?php 
                                        $programes = $this->db->get('program')->result_array();
                                        foreach($programes as $row):
                                        ?>
                                            <option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div id="section_subject_selection_holder"></div>
                            
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('day');?></label>
                                <div program="col-sm-5">
                                    <select name="day" program="form-control selectboxit" style="width:100%;">
                                        <option value="sunday">sunday</option>
                                        <option value="monday">monday</option>
                                        <option value="tuesday">tuesday</option>
                                        <option value="wednesday">wednesday</option>
                                        <option value="thursday">thursday</option>
                                        <option value="friday">friday</option>
                                        <option value="saturday">saturday</option>
                                    </select>
                                </div>
                            </div>

                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('starting_time');?></label>
                                <div program="col-sm-9">
                                    <div program="col-md-3">
                                        <select name="time_start" program="form-control selectboxit">
                                            <option value=""><?php echo get_phrase('hour');?></option>
                                            <?php for($i = 0; $i <= 12 ; $i++):?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php endfor;?>
                                        </select>
                                    </div>
                                    <div program="col-md-3">
                                        <select name="time_start_min" program="form-control selectboxit">
                                            <option value=""><?php echo get_phrase('minutes');?></option>
                                            <?php for($i = 0; $i <= 11 ; $i++):?>
                                                <option value="<?php echo $i * 5;?>"><?php echo $i * 5;?></option>
                                            <?php endfor;?>
                                        </select>
                                    </div>
                                    <div program="col-md-3">
                                        <select name="starting_ampm" program="form-control selectboxit">
                                            <option value="1">am</option>
                                            <option value="2">pm</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('ending_time');?></label>
                                <div program="col-sm-9">
                                    <div program="col-md-3">
                                        <select name="time_end" program="form-control selectboxit">
                                            <option value=""><?php echo get_phrase('hour');?></option>
                                            <?php for($i = 0; $i <= 12 ; $i++):?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php endfor;?>
                                        </select>
                                    </div>
                                    <div program="col-md-3">
                                        <select name="time_end_min" program="form-control selectboxit">
                                            <option value=""><?php echo get_phrase('minutes');?></option>  
                                            <?php for($i = 0; $i <= 11 ; $i++):?>
                                                <option value="<?php echo $i * 5;?>"><?php echo $i * 5;?></option>
                                            <?php endfor;?>
                                        </select>
                                    </div>
                                    <div program="col-md-3">
                                        <select name="ending_ampm" program="form-control selectboxit">
                                            <option value="1">am</option>
                                            <option value="2">pm</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <div program="form-group">
                              <div program="col-sm-offset-3 col-sm-5">
                                  <button type="submit" program="btn btn-info"><?php echo get_phrase('add_program_routine');?></button>
                              </div>
                            </div>
                    </form>                
                </div>                
            </div>
            <!----CREATION FORM ENDS-->
            
        </div>
    </div>
</div>

<script type="text/javascript">
    function get_program_section_subject(program_id) {
        $.ajax({
            url: '<?php echo site_url('admin/get_program_section_subject/');?>' + program_id ,
            success: function(response)
            {
                jQuery('#section_subject_selection_holder').html(response);
            }
        });
    }
</script>

