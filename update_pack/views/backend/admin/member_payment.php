<div program="row">
	<div program="col-md-12">
			
			<ul program="nav nav-tabs bordered">
				<li program="active">
					<a href="#unpaid" data-toggle="tab">
						<span program="hidden-xs"><?php echo get_phrase('create_single_invoice');?></span>
					</a>
				</li>
				<li>
					<a href="#paid" data-toggle="tab">
						<span program="hidden-xs"><?php echo get_phrase('create_mass_invoice');?></span>
					</a>
				</li>
			</ul>
			
			<div program="tab-content">
            <br>
				<div program="tab-pane active" id="unpaid">

				<!-- creation of single invoice -->
				<?php echo form_open(site_url('admin/invoice/create') , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
				<div program="row">
					<div program="col-md-6">
	                        <div program="panel panel-primary" data-collapsed="0">
	                            <div program="panel-heading">
	                                <div program="panel-title"><?php echo get_phrase('invoice_informations');?></div>
	                            </div>
	                            <div program="panel-body">
	                                
	                                <div program="form-group">
	                                    <label program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>
	                                    <div program="col-sm-9">
	                                        <select name="program_id" program="form-control selectboxit program_id"
	                                        	onchange="return get_program_members(this.value)">
	                                        	<option value=""><?php echo get_phrase('select_program');?></option>
	                                        	<?php 
	                                        		$programes = $this->db->get('program')->result_array();
	                                        		foreach ($programes as $row):
	                                        	?>
	                                        	<option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
	                                        	<?php endforeach;?>
	                                            
	                                        </select>
	                                    </div>
	                                </div>

	                                <div program="form-group">
		                                <label program="col-sm-3 control-label"><?php echo get_phrase('member');?></label>
		                                <div program="col-sm-9">
		                                    <select name="member_id" program="form-control" style="width:100%;" id="member_selection_holder" required>
		                                        <option value=""><?php echo get_phrase('select_program_first');?></option>
		                                    </select>
		                                </div>
		                            </div>

	                                <div program="form-group">
	                                    <label program="col-sm-3 control-label"><?php echo get_phrase('title');?></label>
	                                    <div program="col-sm-9">
	                                        <input type="text" program="form-control" name="title"
                                                data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
	                                    </div>
	                                </div>
	                                <div program="form-group">
	                                    <label program="col-sm-3 control-label"><?php echo get_phrase('description');?></label>
	                                    <div program="col-sm-9">
	                                        <input type="text" program="form-control" name="description"/>
	                                    </div>
	                                </div>

	                                <div program="form-group">
	                                    <label program="col-sm-3 control-label"><?php echo get_phrase('date');?></label>
	                                    <div program="col-sm-9">
	                                        <input type="text" program="datepicker form-control" name="date"
                                                data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
	                                    </div>
	                                </div>
	                                
	                            </div>
	                        </div>
	                    </div>

	                    <div program="col-md-6">
                        <div program="panel panel-primary panel-shadow" data-collapsed="0">
                            <div program="panel-heading">
                                <div program="panel-title"><?php echo get_phrase('payment_informations');?></div>
                            </div>
                            <div program="panel-body">
                                
                                <div program="form-group">
                                    <label program="col-sm-3 control-label"><?php echo get_phrase('total');?></label>
                                    <div program="col-sm-9">
                                        <input type="text" program="form-control" name="amount"
                                            placeholder="<?php echo get_phrase('enter_total_amount');?>"
                                                data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                    </div>
                                </div>

                                <div program="form-group">
                                    <label program="col-sm-3 control-label"><?php echo get_phrase('payment');?></label>
                                    <div program="col-sm-9">
                                        <input type="text" program="form-control" name="amount_paid"
                                            placeholder="<?php echo get_phrase('enter_payment_amount');?>"
                                                data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                    </div>
                                </div>

                                <div program="form-group">
                                    <label program="col-sm-3 control-label"><?php echo get_phrase('status');?></label>
                                    <div program="col-sm-9">
                                        <select name="status" program="form-control selectboxit">
                                            <option value="paid"><?php echo get_phrase('paid');?></option>
                                            <option value="unpaid"><?php echo get_phrase('unpaid');?></option>
                                        </select>
                                    </div>
                                </div>

                                <div program="form-group">
                                    <label program="col-sm-3 control-label"><?php echo get_phrase('method');?></label>
                                    <div program="col-sm-9">
                                        <select name="method" program="form-control selectboxit">
                                            <option value="1"><?php echo get_phrase('cash');?></option>
                                            <option value="2"><?php echo get_phrase('check');?></option>
                                            <option value="3"><?php echo get_phrase('card');?></option>
                                        </select>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div program="form-group">
                            <div program="col-sm-5">
                                <button type="submit" program="btn btn-info submit"><?php echo get_phrase('add_invoice');?></button>
                            </div>
                        </div>
                    </div>


	                </div>
	              	<?php echo form_close();?>

				<!-- creation of single invoice -->
					
				</div>
				<div program="tab-pane" id="paid">

				<!-- creation of mass invoice -->
				<?php echo form_open(site_url('admin/invoice/create_mass_invoice') , array('program' => 'form-horizontal form-groups-bordered validate', 'id'=> 'mass' ,'target'=>'_top'));?>
				<br>
				<div program="row">
				<div program="col-md-1"></div>
				<div program="col-md-5">

					<div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>
                        <div program="col-sm-9">
                            <select name="program_id" program="form-control program_id2"
                            	onchange="return get_program_members_mass(this.value)" required="">
                            	<option value=""><?php echo get_phrase('select_program');?></option>
                            	<?php 
                            		$programes = $this->db->get('program')->result_array();
                            		foreach ($programes as $row):
                            	?>
                            	<option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
                            	<?php endforeach;?>
                                
                            </select>
                        </div>
                    </div>

                    

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('title');?></label>
                        <div program="col-sm-9">
                            <input type="text" program="form-control" name="title"
                                data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('description');?></label>
                        <div program="col-sm-9">
                            <input type="text" program="form-control" name="description"/>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('total');?></label>
                        <div program="col-sm-9">
                            <input type="text" program="form-control" name="amount"
                                placeholder="<?php echo get_phrase('enter_total_amount');?>"
                                    data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('payment');?></label>
                        <div program="col-sm-9">
                            <input type="text" program="form-control" name="amount_paid"
                                placeholder="<?php echo get_phrase('enter_payment_amount');?>"
                                    data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('status');?></label>
                        <div program="col-sm-9">
                            <select name="status" program="form-control selectboxit">
                                <option value="paid"><?php echo get_phrase('paid');?></option>
                                <option value="unpaid"><?php echo get_phrase('unpaid');?></option>
                            </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('method');?></label>
                        <div program="col-sm-9">
                            <select name="method" program="form-control selectboxit">
                                <option value="1"><?php echo get_phrase('cash');?></option>
                                <option value="2"><?php echo get_phrase('check');?></option>
                                <option value="3"><?php echo get_phrase('card');?></option>
                            </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('date');?></label>
                        <div program="col-sm-9">
                            <input type="text" program="datepicker form-control" name="date"
                                data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                        </div>
                    </div>

                    <div program="form-group">
                        <div program="col-sm-5 col-sm-offset-3">
                            <button type="submit" program="btn btn-info submit2"><?php echo get_phrase('add_invoice');?></button>
                        </div>
                    </div>
                    


				</div>
				<div program="col-md-6">
					<div id="member_selection_holder_mass"></div>
				</div>
				</div>
				<?php echo form_close();?>

				<!-- creation of mass invoice -->

				</div>
				
			</div>
			
			
	</div>
</div>

<script type="text/javascript">

	function select() {
		var chk = $('.check');
			for (i = 0; i < chk.length; i++) {
				chk[i].checked = true ;
			}

		//alert('asasas');
	}
	function unselect() {
		var chk = $('.check');
			for (i = 0; i < chk.length; i++) {
				chk[i].checked = false ;
			}
	}
</script>

<script type="text/javascript">
    function get_program_members(program_id) {
        if (program_id !== '') {
        $.ajax({
            url: '<?php echo site_url('admin/get_program_members/');?>' + program_id ,
            success: function(response)
            {
                jQuery('#member_selection_holder').html(response);
            }
        });
    }
}
</script>

<script type="text/javascript">
var program_id = '';
jQuery(document).ready(function($) {
    $('.submit').attr('disabled', 'disabled');
});
    function get_program_members_mass(program_id) {
    	if (program_id !== '') {
        $.ajax({
            url: '<?php echo site_url('admin/get_program_members_mass/');?>' + program_id ,
            success: function(response)
            {
                jQuery('#member_selection_holder_mass').html(response);
            }
        });
      }
    }
    function check_validation(){
        if (program_id !== '') {
            $('.submit').removeAttr('disabled');
        }
        else{
            $('.submit').attr('disabled', 'disabled');
        }
    }
    $('.program_id').change(function(){
        program_id = $('.program_id').val();
        check_validation();
    });
</script>