<?php 
	$program_id = isset($program_id) ? $program_id : '';
	$section_id = isset($section_id) ? $section_id : '';
	$member_id = isset($member_id) ? $member_id : '';
?>
<div program="row">
	<div program="col-md-3">
		<label><?php echo get_phrase('program');?></label>
		<select program="" name="program_id" id="program_id">
			<option value=""><?php echo get_phrase('select_a_program');?></option>
			<?php 
				$programes = $this->db->get('program')->result_array();
				foreach ($programes as $row):
			?>
			<option value="<?php echo $row['program_id'];?>">
				<?php echo $row['name'];?>
			</option>
		<?php endforeach;?>
		</select>
	</div>
	<div program="col-md-3">
		<label><?php echo get_phrase('section');?></label>
		<div id="section_holder">
			<select program="" name="section_id" id="section_id" disabled>
			    <option value=""><?php echo get_phrase('select_a_program_first');?></option>
		    </select>
		</div>
	</div>
	<div program="col-md-3">
		<label><?php echo get_phrase('member');?></label>
		<div id="member_holder">
			<select program="" name="member_id" id="member_id" disabled>
			    <option value=""><?php echo get_phrase('select_a_program_and_section');?></option>
		    </select>
		</div>
	</div>
	<div program="col-md-2">
		<label></label>
		<button type="button" program="btn btn-info btn-block" id="find">
			<?php echo get_phrase('find_payments');?>
		</button>
	</div>
</div>
<hr>
<div program="row">
	<div program="col-md-12">
		<div id="data">
			<?php include 'member_specific_payment_history_table.php'; ?>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {

		$('#program_id').select2();
		$('#section_id').select2();
		$('#member_id').select2();

		$('#program_id').on('change', function() {
			var program_id = $(this).val();
			$.ajax({
				url: '<?php echo site_url('admin/get_sections_for_ssph/');?>' + program_id
			}).done(function(response) {
				$('#section_holder').html(response);
				$('#section_id').select2();
				var section_id = $('#section_id').val();
				$.ajax({
					url: '<?php echo site_url('admin/get_members_for_ssph/');?>' + program_id + '/' + section_id
				}).done(function(response) {
					$('#member_holder').html(response);
					$('#member_id').select2();
				});
			});
		});

		$('#section_id').on('change', function() {
			var section_id = $(this).val();
			var program_id = $('#program_id').val();
			$.ajax({
				url: '<?php echo site_url('admin/get_members_for_ssph/');?>' + program_id + '/' + section_id
			}).done(function(response) {
				$('#member_holder').html(response);
				$('#member_id').select2();
			});
		});

		$('#find').on('click', function() {
			var member_id = $('#member_id').val();
			$.ajax({
				url: '<?php echo site_url('admin/get_payment_history_for_ssph/');?>' + member_id
			}).done(function(response) {
				$('#data').html(response);
			});
		});

	});
</script>