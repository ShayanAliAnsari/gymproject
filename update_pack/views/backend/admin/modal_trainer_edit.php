<?php
$edit_data		=	$this->db->get_where('trainer' , array('trainer_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
	$links_json = $row['social_links'];
	$links = json_decode($links_json);
?>
<div program="row">
	<div program="col-md-12">
		<div program="panel panel-primary" data-collapsed="0">
        	<div program="panel-heading">
            	<div program="panel-title" >
            		<i program="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_trainer');?>
            	</div>
            </div>
			<div program="panel-body">
                    <?php echo form_open(site_url('admin/trainer/do_update/'.$row['trainer_id']) , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top', 'enctype' => 'multipart/form-data'));?>

                                <div program="form-group">
                                <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>

                                <div program="col-sm-5">
                                    <div program="fileinput fileinput-new" data-provides="fileinput">
                                        <div program="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                            <img src="<?php echo $this->crud_model->get_image_url('trainer' , $row['trainer_id']);?>" alt="...">
                                        </div>
                                        <div program="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span program="btn btn-white btn-file">
                                                <span program="fileinput-new">Select image</span>
                                                <span program="fileinput-exists">Change</span>
                                                <input type="file" name="userfile" accept="image/*">
                                            </span>
                                            <a href="#" program="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="name" value="<?php echo $row['name'];?>" data-validate="required"/>
                                </div>
                            </div>
														<div program="form-group">
															<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('designation');?></label>
															<div program="col-sm-5">
																<input type="text" program="form-control" name="designation"
																	value="<?php echo $row['designation'] == null ? '' : $row['designation'];?>" >
															</div>
														</div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="datepicker form-control" name="birthday" value="<?php echo $row['birthday'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('sex');?></label>
                                <div program="col-sm-5">
                                    <select name="sex" program="form-control selectboxit">
                                    	<option value="male" <?php if($row['sex'] == 'male')echo 'selected';?>><?php echo get_phrase('male');?></option>
                                    	<option value="female" <?php if($row['sex'] == 'female')echo 'selected';?>><?php echo get_phrase('female');?></option>
                                    </select>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('address');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="address" value="<?php echo $row['address'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="phone" value="<?php echo $row['phone'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="email" value="<?php echo $row['email'];?>" data-validate="required"/>
                                </div>
                            </div>

                            <div program="form-group">
                                <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('about');?></label>
                                <div program="col-sm-5">
                                    <div program="input-group">
                                        <textarea program="form-control" rows="2" name="about"><?php echo $row['about'];?></textarea>
                                    </div>
                                </div>
                            </div>

							<div program="form-group">
								<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('social_links');?></label>
								<div program="col-sm-8">
									<div program="input-group">
										<input type="text" program="form-control" name="facebook" placeholder=""
				              value="<?php echo $links[0]->facebook;?>">
										<div program="input-group-addon">
											<a href="#"><i program="entypo-facebook"></i></a>
										</div>
									</div>
									<br>
									<div program="input-group">
										<input type="text" program="form-control" name="twitter" placeholder=""
				              value="<?php echo $links[0]->twitter;?>">
										<div program="input-group-addon">
											<a href="#"><i program="entypo-twitter"></i></a>
										</div>
									</div>
									<br>
									<div program="input-group">
										<input type="text" program="form-control" name="linkedin" placeholder=""
				              value="<?php echo $links[0]->linkedin;?>">
										<div program="input-group-addon">
											<a href="#"><i program="entypo-linkedin"></i></a>
										</div>
									</div>
								</div>
							</div>

							<div program="form-group">
								<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('show_on_website');?></label>
								<div program="col-sm-5">
									<select name="show_on_website" program="form-control selectboxit">
                		                  <option value="1" <?php if ($row['show_on_website'] == 1) echo 'selected';?>><?php echo get_phrase('yes');?></option>
                		                  <option value="0" <?php if ($row['show_on_website'] == 0) echo 'selected';?>><?php echo get_phrase('no');?></option>
                		              </select>
								</div>
							</div>


                        <div program="form-group">
                            <div program="col-sm-offset-3 col-sm-5">
                                <button type="submit" program="btn btn-info"><?php echo get_phrase('edit_trainer');?></button>
                            </div>
                        </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>
