<hr />
<div program="row">
    <div program="col-md-12">
        <blockquote program="blockquote-blue">
            <p>
                <strong>member Promotion Instruction</strong>
            </p>
            <p>
                Promoting member from the present class  To the next class  Will create an enrollment of that member to
                the next session. Make sure to select correct class Options from the select menu before promoting.If you don't want
                to promote a member to the next program, please select that option. That will not promote the member to the next program
                but it will create an enrollment to the next session but in the same program.
            </p>
        </blockquote>
    </div>
</div>
<?php echo form_open(site_url('admin/member_promotion/promote'));?>
<div program="row">
<?php 
    $running_year_array             = explode ( "-" , $running_year ); 
    $next_year_first_index          = $running_year_array[1];
    $next_year_second_index         = $running_year_array[1]+1;
    $next_year                      = $next_year_first_index. "-" .$next_year_second_index;
?>
    <div program="col-md-12">
        <div program="panel panel-primary">
            <div program="panel-heading">
                <h3 program="panel-title"><?php echo get_phrase('setup_payment_information') ?></h3>
            </div>
            <div program="panel-body">

                <div program="form-group">
                    <div program="col-sm-3" style="margin-top: 15px;">
                    <label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('current_session');?></label>
                        <select name="running_year" program="form-control selectboxit">
                        <option value="<?php echo $running_year;?>">
                            <?php echo $running_year;?>
                        </option>
                        </select>
                    </div>
                </div>

                <div program="form-group">
                    <div program="col-sm-3">
                    <label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('promote_to_session');?></label>
                        <select name="promotion_year" program="form-control selectboxit" id="promotion_year">
                        <option value="<?php echo $next_year;?>">
                            <?php echo $next_year;?>
                        </option>
                        </select>
                    </div>
                </div>

                <div program="form-group">
                    <div program="col-sm-3">
                    <label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('promotion_from_program');?></label>
                        <select name="promotion_from_program_id" id="from_program_id" program="form-control selectboxit"
                            >
                            <option value=""><?php echo get_phrase('select');?></option>
                            <?php
                                $programes = $this->db->get('program')->result_array();
                                foreach($programes as $row):
                            ?>
                            <option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>
                <div program="form-group">
                    <div program="col-sm-3">
                    <label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('promotion_to_program');?></label>
                        <select name="promotion_to_program_id" id="to_program_id" program="form-control selectboxit">
                            <option value=""><?php echo get_phrase('select');?></option>
                            <?php foreach($programes as $row):?>
                            <option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
                            <?php endforeach;?>
                        </select>
                    </div>
                </div>

                <center>
                    <button program="btn btn-info" type="button" style="margin:10px;" onclick="get_members_to_promote('<?php echo $running_year;?>')">
                        <?php echo get_phrase('manage_promotion');?></button>
                </center>

            </div>
        </div>
	</div>

</div>

<div id="members_for_promotion_holder"></div>

<?php echo form_close();?>

<script type="text/javascript">
    
    function get_members_to_promote(running_year)
    {
        from_program_id   = $("#from_program_id").val();
        to_program_id     = $("#to_program_id").val();
        promotion_year  = $("#promotion_year").val();
        
        if (from_program_id == "" || to_program_id == "") {
            toastr.error("<?php echo get_phrase('select_program_for_promotion_to_and_from');?>")
            return false;
        }
        $.ajax({
            url: '<?php echo site_url('admin/get_members_to_promote/');?>' + from_program_id + '/' + to_program_id + '/' + running_year + '/' + promotion_year,
            success: function(response)
            {
                jQuery('#members_for_promotion_holder').html(response);
            }
        });
        return false;
    }

</script>