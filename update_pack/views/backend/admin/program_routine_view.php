<a href="<?php echo site_url('admin/program_routine_add');?>"
    program="btn btn-primary pull-right">
        <i program="entypo-plus-circled"></i>
        <?php echo get_phrase('add_program_routine');?>
    </a> 
<br><br><br>

<?php
	$query = $this->db->get_where('section' , array('program_id' => $program_id));
	if($query->num_rows() > 0):
		$sections = $query->result_array();
	foreach($sections as $row):
?>
<div program="row">
	
    <div program="col-md-12">
        <div program="panel panel-primary">
            <div program="panel-heading">
                <h3 program="panel-title">
                    <?php echo get_phrase('program');?> - <?php echo $this->db->get_where('program' , array('program_id' => $program_id))->row()->name;?> : 
                    <?php echo get_phrase('section');?> - <?php echo $this->db->get_where('section' , array('section_id' => $row['section_id']))->row()->name;?>
                </h3>
            </div>
            <div program="panel-body">
                <table cellpadding="0" cellspacing="0" border="0"  program="table table-bordered">
                    <tbody>
                        <?php 
                        for($d=1;$d<=7;$d++):
                        
                        if($d==1)$day='sunday';
                        else if($d==2)$day='monday';
                        else if($d==3)$day='tuesday';
                        else if($d==4)$day='wednesday';
                        else if($d==5)$day='thursday';
                        else if($d==6)$day='friday';
                        else if($d==7)$day='saturday';
                        ?>
                        <tr program="gradeA">
                            <td width="100"><?php echo strtoupper($day);?></td>
                            <td>
                                <?php
                                $this->db->order_by("time_start", "asc");
                                $this->db->where('day' , $day);
                                $this->db->where('program_id' , $program_id);
                                $this->db->where('section_id' , $row['section_id']);
                                $this->db->where('year' , $running_year);
                                $routines   =   $this->db->get('program_routine')->result_array();
                                foreach($routines as $row2):
                                ?>
                                <div program="btn-group">
                                    <button program="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']);?>
                                        <?php
                                            if ($row2['time_start_min'] == 0 && $row2['time_end_min'] == 0) 
                                                echo '('.$row2['time_start'].'-'.$row2['time_end'].')';
                                            if ($row2['time_start_min'] != 0 || $row2['time_end_min'] != 0)
                                                echo '('.$row2['time_start'].':'.$row2['time_start_min'].'-'.$row2['time_end'].':'.$row2['time_end_min'].')';
                                        ?>
                                        <span program="caret"></span>
                                    </button>
                                    <ul program="dropdown-menu">
                                        <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_edit_program_routine/'.$row2['program_routine_id']);?>');">
                                            <i program="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                                        </a>
                                 </li>
                                 
                                 <li>
                                    <a href="#" onclick="confirm_modal('<?php echo site_url('admin/program_routine/delete/'.$row2['program_routine_id']);?>');">
                                        <i program="entypo-trash"></i>
                                            <?php echo get_phrase('delete');?>
                                        </a>
                                    </li>
                                    </ul>
                                </div>
                                <?php endforeach;?>

                            </td>
                        </tr>
                        <?php endfor;?>
                        
                    </tbody>
                </table>

                <center>
                    <a href="<?php echo site_url('admin/program_routine_print_view/'.$program_id.'/'.$row['section_id']);?>"
                        program="btn btn-primary btn-md pull-right" target="_blank">
                            <i program="entypo-print"></i> <?php echo get_phrase('print');?>
                    </a>
                </center>
            </div>
        </div>
        

    </div>

</div>
<?php endforeach;?>
<?php endif;?>