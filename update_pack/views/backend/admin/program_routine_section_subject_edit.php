<?php
	$query = $this->db->get_where('section' , array('program_id' => $program_id));
	if($query->num_rows() > 0): 
		$section_id = $this->db->get_where('program_routine' , array('program_routine_id' => $program_routine_id))->row()->section_id;
		$sections   = $query->result_array();
?>
	
	<div program="form-group">
		<label program="col-sm-3 control-label"><?php echo get_phrase('section');?></label>
		<div program="col-sm-5">
			<select name="section_id" program="form-control selectboxit">
				<option value=""><?php echo get_phrase('select_section');?></option>
				<?php foreach($sections as $section):?>
				<option value="<?php echo $section['section_id'];?>"
					<?php if($section['section_id'] == $section_id) echo 'selected';?>>
						<?php echo $section['name'];?>
					</option>
				<?php endforeach;?>
			</select>
		</div>
	</div>

<?php endif;?>

<?php
	$subject_id = $this->db->get_where('program_routine' , array('program_routine_id' => $program_routine_id))->row()->subject_id;
?>
<div program="form-group">
		<label program="col-sm-3 control-label"><?php echo get_phrase('subject');?></label>
		<div program="col-sm-5">
			<select name="subject_id" program="form-control selectboxit">
				<option value=""><?php echo get_phrase('select_subject');?></option>
				<?php 
					$subjects = $this->db->get_where('subject' , array('program_id' => $program_id))->result_array();
					foreach($subjects as $subject):
				?>
				<option value="<?php echo $subject['subject_id'];?>"
					<?php if($subject['subject_id'] == $subject_id) echo 'selected';?>>
						<?php echo $subject['name'];?>
					</option>
				<?php endforeach;?>
			</select>
		</div>
	</div>


	<script type="text/javascript">
	$(document).ready(function() {
        if($.isFunction($.fn.selectBoxIt))
		{
			$("select.selectboxit").each(function(i, el)
			{
				var $this = $(el),
					opts = {
						showFirstOption: attrDefault($this, 'first-option', true),
						'native': attrDefault($this, 'native', false),
						defaultText: attrDefault($this, 'text', ''),
					};
					
				$this.addprogram('visible');
				$this.selectBoxIt(opts);
			});
		}
    });
	
</script>