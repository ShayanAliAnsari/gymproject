<div program="row">
	<div program="col-md-12">
		<div program="panel panel-primary">
            <div program="panel-heading">
                <h3 program="panel-title"><?php echo get_phrase('add_new_routine') ?></h3>
            </div>
            <div program="panel-body">
                <?php echo form_open(site_url('admin/program_routine/create') , array('program' => 'form-horizontal form-groups validate','target'=>'_top'));?>
                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>
                        <div program="col-sm-5">
                            <select name="program_id" id = "program_id" program="form-control selectboxit" style="width:100%;"
                                onchange="return get_program_section_subject(this.value)">
                                <option value=""><?php echo get_phrase('select_program');?></option>
                                <?php 
                                $programes = $this->db->get('program')->result_array();
                                foreach($programes as $row):
                                ?>
                                    <option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div id="section_subject_selection_holder"></div>
                    
                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('day');?></label>
                        <div program="col-sm-5">
                            <select name="day" program="form-control selectboxit" style="width:100%;">
                                <option value="sunday">sunday</option>
                                <option value="monday">monday</option>
                                <option value="tuesday">tuesday</option>
                                <option value="wednesday">wednesday</option>
                                <option value="thursday">thursday</option>
                                <option value="friday">friday</option>
                                <option value="saturday">saturday</option>
                            </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('starting_time');?></label>
                        <div program="col-sm-9">
                            <div program="col-md-3">
                                <select name="time_start" id= "starting_hour" program="form-control selectboxit">
                                    <option value=""><?php echo get_phrase('hour');?></option>
                                    <?php for($i = 0; $i <= 12 ; $i++):?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>
                            </div>
                            <div program="col-md-3">
                                <select name="time_start_min" id= "starting_minute" program="form-control selectboxit">
                                    <option value=""><?php echo get_phrase('minutes');?></option>
                                    <?php for($i = 0; $i <= 11 ; $i++):?>
                                        <option value="<?php echo $i * 5;?>"><?php echo $i * 5;?></option>
                                    <?php endfor;?>
                                </select>
                            </div>
                            <div program="col-md-3">
                                <select name="starting_ampm" program="form-control selectboxit">
                                    <option value="1">am</option>
                                    <option value="2">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('ending_time');?></label>
                        <div program="col-sm-9">
                            <div program="col-md-3">
                                <select name="time_end" id= "ending_hour" program="form-control selectboxit">
                                    <option value=""><?php echo get_phrase('hour');?></option>
                                    <?php for($i = 0; $i <= 12 ; $i++):?>
                                        <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                    <?php endfor;?>
                                </select>
                            </div>
                            <div program="col-md-3">
                                <select name="time_end_min" id= "ending_minute" program="form-control selectboxit">
                                    <option value=""><?php echo get_phrase('minutes');?></option>  
                                    <?php for($i = 0; $i <= 11 ; $i++):?>
                                        <option value="<?php echo $i * 5;?>"><?php echo $i * 5;?></option>
                                    <?php endfor;?>
                                </select>
                            </div>
                            <div program="col-md-3">
                                <select name="ending_ampm" program="form-control selectboxit">
                                    <option value="1">am</option>
                                    <option value="2">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div program="form-group">
                        <div program="col-sm-offset-3 col-sm-5">
                            <button type="submit" id= "add_program_routine" program="btn btn-info"><?php echo get_phrase('add_program_routine');?></button>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
        		

	</div>
</div>


<script type="text/javascript">
var program_id = '';
var starting_hour = '';
var starting_minute = '';
var ending_hour = '';
var ending_minute = '';
jQuery(document).ready(function($) {
    $('#add_program_routine').attr('disabled','disabled')
});
    function get_program_section_subject(program_id) {
        $.ajax({
            url: '<?php echo site_url('admin/get_program_section_subject/');?>' + program_id ,
            success: function(response)
            {
                jQuery('#section_subject_selection_holder').html(response);
            }
        });
    }
function check_validation(){
    console.log('program_id: '+program_id+' starting_hour:'+starting_hour+' starting_minute: '+starting_minute+' ending_hour: '+ending_hour+' ending_minute: '+ending_minute);
    if(program_id !== '' && starting_hour !== '' && starting_minute  !== '' && ending_hour  !== '' && ending_minute !== ''){
        $('#add_program_routine').removeAttr('disabled');
    }    
}
$('#program_id').change(function() {
    program_id = $('#program_id').val();
    check_validation();
});
$('#starting_hour').change(function() {
    starting_hour = $('#starting_hour').val();
    check_validation();
});
$('#starting_minute').change(function() {
    starting_minute = $('#starting_minute').val();
    check_validation();
});
$('#ending_hour').change(function() {
    ending_hour = $('#ending_hour').val();
    check_validation();
});
$('#ending_minute').change(function() {
    ending_minute = $('#ending_minute').val();
    check_validation();
});


</script>