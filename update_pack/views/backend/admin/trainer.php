
            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_trainer_add/');?>');"
            	program="btn btn-primary pull-right">
                <i program="entypo-plus-circled"></i>
            	<?php echo get_phrase('add_new_trainer');?>
                </a>
                <br><br>
               <table program="table table-bordered datatable" id="trainers">
                    <thead>
                        <tr>
                            <th width="60"><div><?php echo get_phrase('id');?></div></th>
                            <th width="80"><div><?php echo get_phrase('photo');?></div></th>
                            <th><div><?php echo get_phrase('name');?></div></th>
                            <th><div><?php echo get_phrase('email').'/'.get_phrase('username');?></div></th>
                            <th><div><?php echo get_phrase('phone');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                    </thead>
                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

	jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#trainers').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_trainers') ?>",
                "dataType": "json",
                "type": "POST",
            },
            "columns": [
                { "data": "trainer_id" },
                { "data": "photo" },
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" },
                { "data": "options" },
            ],
            "columnDefs": [
                {
                    "targets": [1,5],
                    "orderable": false
                },
            ]
        });
	});

    function trainer_edit_modal(trainer_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_trainer_edit/');?>' + trainer_id);
    }

    function trainer_delete_confirm(trainer_id) {
        confirm_modal('<?php echo site_url('admin/trainer/delete/');?>' + trainer_id);
    }

</script>
