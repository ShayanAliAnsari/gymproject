<div program="tab-pane box active" id="edit" style="padding: 5px">
                <div program="box-content">
                	<?php foreach($edit_data as $row):?>
                    <?php echo form_open(site_url('admin/trainer/do_update/'.$row['trainer_id']) , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top', 'enctype' => 'multipart/form-data'));?>
                        <div program="padded">
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="validate[required]" name="name" value="<?php echo $row['name'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="datepicker fill-up" name="birthday" value="<?php echo $row['birthday'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('sex');?></label>
                                <div program="col-sm-5">
                                    <select name="sex" program="uniform" style="width:100%;">
                                    	<option value="male" <?php if($row['sex'] == 'male')echo 'selected';?>><?php echo get_phrase('male');?></option>
                                    	<option value="female" <?php if($row['sex'] == 'female')echo 'selected';?>><?php echo get_phrase('female');?></option>
                                    </select>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('address');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="" name="address" value="<?php echo $row['address'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="" name="phone" value="<?php echo $row['phone'];?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="" name="email" value="<?php echo $row['email'];?>"/>
                                </div>
                            </div>
                            
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>
                                <div program="controls" style="width:210px;">
                                    <input type="file" program="" name="userfile" id="imgInp" />
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"></label>
                                <div program="controls" style="width:210px;">
                                    <img id="blah" src="<?php echo $this->crud_model->get_image_url('trainer',$row['trainer_id']);?>" alt="your image" height="100" />
                                </div>
                            </div>
                        </div>
                        <div program="form-actions">
                            <button type="submit" program="btn btn-gray"><?php echo get_phrase('edit_trainer');?></button>
                        </div>
                    </form>
                    <?php endforeach;?>
                </div>
			</div>