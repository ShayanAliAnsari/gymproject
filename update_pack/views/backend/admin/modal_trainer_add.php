<div program="row">
	<div program="col-md-12">
		<div program="panel panel-primary" data-collapsed="0">
        	<div program="panel-heading">
            	<div program="panel-title" >
            		<i program="entypo-plus-circled"></i>
					<?php echo get_phrase('add_trainer');?>
            	</div>
            </div>
			<div program="panel-body">

      <?php echo form_open(site_url('admin/trainer/create/') , array('program' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>

						<div program="col-sm-5">
							<input type="text" program="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('designation');?></label>
						<div program="col-sm-5">
							<input type="text" program="form-control" name="designation" value="" >
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>

						<div program="col-sm-5">
							<input type="text" program="form-control datepicker" name="birthday" value="" data-start-view="2">
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('gender');?></label>

						<div program="col-sm-5">
							<select name="sex" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
                              <option value="male"><?php echo get_phrase('male');?></option>
                              <option value="female"><?php echo get_phrase('female');?></option>
                          </select>
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('address');?></label>

						<div program="col-sm-5">
							<input type="text" program="form-control" name="address" value="" >
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>
						<div program="col-sm-5">
							<input type="text" program="form-control" name="phone" value="" >
						</div>
					</div>

					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
						<div program="col-sm-5">
							<input type="text" program="form-control" name="email" value="" data-validate="required">
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('password');?></label>

						<div program="col-sm-5">
							<input type="password" program="form-control" name="password" value="" data-validate="required">
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('about');?></label>
						<div program="col-sm-5">
							<div program="input-group">
								<textarea program="form-control" rows="2" name="about"></textarea>
							</div>
						</div>
					</div>
					
					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('social_links');?></label>
						<div program="col-sm-8">
							<div program="input-group">
								<input type="text" program="form-control" name="facebook" placeholder=""
		              value="">
								<div program="input-group-addon">
									<a href="#"><i program="entypo-facebook"></i></a>
								</div>
							</div>
							<br>
							<div program="input-group">
								<input type="text" program="form-control" name="twitter" placeholder=""
		              value="">
								<div program="input-group-addon">
									<a href="#"><i program="entypo-twitter"></i></a>
								</div>
							</div>
							<br>
							<div program="input-group">
								<input type="text" program="form-control" name="linkedin" placeholder=""
		              value="">
								<div program="input-group-addon">
									<a href="#"><i program="entypo-linkedin"></i></a>
								</div>
							</div>
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('show_on_website');?></label>
						<div program="col-sm-5">
							<select name="show_on_website" program="form-control selectboxit">
                  <option value="1"><?php echo get_phrase('yes');?></option>
                  <option value="0"><?php echo get_phrase('no');?></option>
              </select>
						</div>
					</div>

					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>

						<div program="col-sm-5">
							<div program="fileinput fileinput-new" data-provides="fileinput">
								<div program="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="http://placehold.it/200x200" alt="...">
								</div>
								<div program="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span program="btn btn-white btn-file">
										<span program="fileinput-new">Select image</span>
										<span program="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" program="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
					</div>

                    <div program="form-group">
						<div program="col-sm-offset-3 col-sm-5">
							<button type="submit" program="btn btn-info"><?php echo get_phrase('add_trainer');?></button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
