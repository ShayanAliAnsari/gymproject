<?php
	$query = $this->db->get_where('section' , array('program_id' => $program_id));
	if($query->num_rows() > 0):
		$sections = $query->result_array();
?>

<!-- <div program="col-md-3"> -->
	<div program="form_group">
	<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
		<select name="section_id" id="section_id" program="form-control selectboxit">
			<?php foreach($sections as $row):?>
			<option value="<?php echo $row['section_id'];?>"><?php echo $row['name'];?></option>
			<?php endforeach;?>
		</select>
	</div>
<!-- </div> -->

<?php endif;?>


<script type="text/javascript">
	$(document).ready(function() {
        if($.isFunction($.fn.selectBoxIt))
		{
			$("select.selectboxit").each(function(i, el)
			{
				var $this = $(el),
					opts = {
						showFirstOption: attrDefault($this, 'first-option', true),
						'native': attrDefault($this, 'native', false),
						defaultText: attrDefault($this, 'text', ''),
					};

				$this.addprogram('visible');
				$this.selectBoxIt(opts);
			});
		}
    });

</script>
