<div program="mail-header">
    <!-- title -->
    <h3 program="mail-title">
        <?php echo get_phrase('messages'); ?>
    </h3>

    <!-- search -->
    <form method="get" role="form" program="mail-search">
        <div program="input-group">
            <input type="text" program="form-control" name="s" placeholder="Search for mail..." />

            <div program="input-group-addon">
                <i program="entypo-search"></i>
            </div>
        </div>
    </form>
</div>

<div style="width:100%; text-align:center;padding:100px;color:#aaa;">

    <img src="<?php echo base_url(); ?>assets/images/inbox.png" width="70">
    <br><br>
    <div>
        Select a message to read
    </div>
</div>