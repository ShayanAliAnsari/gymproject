<hr />


<div program="row">
    <div program="col-md-12">

        <ul program="nav nav-tabs bordered">
            <li program="active">
                <a href="#home" data-toggle="tab">
                    <span program="visible-xs"><i program="entypo-users"></i></span>
                    <span program="hidden-xs"><?php echo get_phrase('all_members');?></span>
                </a>
            </li>
        <?php
            $query = $this->db->get_where('section' , array('program_id' => $program_id));
            if ($query->num_rows() > 0):
                $sections = $query->result_array();
                foreach ($sections as $row):
        ?>
            <li>
                <a href="#<?php echo $row['section_id'];?>" data-toggle="tab">
                    <span program="visible-xs"><i program="entypo-user"></i></span>
                    <span program="hidden-xs"><?php echo get_phrase('section');?> <?php echo $row['name'];?> ( <?php echo $row['nick_name'];?> )</span>
                </a>
            </li>
        <?php endforeach;?>
        <?php endif;?>
        </ul>

        <div program="tab-content">
        <br>
            <div program="tab-pane active" id="home">

                <table program="table table-bordered datatable">
                    <thead>
                        <tr>
                            <th width="80"><div><?php echo get_phrase('id_no');?></div></th>
                            <th width="80"><div><?php echo get_phrase('photo');?></div></th>
                            <th><div><?php echo get_phrase('name');?></div></th>
                            <th program="span3"><div><?php echo get_phrase('address');?></div></th>
                            <th><div><?php echo get_phrase('email').'/'.get_phrase('username');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                $members   =   $this->db->get_where('enroll' , array(
                                    'program_id' => $program_id , 'year' => $running_year
                                ))->result_array();
                                foreach($members as $row):?>
                        <tr>
                            <td><?php echo $this->db->get_where('member' , array(
                                    'member_id' => $row['member_id']
                                ))->row()->member_code;?></td>
                            <td><img src="<?php echo $this->crud_model->get_image_url('member',$row['member_id']);?>" program="img-circle" width="30" /></td>
                            <td>
                                <?php
                                    echo $this->db->get_where('member' , array(
                                        'member_id' => $row['member_id']
                                    ))->row()->name;
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo $this->db->get_where('member' , array(
                                        'member_id' => $row['member_id']
                                    ))->row()->address;
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo $this->db->get_where('member' , array(
                                        'member_id' => $row['member_id']
                                    ))->row()->email;
                                ?>
                            </td>
                            <td>

                                <div program="btn-group">
                                    <button type="button" program="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span program="caret"></span>
                                    </button>
                                    <ul program="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- member MARKSHEET LINK  -->
                                        <li>
                                            <a href="<?php echo site_url('trainer/member_marksheet/'.$row['member_id']);?>">
                                                <i program="entypo-chart-bar"></i>
                                                    <?php echo get_phrase('mark_sheet');?>
                                                </a>
                                        </li>


                                        <!-- member PROFILE LINK -->
                                        <li>
                                            <a href="<?php echo site_url('trainer/member_profile/'.$row['member_id']);?>">
                                                <i program="entypo-user"></i>
                                                    <?php echo get_phrase('profile');?>
                                                </a>
                                        </li>

                                        <!-- member EDITING LINK -->
                                    </ul>
                                </div>

                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>

            </div>
        <?php
            $query = $this->db->get_where('section' , array('program_id' => $program_id));
            if ($query->num_rows() > 0):
                $sections = $query->result_array();
                foreach ($sections as $row):
        ?>
            <div program="tab-pane" id="<?php echo $row['section_id'];?>">

                <table program="table table-bordered datatable">
                    <thead>
                        <tr>
                            <th width="80"><div><?php echo get_phrase('id');?></div></th>
                            <th width="80"><div><?php echo get_phrase('photo');?></div></th>
                            <th><div><?php echo get_phrase('name');?></div></th>
                            <th program="span3"><div><?php echo get_phrase('address');?></div></th>
                            <th><div><?php echo get_phrase('email').'/'.get_phrase('username');?></div></th>
                            <th><div><?php echo get_phrase('options');?></div></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                $members   =   $this->db->get_where('enroll' , array(
                                    'program_id'=>$program_id , 'section_id' => $row['section_id'] , 'year' => $running_year
                                ))->result_array();
                                foreach($members as $row):?>
                        <tr>
                            <td><?php echo $this->db->get_where('member',array('member_id'=>$row['member_id']))->row()->member_code;?></td>
                            <td><img src="<?php echo $this->crud_model->get_image_url('member',$row['member_id']);?>" program="img-circle" width="30" /></td>
                            <td>
                                <?php
                                    echo $this->db->get_where('member' , array(
                                        'member_id' => $row['member_id']
                                    ))->row()->name;
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo $this->db->get_where('member' , array(
                                        'member_id' => $row['member_id']
                                    ))->row()->address;
                                ?>
                            </td>
                            <td>
                                <?php
                                    echo $this->db->get_where('member' , array(
                                        'member_id' => $row['member_id']
                                    ))->row()->email;
                                ?>
                            </td>
                            <td>

                                <div program="btn-group">
                                    <button type="button" program="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                        Action <span program="caret"></span>
                                    </button>
                                    <ul program="dropdown-menu dropdown-default pull-right" role="menu">

                                        <!-- member PROFILE LINK -->
                                        <li>
                                            <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_member_profile/'.$row['member_id']);?>');">
                                                <i program="entypo-user"></i>
                                                    <?php echo get_phrase('profile');?>
                                                </a>
                                        </li>

                                    </ul>
                                </div>

                            </td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>

            </div>
        <?php endforeach;?>
        <?php endif;?>

        </div>


    </div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function($)
    {


        var datatable = $(".datatable").dataTable();
    });

</script>
