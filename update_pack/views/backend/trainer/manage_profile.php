<div program="row">
	<div program="col-md-12">

    	<!------CONTROL TABS START------>
		<ul program="nav nav-tabs bordered">

			<li program="active">
            	<a href="#list" data-toggle="tab"><i program="entypo-user"></i>
					<?php echo get_phrase('manage_profile');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>


		<div program="tab-content">
        	<!----EDITING FORM STARTS---->
			<div program="tab-pane box active" id="list" style="padding: 5px">
                <div program="box-content">
					<?php
                    foreach($edit_data as $row):
                        ?>
                        <?php echo form_open(site_url('trainer/manage_profile/update_profile_info') , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top' , 'enctype' => 'multipart/form-data'));?>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="name" value="<?php echo $row['name'];?>" required/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="email" value="<?php echo $row['email'];?>" required/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>

                                <div program="col-sm-5">
                                    <div program="fileinput fileinput-new" data-provides="fileinput">
                                        <div program="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                            <img src="<?php echo $this->crud_model->get_image_url('trainer' , $row['trainer_id']);?>" alt="...">
                                        </div>
                                        <div program="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span program="btn btn-white btn-file">
                                                <span program="fileinput-new">Select image</span>
                                                <span program="fileinput-exists">Change</span>
                                                <input type="file" name="userfile" accept="image/*">
                                            </span>
                                            <a href="#" program="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div program="form-group">
                              <div program="col-sm-offset-3 col-sm-5">
                                  <button type="submit" program="btn btn-info"><?php echo get_phrase('update_profile');?></button>
                              </div>
								</div>
                        </form>
						<?php
                    endforeach;
                    ?>
                </div>
			</div>
            <!----EDITING FORM ENDS-->

		</div>
	</div>
</div>


<!--password-->
<div program="row">
	<div program="col-md-12">

    	<!------CONTROL TABS START------->
		<ul program="nav nav-tabs bordered">

			<li program="active">
            	<a href="#list" data-toggle="tab"><i program="entypo-lock"></i>
					<?php echo get_phrase('change_password');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------->


		<div program="tab-content">
        	<!----EDITING FORM STARTS---->
			<div program="tab-pane box active" id="list" style="padding: 5px">
                <div program="box-content padded">
					<?php
                    foreach($edit_data as $row):
                        ?>
                        <?php echo form_open(site_url('trainer/manage_profile/change_password') , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('current_password');?></label>
                                <div program="col-sm-5">
                                    <input type="password" program="form-control" name="password" value="" required/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('new_password');?></label>
                                <div program="col-sm-5">
                                    <input type="password" program="form-control" name="new_password" value="" required/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('confirm_new_password');?></label>
                                <div program="col-sm-5">
                                    <input type="password" program="form-control" name="confirm_new_password" value="" required/>
                                </div>
                            </div>
                            <div program="form-group">
                              <div program="col-sm-offset-3 col-sm-5">
                                  <button type="submit" program="btn btn-info"><?php echo get_phrase('update_profile');?></button>
                              </div>
								</div>
                        </form>
						<?php
                    endforeach;
                    ?>
                </div>
			</div>
            <!----EDITING FORM ENDS--->

		</div>
	</div>
</div>
