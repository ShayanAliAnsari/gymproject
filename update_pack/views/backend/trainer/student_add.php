<div program="row">
    <div program="col-md-8">
        <div program="panel panel-primary" data-collapsed="0">
            <div program="panel-heading">
                <div program="panel-title" >
                    <i program="entypo-plus-circled"></i>
                    <?php echo get_phrase('addmission_form'); ?>
                </div>
            </div>
            <div program="panel-body">

                <?php echo form_open(site_url('trainer/member/create/'), array('program' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div program="form-group">
                        <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>

                        <div program="col-sm-5">
                            <input type="text" program="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('parent');?></label>

                        <div program="col-sm-5">
                            <select name="parent_id" program="form-control select2" required>
                              <option value=""><?php echo get_phrase('select');?></option>
                              <?php
                                $parents = $this->db->get('parent')->result_array();
                                foreach($parents as $row):
                                    ?>
                                    <option value="<?php echo $row['parent_id'];?>">
                                        <?php echo $row['name'];?>
                                    </option>
                                <?php
                                endforeach;
                              ?>
                          </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>

                        <div program="col-sm-5">
                            <select name="program_id" program="form-control" data-validate="required" id="program_id"
                                data-message-required="<?php echo get_phrase('value_required');?>"
                                    onchange="return get_program_sections(this.value)">
                              <option value=""><?php echo get_phrase('select');?></option>
                              <?php
                                $programes = $this->db->get('program')->result_array();
                                foreach($programes as $row):
                                    ?>
                                    <option value="<?php echo $row['program_id'];?>">
                                            <?php echo $row['name'];?>
                                            </option>
                                <?php
                                endforeach;
                              ?>
                          </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('section');?></label>
                            <div program="col-sm-5">
                                <select name="section_id" program="form-control" id="section_selector_holder">
                                    <option value=""><?php echo get_phrase('select_program_first');?></option>

                                </select>
                            </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('roll');?></label>

                        <div program="col-sm-5">
                            <input type="text" program="form-control" name="roll" value="" >
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>

                        <div program="col-sm-5">
                            <input type="text" program="form-control datepicker" name="birthday" value="" data-start-view="2">
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('gender');?></label>

                        <div program="col-sm-5">
                            <select name="sex" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
                              <option value="male"><?php echo get_phrase('male');?></option>
                              <option value="female"><?php echo get_phrase('female');?></option>
                          </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('address');?></label>

                        <div program="col-sm-5">
                            <input type="text" program="form-control" name="address" value="" >
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>

                        <div program="col-sm-5">
                            <input type="text" program="form-control" name="phone" value="" >
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
                        <div program="col-sm-5">
                            <input type="text" program="form-control" name="email" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="">
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('password');?></label>

                        <div program="col-sm-5">
                            <input type="password" program="form-control" name="password" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" >
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('dormitory');?></label>

                        <div program="col-sm-5">
                            <select name="dormitory_id" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
                                  <?php
                                    $dormitories = $this->db->get('dormitory')->result_array();
                                    foreach($dormitories as $row):
                                  ?>
                                    <option value="<?php echo $row['dormitory_id'];?>"><?php echo $row['name'];?></option>
                                <?php endforeach;?>
                          </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('transport_route');?></label>

                        <div program="col-sm-5">
                            <select name="transport_id" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
                                  <?php
                                    $transports = $this->db->get('transport')->result_array();
                                    foreach($transports as $row):
                                  ?>
                                    <option value="<?php echo $row['transport_id'];?>"><?php echo $row['route_name'];?></option>
                                <?php endforeach;?>
                          </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>

                        <div program="col-sm-5">
                            <div program="fileinput fileinput-new" data-provides="fileinput">
                                <div program="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                    <img src="http://placehold.it/200x200" alt="...">
                                </div>
                                <div program="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                <div>
                                    <span program="btn btn-white btn-file">
                                        <span program="fileinput-new">Select image</span>
                                        <span program="fileinput-exists">Change</span>
                                        <input type="file" name="userfile" accept="image/*">
                                    </span>
                                    <a href="#" program="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div program="form-group">
                        <div program="col-sm-offset-3 col-sm-5">
                            <button type="submit" program="btn btn-info"><?php echo get_phrase('add_member');?></button>
                        </div>
                    </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <div program="col-md-4">
        <blockquote program="blockquote-blue">
            <p>
                <strong>member Admission Notes</strong>
            </p>
            <p>
                Admitting new members will automatically create an enrollment to the selected class In the running session.
                Please check and recheck the informations you have inserted because once you admit new member, you won't be able
                to edit his/her program,roll,section without promoting to the next session.
            </p>
        </blockquote>
    </div>

</div>

<script type="text/javascript">

    function get_program_sections(program_id) {

        $.ajax({
            url: '<?php echo site_url('trainer/get_program_section/'); ?>' + program_id,
            success: function (response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });

    }

</script>