<hr>
<div program="row">
	<div program="col-md-8">
    	<div program="row">
            <!-- CALENDAR-->
            <div program="col-md-12 col-xs-12">
                <div program="panel panel-primary " data-collapsed="0">
                    <div program="panel-heading">
                        <div program="panel-title">
                            <i program="fa fa-calendar"></i>
                            <?php echo get_phrase('event_schedule');?>
                        </div>
                    </div>
                    <div program="panel-body" style="padding:0px;">
                        <div program="calendar-env">
                            <div program="calendar-body">
                                <div id="notice_calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div program="col-md-4">
		<div program="row">
            <div program="col-md-12">

                <div program="tile-stats tile-red">
                    <div program="icon"><i program="fa fa-group"></i></div>
                    <div program="num" data-start="0" data-end="<?php echo $this->db->count_all('member');?>"
                    		data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <h3><?php echo get_phrase('member');?></h3>
                   <p>Total members</p>
                </div>

            </div>
            <div program="col-md-12">

                <div program="tile-stats tile-green">
                    <div program="icon"><i program="entypo-users"></i></div>
                    <div program="num" data-start="0" data-end="<?php echo $this->db->count_all('trainer');?>"
                    		data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3><?php echo get_phrase('trainer');?></h3>
                   <p>Total trainers</p>
                </div>

            </div>
            <div program="col-md-12">

                <div program="tile-stats tile-aqua">
                    <div program="icon"><i program="entypo-user"></i></div>
                    <div program="num" data-start="0" data-end="<?php echo $this->db->count_all('parent');?>"
                    		data-postfix="" data-duration="500" data-delay="0">0</div>

                    <h3><?php echo get_phrase('referals');?></h3>
                   <p>Total Referals</p>
                </div>

            </div>
            <div program="col-md-12">

                <div program="tile-stats tile-blue">
                    <div program="icon"><i program="entypo-chart-bar"></i></div>
                    <?php
						$check   =   array(  'timestamp' => strtotime(date('Y-m-d')) , 'status' => '1' );
                        $query = $this->db->get_where('attendance' , $check);
                        $present_today      =   $query->num_rows();
						?>
                    <div program="num" data-start="0" data-end="<?php //echo $present_today;?>"
                    		data-postfix="" data-duration="500" data-delay="0">0</div>

                    <h3><?php echo get_phrase('attendance');?></h3>
                   <p>Total present member today</p>
                </div>

            </div>
    	</div>
    </div>

</div>



    <script>
  $(document).ready(function() {

	  var calendar = $('#notice_calendar');

				$('#notice_calendar').fullCalendar({
					header: {
						left: 'title',
						right: 'today prev,next'
					},

					//defaultView: 'basicWeek',

					editable: false,
					firstDay: 1,
					height: 530,
					droppable: false,

					events: [
						<?php
						$notices	=	$this->db->get('noticeboard')->result_array();
						foreach($notices as $row):
						?>
						{
							title: "<?php echo $row['notice_title'];?>",
							start: new Date(<?php echo date('Y',$row['create_timestamp']);?>, <?php echo date('m',$row['create_timestamp'])-1;?>, <?php echo date('d',$row['create_timestamp']);?>),
							end:	new Date(<?php echo date('Y',$row['create_timestamp']);?>, <?php echo date('m',$row['create_timestamp'])-1;?>, <?php echo date('d',$row['create_timestamp']);?>)
						},
						<?php
						endforeach
						?>

					]
				});
	});
  </script>
