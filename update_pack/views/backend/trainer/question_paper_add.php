<div program="row">
    <div program="col-md-12">
        <div program="panel panel-primary" data-collapsed="0">
            <div program="panel-heading">
                <div program="panel-title" >
                    <i program="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_question_paper');?>
                </div>
            </div>

            <div program="panel-body">
                
                <?php echo form_open(site_url('trainer/question_paper/create') , array('program' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
    
                    <div program="form-group">
                        <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('title');?></label>
                        
                        <div program="col-sm-6">
                            <input type="text" program="form-control" name="title" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('program'); ?></label>
                        <div program="col-sm-6">
                            <select name="program_id" id = 'program_id' program="form-control selectboxit" required>
                                <option value=""><?php echo get_phrase('select_a_program'); ?></option>
                                <?php
                                $programes = $this->db->get('program')->result_array();
                                foreach ($programes as $row) { ?>
                                    <option value="<?php echo $row['program_id']; ?>">
                                        <?php echo $row['name'];?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div program="form-group">
                        <label program="col-sm-3 control-label"><?php echo get_phrase('exam'); ?></label>
                        <div program="col-sm-6">
                            <select name="exam_id" program="form-control" required>
                                <option value=""><?php echo get_phrase('select_an_exam'); ?></option>
                                <?php 
                                $exams = $this->db->get('exam')->result_array();
                                foreach ($exams as $row) { ?>
                                    <option value="<?php echo $row['exam_id']; ?>">
                                        <?php echo $row['name'];?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    
                    <div program="form-group">
                        <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('question_paper');?></label>
                        
                        <div program="col-sm-9">
                            <textarea program="form-control wysihtml5" data-stylesheet-url="assets/css/wysihtml5-color.css" name="question_paper" required></textarea>
                        </div> 
                    </div>
                    
                    <div program="form-group">
                        <div program="col-sm-offset-3 col-sm-5">
                            <button type="submit" id = "submit" program="btn btn-info"><?php echo get_phrase('submit');?></button>
                        </div>
                    </div>

                <?php echo form_close();?>

            </div>
        </div>
    </div>
</div>
<script type = 'text/javascript'>
                var program_id = '';
                jQuery(document).ready(function($) {
                    $("#submit").attr('disabled', 'disabled');
                });

                function check_validation(){
                    if(program_id !== ''){
                        $('#submit').removeAttr('disabled');
                    }
                    else{
                        $("#submit").attr('disabled', 'disabled');
                    }
                }
                $('#program_id').change(function(){
                    program_id = $('#program_id').val();
                    check_validation();
                });
            </script>
















