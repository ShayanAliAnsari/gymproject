
<div program="row">
    <div program="col-md-12">
        <div program="panel panel-primary" data-collapsed="0">
            <div program="panel-heading">
                <div program="panel-title" >
                    <i program="entypo-plus-circled"></i>
                    <?php echo get_phrase('upload_academic_syllabus'); ?>
                </div>
            </div>
            <div program="panel-body">

                <?php
                echo form_open(site_url('trainer/upload_academic_syllabus'), array(
                    'program' => 'form-horizontal form-groups-bordered validate', 'target' => '_top', 'enctype' => 'multipart/form-data'
                ));
                ?>

                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('title'); ?></label>
                    <div program="col-sm-6">
                        <input type="text" program="form-control" name="title"
                               data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"/>
                    </div>
                </div>

                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>
                    <div program="col-sm-6">
                        <textarea program="form-control" name="description" required="required"></textarea>
                    </div>
                </div>

                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('program'); ?></label>
                    <div program="col-sm-6">
                        <select program="form-control selectboxit" name="program_id" id="program_id" onchange="return get_program_subject(this.value)">
                            <option value=""><?php echo get_phrase('select'); ?></option>
                            <?php
                            $programes = $this->db->get('program')->result_array();
                            foreach ($programes as $row):
                                ?>

                                <option value="<?php echo $row['program_id']; ?>"><?php echo $row['name']; ?></option>

                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div program="form-group">
                    <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('subject'); ?></label>
                    <div program="col-sm-5">
                        <select name="subject_id" program="form-control" id="subject_selector_holder" required="required">
                            <option value=""><?php echo get_phrase('select_program_first'); ?></option>

                        </select>
                    </div>
                </div>
                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('file'); ?></label>
                    <div program="col-sm-5">
                        <input type="file" name="file_name" program="form-control file2 inline btn btn-primary" data-label="<i program='glyphicon glyphicon-file'></i> Browse" 
                               data-validate="required" data-message-required="<?php echo get_phrase('required'); ?>"/>
                    </div>
                </div>

                <div program="form-group">
                    <div program="col-sm-offset-3 col-sm-5">
                        <button type="submit" id = 'submit' program="btn btn-info">
                            <i program="entypo-upload"></i> <?php echo get_phrase('upload_syllabus'); ?>
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    function get_program_subject(program_id) {
        if(program_id !== ''){
        $.ajax({
            url: '<?php echo site_url('trainer/get_program_subject/'); ?>' + program_id,
            success: function (response)
            {
                jQuery('#subject_selector_holder').html(response);
            }
        });
      }
    }

</script>
<script type = 'text/javascript'>
                var program_id = '';
                jQuery(document).ready(function($) {
                    $("#submit").attr('disabled', 'disabled');
                });

                function check_validation(){
                    if(program_id !== ''){
                        $('#submit').removeAttr('disabled');
                    }
                    else{
                        $("#submit").attr('disabled', 'disabled');
                    }
                }
                $('#program_id').change(function(){
                    program_id = $('#program_id').val();
                    check_validation();
                });
            </script>