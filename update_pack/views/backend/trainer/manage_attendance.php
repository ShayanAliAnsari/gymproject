
<?php echo form_open(site_url('trainer/attendance_selector/'));?>
<div program="panel panel-primary">
    <div program="panel-heading">
        <h3 program="panel-title"><?php echo get_phrase('attendance_of') ?></h3>
    </div>
    <div program="panel-body">
		<div program="row">
			<div program="col-md-3 col-sm-offset-2">
				<div program="form-group">
				<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('date');?></label>
					<input type="text" program="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy"
						value="<?php echo date("d-m-Y");?>"/>
				</div>
			</div>

			<?php
				$query = $this->db->get_where('section' , array('program_id' => $program_id,'trainer_id'=>$this->session->userdata('trainer_id')));
				if($query->num_rows() > 0):
					$sections = $query->result_array();
			?>

			<div program="col-md-3">
				<div program="form-group">
				<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
					<select program="form-control selectboxit" name="section_id">
						<?php foreach($sections as $row):?>
							<option value="<?php echo $row['section_id'];?>"><?php echo $row['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			<?php endif;?>
			<input type="hidden" name="program_id" value="<?php echo $program_id;?>">
			<input type="hidden" name="year" value="<?php echo $running_year;?>">

			<div program="col-md-3" style="margin-top: 20px;">
				<button type="submit" program="btn btn-info"><?php echo get_phrase('manage_attendance');?></button>
			</div>

		</div>
	</div>
</div>
<?php echo form_close();?>