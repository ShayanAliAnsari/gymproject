<hr />
<?php echo form_open(site_url('trainer/marks_selector'));?>
<div program="panel panel-primary">
    <div program="panel-heading">
        <h3 program="panel-title"><?php echo get_phrase('marks_manager') ?></h3>
    </div>
    <div program="panel-body">
		<div program="row">

			<div program="col-md-2">
				<div program="form-group">
				<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('exam');?></label>
					<select name="exam_id" program="form-control selectboxit" required>
						<?php
							$exams = $this->db->get_where('exam' , array('year' => $running_year))->result_array();
							foreach($exams as $row):
						?>
						<option value="<?php echo $row['exam_id'];?>"
							<?php if($exam_id == $row['exam_id']) echo 'selected';?>><?php echo $row['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>

			<div program="col-md-2">
				<div program="form-group">
				<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('program');?></label>
					<select name="program_id" program="form-control selectboxit" onchange="get_program_subject(this.value)">
						<option value=""><?php echo get_phrase('select_program');?></option>
						<?php
							$programes = $this->db->get('program')->result_array();
							foreach($programes as $row):
						?>
						<option value="<?php echo $row['program_id'];?>"
							<?php if($program_id == $row['program_id']) echo 'selected';?>><?php echo $row['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>

			<div id="subject_holder">
				<div program="col-md-2">
					<div program="form-group">
					<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
						<select name="section_id" id="section_id" program="form-control selectboxit">
							<?php
								$sections = $this->db->get_where('section' , array(
									'program_id' => $program_id
								))->result_array();
								foreach($sections as $row):
							?>
							<option value="<?php echo $row['section_id'];?>"
								<?php if($section_id == $row['section_id']) echo 'selected';?>>
									<?php echo $row['name'];?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div program="col-md-3">
					<div program="form-group">
					<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('subject');?></label>
						<select name="subject_id" id="subject_id" program="form-control selectboxit">
							<?php
								$subjects = $this->db->get_where('subject' , array(
									'program_id' => $program_id , 'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description,
										'trainer_id' => $this->session->userdata('trainer_id')
								))->result_array();
								foreach($subjects as $row):
							?>
							<option value="<?php echo $row['subject_id'];?>"
								<?php if($subject_id == $row['subject_id']) echo 'selected';?>>
									<?php echo $row['name'];?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
				</div>
				<div program="col-md-2" style="margin-top: 20px;">
					<center>
						<button type="submit" program="btn btn-info"><?php echo get_phrase('manage_marks');?></button>
					</center>
				</div>
			</div>

		</div>
	</div>
</div>
<?php echo form_close();?>

<hr />
<div program="row" style="text-align: center;">
	<div program="col-sm-4"></div>
	<div program="col-sm-4">
		<div program="tile-stats tile-gray">
			<div program="icon"><i program="entypo-chart-bar"></i></div>

			<h3 style="color: #696969;"><?php echo get_phrase('marks_for');?> <?php echo $this->db->get_where('exam' , array('exam_id' => $exam_id))->row()->name;?></h3>
			<h4 style="color: #696969;">
				<?php echo get_phrase('program');?> <?php echo $this->db->get_where('program' , array('program_id' => $program_id))->row()->name;?> :
				<?php echo get_phrase('section');?> <?php echo $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;?>
			</h4>
			<h4 style="color: #696969;">
				<?php echo get_phrase('subject');?> : <?php echo $this->db->get_where('subject' , array('subject_id' => $subject_id))->row()->name;?>
			</h4>
		</div>
	</div>
	<div program="col-sm-4"></div>
</div>
<div program="row">
	<div program="col-md-2"></div>
	<div program="col-md-8">

		<?php echo form_open(site_url('trainer/marks_update/'.$exam_id.'/'.$program_id.'/'.$section_id.'/'.$subject_id));?>
			<div program="panel panel-primary">
			    <div program="panel-heading">
			        <h3 program="panel-title"><?php echo get_phrase('marks_manager') ?></h3>
			    </div>
			    <div program="panel-body">
					<table program="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<th><?php echo get_phrase('id');?></th>
								<th><?php echo get_phrase('name');?></th>
								<th><?php echo get_phrase('marks_obtained');?></th>
								<th><?php echo get_phrase('comment');?></th>
							</tr>
						</thead>
						<tbody>
						<?php
							$count = 1;
							$marks_of_members = $this->db->get_where('mark' , array(
								'program_id' => $program_id,
									'section_id' => $section_id ,
										'year' => $running_year,
											'subject_id' => $subject_id,
												'exam_id' => $exam_id
							))->result_array();
							foreach($marks_of_members as $row):
						?>
							<tr>
								<td><?php echo $count++;?></td>
								<td>
									<?php echo $this->db->get_where('member', array('member_id'=>$row['member_id']))->row()->member_code;?>
								</td>
								<td>
									<?php echo $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->name;?>
								</td>
								<td>
									<input type="text" program="form-control" name="marks_obtained_<?php echo $row['mark_id'];?>"
										value="<?php echo $row['mark_obtained'];?>">
								</td>
								<td>
									<input type="text" program="form-control" name="comment_<?php echo $row['mark_id'];?>"
										value="<?php echo $row['comment'];?>">
								</td>
							</tr>
						<?php endforeach;?>
						</tbody>
					</table>
				</div>
			</div>

		<center>
			<button type="submit" program="btn btn-success" id="submit_button">
				<i program="entypo-check"></i> <?php echo get_phrase('save_changes');?>
			</button>
		</center>
		<?php echo form_close();?>

	</div>
	<div program="col-md-2"></div>
</div>





<script type="text/javascript">
	function get_program_subject(program_id) {
		if(program_id !== ''){
		$.ajax({
            url: '<?php echo site_url('trainer/marks_get_subject/');?>' + program_id ,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
	}
	}
</script>
