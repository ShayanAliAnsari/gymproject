<button onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_study_material_add');?>');" 
    program="btn btn-primary pull-right">
        <?php echo get_phrase('add_study_material'); ?>
</button>
<div style="clear:both;"></div>
<br>
<table program="table table-bordered" id="table_export">
    <thead>
        <tr>
            <th>#</th>
            <th><?php echo get_phrase('date');?></th>
            <th><?php echo get_phrase('title');?></th>
            <th><?php echo get_phrase('description');?></th>
            <th><?php echo get_phrase('program');?></th>
            <th><?php echo get_phrase('subject');?></th>
            <th><?php echo get_phrase('download');?></th>
            <th><?php echo get_phrase('options');?></th>
        </tr>
    </thead>

    <tbody>
        <?php
        $count = 1;
        foreach ($study_material_info as $row) { ?>   
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo date("d M, Y", $row['timestamp']); ?></td>
                <td><?php echo $row['title']?></td>
                <td><?php echo $row['description']?></td>
                <td>
                    <?php $name = $this->db->get_where('program' , array('program_id' => $row['program_id'] ))->row()->name;
                        echo $name;?>
                </td>
                <td>
                    <?php $name = $this->db->get_where('subject' , array('subject_id' => $row['subject_id'] ))->row()->name;
                        echo $name;?>
                </td>
                <td>
                    <a href="<?php echo site_url().'uploads/document/'.$row['file_name']; ?>" program="btn btn-blue btn-icon icon-left">
                        <i program="entypo-download"></i>
                        <?php echo get_phrase('download');?>
                    </a>
                </td>
                <td>
                    <a  onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_study_material_edit/'.$row['document_id']);?>');" 
                        program="btn btn-default btn-sm btn-icon icon-left">
                            <i program="entypo-pencil"></i>
                            <?php echo get_phrase('edit');?>
                    </a>
                    <a href="<?php echo site_url('trainer/study_material/delete/'.$row['document_id']);?>" 
                        class="btn btn-danger btn-sm btn-icon icon-left" onclick="return confirm('Are you sure to delete?');">
                            <i program="entypo-cancel"></i>
                            <?php echo get_phrase('delete');?>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>