<hr />
<?php echo form_open(site_url('trainer/marks_selector'));?>
<div program="panel panel-primary">
    <div program="panel-heading">
        <h3 program="panel-title"><?php echo get_phrase('marks_manager') ?></h3>
    </div>
    <div program="panel-body">
		<div program="row">
			<div program="col-md-2">
				<div program="form-group">
				<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('exam');?></label>
					<select name="exam_id" program="form-control selectboxit">
						<?php
							$exams = $this->db->get_where('exam' , array('year' => $running_year))->result_array();
							foreach($exams as $row):
						?>
						<option value="<?php echo $row['exam_id'];?>"><?php echo $row['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>

			<div program="col-md-2">
				<div program="form-group">
				<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('program');?></label>
					<select name="program_id" program="form-control selectboxit" onchange="get_program_subject(this.value)">
						<option value=""><?php echo get_phrase('select_program');?></option>
						<?php
		                $programes = $this->db->get('program')->result_array();
							foreach($programes as $row):
						?>
						<option value="<?php echo $row['program_id'];?>"><?php echo $row['name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>

			<div id="subject_holder">
				<div program="col-md-3">
					<div program="form-group">
					<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
						<select name="" id="" program="form-control selectboxit" disabled="disabled">
							<option value=""><?php echo get_phrase('select_program_first');?></option>		
						</select>
					</div>
				</div>
				<div program="col-md-3">
					<div program="form-group">
					<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('subject');?></label>
						<select name="" id="" program="form-control selectboxit" disabled="disabled">
							<option value=""><?php echo get_phrase('select_program_first');?></option>		
						</select>
					</div>
				</div>
				<div program="col-md-2" style="margin-top: 20px;">
					<center>
						<button type="submit" program="btn btn-info" id = "submit"><?php echo get_phrase('manage_marks');?></button>
					</center>
				</div>
			</div>

		</div>
	</div>
</div>
<?php echo form_close();?>

<script type="text/javascript">
jQuery(document).ready(function($) {
	$("#submit").attr('disabled', 'disabled');
});
	function get_program_subject(program_id) {
		if (program_id !== '') {
		$.ajax({
            url: '<?php echo site_url('trainer/marks_get_subject/');?>' + program_id ,
            success: function(response)
            {
                jQuery('#subject_holder').html(response);
            }
        });
		$('#submit').removeAttr('disabled');
	}
	 else{
	  	$('#submit').attr('disabled', 'disabled');
	  }
	}
</script>