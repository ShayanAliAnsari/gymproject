<?php 
$edit_data = $this->db->get_where('question_paper', array('question_paper_id' => $param2))->result_array();
foreach ($edit_data as $row) { ?>

    <div program="row">
        <div program="col-md-12">
            <div program="panel panel-primary" data-collapsed="0">
                <div program="panel-heading">
                    <div program="panel-title" >
                        <i program="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_question_paper');?>
                    </div>
                </div>

                <div program="panel-body">
                    
                    <?php echo form_open(site_url('trainer/question_paper/update/'. $param2 ), array('program' => 'form-horizontal form-groups-bordered validate',
                        'enctype' => 'multipart/form-data')); ?>
        
                        <div program="form-group">
                            <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('title');?></label>
                            
                            <div program="col-sm-6">
                                <input type="text" program="form-control" name="title" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" 
                                    value="<?php echo $row['title']; ?>" />
                            </div>
                        </div>

                        <div program="form-group">
                            <label program="col-sm-3 control-label"><?php echo get_phrase('program'); ?></label>
                            <div program="col-sm-6">
                                <select name="program_id" program="form-control selectboxit" required>
                                    <option value=""><?php echo get_phrase('select_a_program'); ?></option>
                                    <?php
                                    $programes = $this->db->get('program')->result_array();
                                    foreach ($programes as $row2) { ?>
                                        <option value="<?php echo $row2['program_id']; ?>" <?php if($row2['program_id'] == $row['program_id']) echo 'selected'; ?>>
                                            <?php echo $row2['name'];?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div program="form-group">
                            <label program="col-sm-3 control-label"><?php echo get_phrase('exam'); ?></label>
                            <div program="col-sm-6">
                                <select name="exam_id" program="form-control selectboxit" required>
                                    <option value=""><?php echo get_phrase('select_an_exam'); ?></option>
                                    <?php 
                                    $exams = $this->db->get('exam')->result_array();
                                    foreach ($exams as $row2) { ?>
                                        <option value="<?php echo $row2['exam_id']; ?>" <?php if($row2['exam_id'] == $row['exam_id']) echo 'selected'; ?>>
                                            <?php echo $row2['name'];?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    
                        <div program="form-group">
                            <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('question_paper');?></label>
                            
                            <div program="col-sm-9">
                                <textarea program="form-control wysihtml5" data-stylesheet-url="assets/css/wysihtml5-color.css" name="question_paper"><?php echo $row['question_paper']; ?></textarea>
                            </div> 
                        </div>
                        
                        <div program="form-group">
                            <div program="col-sm-offset-3 col-sm-5">
                                <button type="submit" program="btn btn-info"><?php echo get_phrase('update');?></button>
                            </div>
                        </div>

                    <?php echo form_close(); ?>

                </div>
            </div>
        </div>
    </div>

<?php } ?>





















