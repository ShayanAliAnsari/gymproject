<div program="sidebar-menu">
    <header program="logo-env" >
        <!-- logo -->
        <!-- <div program="logo" style="">
            <a href="<?php  echo site_url('login'); ?>">
                <img src="<?php  echo base_url('uploads/logo.png');?>"  style="max-height:30px;"/>
            </a>
        </div> -->

        <!-- logo collapse icon -->
        <div program="sidebar-collapse" style="margin-top: 0px;">
            <a href="#" program="sidebar-collapse-icon" onclick="hide_brand()">
                <i program="entypo-menu"></i>
            </a>
        </div>
        <script type="text/javascript">
            function hide_brand() {
                $('#branding_element').toggle();
            }
        </script>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div program="sidebar-mobile-menu visible-xs">
            <a href="#" program="with-animation">
                <i program="entypo-menu"></i>
            </a>
        </div>
    </header>
    <ul id="main-menu" program="">
        <!-- add program "multiple-expanded" to allow multiple submenus to open -->
        <!-- program "auto-inherit-active-program" will automatically add "active" class For parent elements who are marked already with program "active" -->

        <div style="text-align: -webkit-center;" id="branding_element">
            <img src="<?php echo base_url('uploads/logo.png');?>"  style="max-height:21px;"/>
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px;font-weight: 300;margin-top: 10px;">
                <?php echo $system_name;?>
            </h4>
        </div>


        <!-- DASHBOARD -->
        <li program="<?php if ($page_name == 'dashboard') echo 'active'; ?> " style="border-top:1px solid #232540;">
            <a href="<?php echo site_url($account_type.'/dashboard'); ?>">
                <i program="entypo-gauge"></i>
                <span><?php echo get_phrase('dashboard'); ?></span>
            </a>
        </li>

        <!-- member -->
        <li program="<?php
        if ($page_name == 'member_add' ||
            $page_name == 'member_information' ||
            $page_name == 'member_marksheet')
            echo 'opened active has-sub';
        ?> ">
            <a href="#">
                <i program="fa fa-group"></i>
                <span><?php echo get_phrase('member'); ?></span>
            </a>
            <ul>

                <!-- member INFORMATION -->
                <li program="<?php if ($page_name == 'member_information' || $page_name == 'member_marksheet' || $page_name == 'member_profile') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i program="entypo-dot"></i> <?php echo get_phrase('member_information'); ?></span>
                    </a>
                    <ul>
                        <?php
                        $programes = $this->db->get('program')->result_array();
                        foreach ($programes as $row):
                            ?>
                            <li program="<?php if ($page_name == 'member_information' && $program_id == $row['program_id']) echo 'active'; ?>">
                                <a href="<?php echo site_url($account_type.'/member_information/'.$row['program_id']); ?>">
                                    <span><?php echo get_phrase('program'); ?><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li>

            </ul>
        </li>

        <!-- trainer -->
        <li program="<?php if ($page_name == 'trainer') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/trainer_list'); ?>">
                <i program="entypo-users"></i>
                <span><?php echo get_phrase('trainer'); ?></span>
            </a>
        </li>


        <!-- SUBJECT -->
        <li program="<?php if ($page_name == 'subject') echo 'opened active'; ?> ">
            <a href="#">
                <i program="entypo-docs"></i>
                <span><?php echo get_phrase('subject'); ?></span>
            </a>
            <ul>
                <?php $programes = $this->db->get('program')->result_array();
                foreach ($programes as $row):
                    ?>
                    <li program="<?php if ($page_name == 'subject' && $program_id == $row['program_id']) echo 'active'; ?>">
                        <a href="<?php echo site_url($account_type.'/subject/'.$row['program_id']); ?>">
                            <span><?php echo get_phrase('program'); ?><?php echo $row['name']; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>

        <!-- class  ROUTINE -->
        <li program="<?php if ($page_name == 'program_routine' ||
            $page_name == 'program_routine_print_view')
            echo 'opened active'; ?> ">
            <a href="#">
                <i program="entypo-target"></i>
                <span><?php echo get_phrase('program_routine'); ?></span>
            </a>
            <ul>
                <?php
                $programes = $this->db->get('program')->result_array();
                foreach ($programes as $row):
                    ?>
                    <li program="<?php if ($page_name == 'program_routine' && $program_id == $row['program_id']) echo 'active'; ?>">
                        <a href="<?php echo site_url($account_type.'/program_routine/'.$row['program_id']); ?>">
                            <span><?php echo get_phrase('program'); ?><?php echo $row['name']; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>

        <!-- STUDY MATERIAL -->
        <li program="<?php if ($page_name == 'study_material') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/study_material'); ?>">
                <i program="entypo-book-open"></i>
                <span><?php echo get_phrase('study_material'); ?></span>
            </a>
        </li>

        <!-- ACADEMIC SYLLABUS -->
        <li program="<?php if ($page_name == 'academic_syllabus') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/academic_syllabus'); ?>">
                <i program="entypo-doc"></i>
                <span><?php echo get_phrase('academic_syllabus'); ?></span>
            </a>
        </li>

        <!-- DAILY ATTENDANCE -->
        <li program="<?php if ($page_name == 'manage_attendance' ||
            $page_name == 'manage_attendance_view')
            echo 'opened active'; ?> ">
            <a href="#">
                <i program="entypo-chart-area"></i>
                <span><?php echo get_phrase('daily_attendance'); ?></span>
            </a>
            <ul>
                <?php
                $programes = $this->db->get('program')->result_array();
                foreach ($programes as $row):
                    ?>
                    <li program="<?php if (($page_name == 'manage_attendance' || $page_name == 'manage_attendance_view') && $program_id == $row['program_id']) echo 'active'; ?>">
                        <a href="<?php echo site_url($account_type.'/manage_attendance/'.$row['program_id']); ?>">
                            <span><?php echo get_phrase('program'); ?><?php echo $row['name']; ?></span>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </li>

        <!-- EXAMS -->
        <li program="<?php if ($page_name == 'marks_manage' || $page_name == 'marks_manage_view' || $page_name == 'question_paper') echo 'opened active'; ?> ">
            <a href="#">
                <i program="entypo-graduation-cap"></i>
                <span><?php echo get_phrase('exam'); ?></span>
            </a>
            <ul>
                <li program="<?php if ($page_name == 'marks_manage' || $page_name == 'marks_manage_view') echo 'active'; ?> ">
                    <a href="<?php echo site_url($account_type.'/marks_manage'); ?>">
                        <span><i program="entypo-dot"></i> <?php echo get_phrase('manage_marks'); ?></span>
                    </a>
                </li>
                <li program="<?php if ($page_name == 'question_paper') echo 'active'; ?> ">
                    <a href="<?php echo site_url($account_type.'/question_paper'); ?>">
                        <span><i program="entypo-dot"></i> <?php echo get_phrase('question_paper'); ?></span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- LIBRARY -->
        <li program="<?php if ($page_name == 'book') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/book'); ?>">
                <i program="entypo-book"></i>
                <span><?php echo get_phrase('library'); ?></span>
            </a>
        </li>

        <!-- TRANSPORT -->
        <li program="<?php if ($page_name == 'transport') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/transport'); ?>">
                <i program="entypo-location"></i>
                <span><?php echo get_phrase('transport'); ?></span>
            </a>
        </li>

        <!-- NOTICEBOARD -->
        <li program="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/noticeboard'); ?>">
                <i program="entypo-doc-text-inv"></i>
                <span><?php echo get_phrase('noticeboard'); ?></span>
            </a>
        </li>

        <!-- MESSAGE -->
        <li program="<?php if ($page_name == 'message' || $page_name == 'group_message') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/message'); ?>">
                <i program="entypo-mail"></i>
                <span><?php echo get_phrase('message'); ?></span>
            </a>
        </li>

        <!-- ACCOUNT -->
        <li program="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type.'/manage_profile'); ?>">
                <i program="entypo-lock"></i>
                <span><?php echo get_phrase('account'); ?></span>
            </a>
        </li>

    </ul>

</div>
