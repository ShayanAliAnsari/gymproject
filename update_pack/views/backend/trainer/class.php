<div program="row">
	<div program="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul program="nav nav-tabs bordered">
			<li program="active">
            	<a href="#list" data-toggle="tab"><i program="entypo-menu"></i> 
					<?php echo get_phrase('program_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i program="entypo-plus-circled"></i>
					<?php echo get_phrase('add_program');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
		<div program="tab-content">
            <!----TABLE LISTING STARTS-->
            <div program="tab-pane box active" id="list">
				
                <table program="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div>#</div></th>
                    		<th><div><?php echo get_phrase('program_name');?></div></th>
                    		<th><div><?php echo get_phrase('numeric_name');?></div></th>
                    		<th><div><?php echo get_phrase('trainer');?></div></th>
                    		<th><div><?php echo get_phrase('options');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($programes as $row):?>
                        <tr>
                            <td><?php echo $count++;?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $row['name_numeric'];?></td>
							<td><?php echo $this->crud_model->get_type_name_by_id('trainer',$row['trainer_id']);?></td>
							<td>
                            <div program="btn-group">
                                <button type="button" program="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Action <span program="caret"></span>
                                </button>
                                <ul program="dropdown-menu dropdown-default pull-right" role="menu">
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_edit_program/'.$row['program_id']);?>');">
                                            <i program="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                                    </li>
                                    <li program="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo site_url('trainer/programes/delete/'.$row['program_id']);?>');">
                                            <i program="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                                    </li>
                                </ul>
                            </div>
        					</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<div program="tab-pane box" id="add" style="padding: 5px">
                <div program="box-content">
                	<?php echo form_open('trainer/programes/create' , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                        <div program="padded">
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('name_numeric');?></label>
                                <div program="col-sm-5">
                                    <input type="text" program="form-control" name="name_numeric"/>
                                </div>
                            </div>
                            <div program="form-group">
                                <label program="col-sm-3 control-label"><?php echo get_phrase('trainer');?></label>
                                <div program="col-sm-5">
                                    <select name="trainer_id" program="form-control" style="width:100%;">
                                    	<?php 
										$trainers = $this->db->get('trainer')->result_array();
										foreach($trainers as $row):
										?>
                                    		<option value="<?php echo $row['trainer_id'];?>"><?php echo $row['name'];?></option>
                                        <?php
										endforeach;
										?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div program="form-group">
                              <div program="col-sm-offset-3 col-sm-5">
                                  <button type="submit" program="btn btn-info"><?php echo get_phrase('add_program');?></button>
                              </div>
							</div>
                    </form>                
                </div>                
			</div>
			<!----CREATION FORM ENDS-->
		</div>
	</div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>