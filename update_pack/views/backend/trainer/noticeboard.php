<div program="row">
    <div program="col-md-12">

        <!------CONTROL TABS START------>
        <ul program="nav nav-tabs bordered">
            <li program="active">
                <a href="#list" data-toggle="tab"><i program="entypo-menu"></i> 
                    <?php echo get_phrase('noticeboard_list'); ?>
                </a></li>
        </ul>
        <!------CONTROL TABS END------>


        <div program="tab-content">
            <!----TABLE LISTING STARTS-->
            <div program="tab-pane box active" id="list">
                <table cellpadding="0" cellspacing="0" border="0" program="table table-bordered datatable" id="table_export">
                    <thead>
                        <tr>
                            <th><div>#</div></th>
                    <th><div><?php echo get_phrase('title'); ?></div></th>
                    <th><div><?php echo get_phrase('notice'); ?></div></th>
                    <th><div><?php echo get_phrase('date'); ?></div></th>
                    <th><div></div></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $count = 1;
                        foreach ($notices as $row):
                            ?>
                            <tr>
                                <td><?php echo $count++; ?></td>
                                <td><?php echo $row['notice_title']; ?></td>
                                <td program="span5"><?php echo $row['notice']; ?></td>
                                <td><?php echo date('d M,Y', $row['create_timestamp']); ?></td>
                                <td>
                                    <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_view_notice/'.$row['notice_id']); ?>');"
                                       program="btn btn-default">
                                        <?php echo get_phrase('view_notice'); ?>
                                    </a>
                                </td>

                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <!----TABLE LISTING ENDS-->




        </div>
    </div>
</div>