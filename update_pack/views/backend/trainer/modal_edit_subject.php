<?php 
$edit_data		=	$this->db->get_where('subject' , array('subject_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div program="row">
	<div program="col-md-12">
		<div program="panel panel-primary" data-collapsed="0">
        	<div program="panel-heading">
            	<div program="panel-title" >
            		<i program="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_subject');?>
            	</div>
            </div>
			<div program="panel-body">
                <?php echo form_open(site_url('trainer/subject/do_update/'.$row['subject_id']) , array('program' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                    <div program="col-sm-5 controls">
                        <input type="text" program="form-control" name="name" value="<?php echo $row['name'];?>" required/>
                    </div>
                </div>
                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>
                    <div program="col-sm-5 controls">
                        <select name="program_id" program="form-control" required>
                        <option value=""><?php echo get_phrase('select_program'); ?></option>
                            <?php 
                            $programes = $this->db->get('program')->result_array();
                            foreach($programes as $row2):
                            ?>
                                <option value="<?php echo $row2['program_id'];?>"
                                    <?php if($row['program_id'] == $row2['program_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('trainer');?></label>
                    <div program="col-sm-5 controls">
                        <select name="trainer_id" program="form-control">
                            <?php 
                            $trainers = $this->db->get('trainer')->result_array();
                            foreach($trainers as $row2):
                            ?>
                                <option value="<?php echo $row2['trainer_id'];?>"
                                    <?php if($row['trainer_id'] == $row2['trainer_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div program="form-group">
                    <div program="col-sm-offset-3 col-sm-5">
                        <button type="submit" program="btn btn-info"><?php echo get_phrase('edit_subject');?></button>
                    </div>
                 </div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>



