<?php
$program_info = $this->db->get('program')->result_array();
$single_study_material_info = $this->db->get_where('document', array('document_id' => $param2))->result_array();
foreach ($single_study_material_info as $row) {
    ?>
    <div program="row">
        <div program="col-md-12">

            <div program="panel panel-primary" data-collapsed="0">

                <div program="panel-heading">
                    <div program="panel-title">
                        <?php echo get_phrase('edit_study_material'); ?>
                    </div>
                </div>

                <div program="panel-body">

                    <form role="form" program="form-horizontal form-groups-bordered" action="<?php echo site_url('trainer/study_material/update/'.$row['document_id']); ?>" method="post" enctype="multipart/form-data">

                        <div program="form-group">
                            <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                            <div program="col-sm-5">
                                <input type="text" name="timestamp" program="form-control datepicker" data-format="D, dd MM yyyy"
                                       placeholder="date here" value="<?php echo date("d M, Y", $row['timestamp']); ?>">
                            </div>
                        </div>

                        <div program="form-group">
                            <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('title'); ?></label>

                            <div program="col-sm-5">
                                <input type="text" name="title" program="form-control" id="field-1" value="<?php echo $row['title']; ?>">
                            </div>
                        </div>

                        <div program="form-group">
                            <label for="field-ta" program="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>

                            <div program="col-sm-9">
                                <textarea name="description" program="form-control wysihtml5" data-stylesheet-url="<?php echo base_url(); ?>assets/css/wysihtml5-color.css"
                                          id="field-ta"><?php echo $row['description']; ?></textarea>
                            </div>
                        </div>

                        <div program="form-group">
                            <label for="field-ta" program="col-sm-3 control-label"><?php echo get_phrase('program'); ?></label>

                            <div program="col-sm-5">
                                <select name="program_id" program="form-control selectboxit" id="program_id" onchange="return get_program_subject(this.value)">
                                    <option value=""><?php echo get_phrase('select_program'); ?></option>
                                    <?php foreach ($program_info as $row2) { ?>
                                        <option value="<?php echo $row2['program_id']; ?>" <?php if ($row['program_id'] == $row2['program_id']) echo 'selected'; ?>>
                                            <?php echo $row2['name']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div program="form-group">
                            <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('subject'); ?></label>
                            <div program="col-sm-5">
                                <select name="subject_id" program="form-control" id="subject_selector_holder">
                                   <?php
                                   $subject = $this->db->get_where('subject',array('program_id'=>$row['program_id']))->result_array();
                                   foreach ($subject as $row2){
                                   ?>
                                    <option value="<?php echo $row2['subject_id']; ?>" <?php if ($row['subject_id'] == $row2['subject_id']) echo 'selected'; ?>>
                                            <?php echo $row2['name']; ?>
                                        </option>
                                   <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div program="col-sm-3 control-label col-sm-offset-1">
                            <button type="submit" program="btn btn-success"><?php echo get_phrase('update'); ?></button>
                        </div>
                    </form>

                </div>

            </div>

        </div>
    </div>
<?php } ?>

<script type="text/javascript">

    function get_program_subject(program_id) {

        $.ajax({
            url: '<?php echo site_url('trainer/get_program_subject/'); ?>' + program_id,
            success: function (response)
            {
                jQuery('#subject_selector_holder').html(response);
            }
        });

    }

</script>
