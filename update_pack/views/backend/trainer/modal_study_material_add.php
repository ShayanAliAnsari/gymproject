<?php $program_info = $this->db->get('program')->result_array(); ?>
<div program="row">
    <div program="col-md-12">

        <div program="panel panel-primary" data-collapsed="0">

            <div program="panel-heading">
                <div program="panel-title">
                    <?php echo get_phrase('add_study_material'); ?>
                </div>
            </div>

            <div program="panel-body">

                <?php echo form_open(site_url('trainer/study_material/create'), array('program' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div program="form-group">
                    <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                    <div program="col-sm-5">
                        <input type="text" name="timestamp" program="form-control datepicker" data-format="D, dd MM yyyy"
                               placeholder="<?php echo get_phrase('select_date'); ?>" required>
                    </div>
                </div>

                <div program="form-group">
                    <label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('title'); ?></label>

                    <div program="col-sm-5">
                        <input type="text" name="title" program="form-control" id="field-1" required>
                    </div>
                </div>

                <div program="form-group">
                    <label for="field-ta" program="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>

                    <div program="col-sm-9">
                        <textarea name="description" program="form-control wysihtml5" id="field-ta" data-stylesheet-url="assets/css/wysihtml5-color.css" required></textarea>
                    </div>
                </div>

                <div program="form-group">
                    <label for="field-ta" program="col-sm-3 control-label"><?php echo get_phrase('program'); ?></label>

                    <div program="col-sm-5">
                        <select name="program_id" program="form-control selectboxit" id="program_id" onchange="return get_program_subject(this.value)" required="">
                        <option value=""><?php echo get_phrase('select_program'); ?></option>
                            <?php foreach ($program_info as $row) { ?>
                                <option value="<?php echo $row['program_id']; ?>"><?php echo $row['name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div program="form-group">
                    <label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('subject'); ?></label>
                    <div program="col-sm-5">
                        <select name="subject_id" program="form-control" id="subject_selector_holder" required="required">
                            <option value="" disabled="true"><?php echo get_phrase('select_program_first'); ?></option>
                        </select>
                    </div>
                </div>

                <div program="form-group">
                    <label program="col-sm-3 control-label"><?php echo get_phrase('file'); ?></label>

                    <div program="col-sm-5">

                        <input type="file" name="file_name" program="form-control file2 inline btn btn-primary" data-label="<i program='glyphicon glyphicon-file'></i> Browse" />

                    </div>
                </div>

                <div program="form-group">
                    <label for="field-ta" program="col-sm-3 control-label"><?php echo get_phrase('file_type'); ?></label>

                    <div program="col-sm-5">
                        <select name="file_type" program="form-control selectboxit">
                            <option value=""><?php echo get_phrase('select_file_type'); ?></option>
                            <option value="image"><?php echo get_phrase('image'); ?></option>
                            <option value="doc"><?php echo get_phrase('doc'); ?></option>
                            <option value="pdf"><?php echo get_phrase('pdf'); ?></option>
                            <option value="excel"><?php echo get_phrase('excel'); ?></option>
                            <option value="other"><?php echo get_phrase('other'); ?></option>
                        </select>
                    </div>
                </div>

                <div program="col-sm-3 control-label col-sm-offset-2">
                    <button type="submit" id = "submit" program="btn btn-success"><?php echo get_phrase('upload'); ?></button>
                </div>
                </form>

            </div>

        </div>

    </div>
</div>

<script type="text/javascript">

    function get_program_subject(program_id) {
        if (program_id !== '') {

        $.ajax({
            url: '<?php echo site_url('trainer/get_program_subject/'); ?>' + program_id,
            success: function (response)
            {
                jQuery('#subject_selector_holder').html(response);
            }
        });
}
    }

</script>
<script type = 'text/javascript'>
                var program_id = '';
                jQuery(document).ready(function($) {
                    $("#submit").attr('disabled', 'disabled');
                });

                function check_validation(){
                    if(program_id !== ''){
                        $('#submit').removeAttr('disabled');
                    }
                    else{
                        $("#submit").attr('disabled', 'disabled');
                    }
                }
                $('#program_id').change(function(){
                    program_id = $('#program_id').val();
                    check_validation();
                });
            </script>