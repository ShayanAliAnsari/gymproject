<div program="col-md-2">
	<div program="form-group">
	<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
		<select name="section_id" id="section_id" program="form-control selectboxit">
			<?php
				$sections = $this->db->get_where('section' , array(
					'program_id' => $program_id
				))->result_array();
				foreach($sections as $row):
			?>
			<option value="<?php echo $row['section_id'];?>"><?php echo $row['name'];?></option>
			<?php endforeach;?>
		</select>
	</div>
</div>

<div program="col-md-3">
	<div program="form-group">
	<label program="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('subject');?></label>
		<select name="subject_id" id="subject_id" program="form-control selectboxit">
			<?php
				$subjects = $this->db->get_where('subject' , array(
					'program_id' => $program_id , 'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description,
						'trainer_id' => $this->session->userdata('trainer_id')
				))->result_array();
				foreach($subjects as $row):
			?>
			<option value="<?php echo $row['subject_id'];?>"><?php echo $row['name'];?></option>
			<?php endforeach;?>
		</select>
	</div>
</div>

<div program="col-md-2" style="margin-top: 20px;">
	<center>
		<button type="submit" program="btn btn-info"><?php echo get_phrase('manage_marks');?></button>
	</center>
</div>


<script type="text/javascript">
	$(document).ready(function() {
        if($.isFunction($.fn.selectBoxIt))
		{
			$("select.selectboxit").each(function(i, el)
			{
				var $this = $(el),
					opts = {
						showFirstOption: attrDefault($this, 'first-option', true),
						'native': attrDefault($this, 'native', false),
						defaultText: attrDefault($this, 'text', ''),
					};

				$this.addprogram('visible');
				$this.selectBoxIt(opts);
			});
		}
    });

</script>
