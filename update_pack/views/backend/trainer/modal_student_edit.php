<?php 
$edit_data		=	$this->db->get_where('enroll' , array(
	'member_id' => $param2 , 'year' => $this->db->get_where('settings' , array('type' => 'running_year'))->row()->description
))->result_array();
foreach ($edit_data as $row):
?>
<div program="row">
	<div program="col-md-12">
		<div program="panel panel-primary" data-collapsed="0">
        	<div program="panel-heading">
            	<div program="panel-title" >
            		<i program="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_member');?>
            	</div>
            </div>
			<div program="panel-body">
				
                <?php echo form_open(site_url('trainer/member/do_update/'.$row['member_id'].'/'.$row['program_id']) , array('program' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                
                	
	
					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('photo');?></label>
                        
						<div program="col-sm-5">
							<div program="fileinput fileinput-new" data-provides="fileinput">
								<div program="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="<?php echo $this->crud_model->get_image_url('member' , $row['member_id']);?>" alt="...">
								</div>
								<div program="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span program="btn btn-white btn-file">
										<span program="fileinput-new">Select image</span>
										<span program="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" program="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
					</div>
	
					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        
						<div program="col-sm-5">
							<input type="text" program="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" 
								value="<?php echo $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->name; ?>">
						</div>
					</div>

					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('program');?></label>
                        
						<div program="col-sm-5">
							<input type="text" program="form-control" name="program" disabled
								value="<?php echo $this->db->get_where('program' , array('program_id' => $row['program_id']))->row()->name; ?>">
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('section');?></label>
                        
						<div program="col-sm-5">
							<select name="section_id" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select_section');?></option>
                              <?php
                              	$sections = $this->db->get_where('section' , array('program_id' => $row['program_id']))->result_array();
                              	foreach($sections as $row2):
                              ?>
                              <option value="<?php echo $row2['section_id'];?>"
                              	<?php if($row['section_id'] == $row2['section_id']) echo 'selected';?>><?php echo $row2['name'];?></option>
                          <?php endforeach;?>
                          </select>
						</div> 
					</div>

					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('id');?></label>
                        
						<div program="col-sm-5">
							<input type="text" program="form-control" name="roll"
								value="<?php echo $this->db->get_where('member',array('member_id'=>$row['member_id']))->row()->member_code;?><">
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('parent');?></label>
                        
						<div program="col-sm-5">
							<select name="parent_id" program="form-control select2" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                              <option value=""><?php echo get_phrase('select');?></option>
                              <?php 
									$parents = $this->db->get('parent')->result_array();
									$parent_id = $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->parent_id;
									foreach($parents as $row3):
										?>
                                		<option value="<?php echo $row3['parent_id'];?>"
                                        	<?php if($row3['parent_id'] == $parent_id)echo 'selected';?>>
													<?php echo $row3['name'];?>
                                                </option>
	                                <?php
									endforeach;
								  ?>
                          </select>
						</div> 
					</div>

					
					
					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>
                        
						<div program="col-sm-5">
							<input type="text" program="form-control datepicker" name="birthday" 
								value="<?php echo $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->birthday; ?>" 
									data-start-view="2">
						</div> 
					</div>
					
					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('gender');?></label>
                        
						<div program="col-sm-5">
							<select name="sex" program="form-control selectboxit">
							<?php
								$gender = $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->sex;
							?>
                              <option value=""><?php echo get_phrase('select');?></option>
                              <option value="male" <?php if($gender == 'male')echo 'selected';?>><?php echo get_phrase('male');?></option>
                              <option value="female"<?php if($gender == 'female')echo 'selected';?>><?php echo get_phrase('female');?></option>
                          </select>
						</div> 
					</div>
					
					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('address');?></label>
                        
						<div program="col-sm-5">
							<input type="text" program="form-control" name="address" 
								value="<?php echo $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->address; ?>" >
						</div> 
					</div>
					
					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('phone');?></label>
                        
						<div program="col-sm-5">
							<input type="text" program="form-control" name="phone" 
								value="<?php echo $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->phone; ?>" >
						</div> 
					</div>
                    
					<div program="form-group">
						<label for="field-1" program="col-sm-3 control-label"><?php echo get_phrase('email').'/'.get_phrase('username');?></label>
						<div program="col-sm-5">
							<input type="text" program="form-control" name="email" 
								value="<?php echo $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->email; ?>">
						</div>
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('dormitory');?></label>
                        
						<div program="col-sm-5">
							<select name="dormitory_id" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
	                              <?php
	                              	$dorm_id = $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->dormitory_id;
	                              	$dormitories = $this->db->get('dormitory')->result_array();
	                              	foreach($dormitories as $row2):
	                              ?>
                              		<option value="<?php echo $row2['dormitory_id'];?>"
                              			<?php if($dorm_id == $row2['dormitory_id']) echo 'selected';?>><?php echo $row2['name'];?></option>
                          		<?php endforeach;?>
                          </select>
						</div> 
					</div>

					<div program="form-group">
						<label for="field-2" program="col-sm-3 control-label"><?php echo get_phrase('transport_route');?></label>
                        
						<div program="col-sm-5">
							<select name="transport_id" program="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
	                              <?php
	                              	$trans_id = $this->db->get_where('member' , array('member_id' => $row['member_id']))->row()->transport_id; 
	                              	$transports = $this->db->get('transport')->result_array();
	                              	foreach($transports as $row2):
	                              ?>
                              		<option value="<?php echo $row2['transport_id'];?>"
                              			<?php if($trans_id == $row2['transport_id']) echo 'selected';?>><?php echo $row2['route_name'];?></option>
                          		<?php endforeach;?>
                          </select>
						</div> 
					</div>
                    
                    <div program="form-group">
						<div program="col-sm-offset-3 col-sm-5">
							<button type="submit" program="btn btn-info"><?php echo get_phrase('edit_member');?></button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>

