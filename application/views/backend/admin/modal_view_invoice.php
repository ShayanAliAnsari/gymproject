

<?php
$edit_data = $this->db->get_where('invoice', array('invoice_id' => $param2))->result_array();
foreach ($edit_data as $row):
?>
<center>
    <a onClick="PrintElem('#invoice_print')" class="btn btn-default btn-icon icon-left hidden-print pull-right">
        Print Invoice
        <i class="entypo-print"></i>
    </a>
</center>

    <br><br>

    <div id="invoice_print">
    <!-- <center><h1><b> HAYAT-UL-ISLAM</b></h1></center> -->
        <table align="center"  class="table table-bordered" width="80%" border="0" style="border-collapse:collapse;">
            <tr><td>
           <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <tr>
                <td>
              
                    <h5 align="right" style="padding-right:20 "><?php echo get_phrase('creation_date'); ?> : <?php echo date('d, M,Y', $row['creation_timestamp']);?></h5>
                   <!--  <h5> <?php echo get_phrase('Account no : 9875564442763');  ?></h5> -->
                    <h2 align="center" style="border-collapse:collapse;" ><strong> Hayat-ul-Islam Girls College</strong> </h2>
                    <center><img src="\management\assets\images\hayat.png" width="50" height="50" ></center>
                    <h5 style="padding-left: 50"><?php echo get_phrase('title'); ?> : <?php echo $row['title'];?></h5>
                    <h5 style="padding-left: 50"><?php echo get_phrase('description'); ?> : <?php echo $row['description'];?></h5>


 <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <tr>
               <td ><center><h4><strong>member Name</strong> </h4></center></td>
                <td ><center><h4><strong>Father Name</strong> </h4></center></td>
            </tr>

            <tr>
                <td>
                  <h4 align="center">   <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?> </h4>
                     
                    <!-- <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>             -->
                </td>
                <td>  <h4 align="center">  <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->father_name; ?></td> </h4>
            </tr>



                 
                
                  <h4 align="right" style="padding-right:20 "><?php echo get_phrase('payable_due_date'); ?> 
                  
                  <?php
                    $dt = date('d M,Y', $row['creation_timestamp']);

                    $month = date("m", $row['creation_timestamp']);
                    $month++;
                    $year = date("Y" , $row['creation_timestamp']);

                    echo  date("Y-m-05", mktime(0,0,0,$month, $dt)); 
                           ?>
                  
                </td>
                
            </tr>
            <!-- <tr>
            <td ><?php echo get_phrase('member_name'); ?></td>
          <td>
          
            <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?><br>
            </td>
            </tr> -->
            <!-- <tr>
            <td align="" width="80%"><?php echo get_phrase('father_name'); ?> :</td>
            <td> <?php  echo get_phrase('') . ' ' . $this->db->get_where('member', array('member_id' => $member_id))->row()->father_name;  ?></td>
            </tr> -->
        </table>
        <hr>
        <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <tr>
                <td ><center><h5><strong>Payment to</strong> </h5></center></td>
                <td ><center><h5><strong>Bill to</strong> </h5></center></td>
            </tr>

            <tr>
                <td align="center">
                    <?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?><br>
                    <?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?><br>
                    <!-- <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>             -->
                </td>
                <td align="center">
                    <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?><br>
                    <?php 
                        $class_id = $this->db->get_where('enroll' , array(
                            'member_id' => $row['member_id'],
                                'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
                        ))->row()->class_id;
                        echo get_phrase('class') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
                    ?><br>
                </td>
            </tr>
        </table>
        <hr>

        <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <!-- <tr>
                <td align="" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align=""><?php echo $row['amount']; ?></td>
            </tr> -->
            <h4 align="center"><b>Fee Payable</b></h4>

             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('annual_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->annual_charges;  ?></td>
            
           
            </tr>
            
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('medical_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->medical_fee;  ?></td>
            
           
            </tr>
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('utility_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->utility_charges;  ?></td>
            
           
            </tr>
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('tution_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->tution_fee;  ?></td>
            
           
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('examination_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->examination_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('games_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->games_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('computer_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->computer_fee;  ?></td>
            </tr>
            <tr>
                 <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('lab_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->lab_charges;  ?></td>
            
           
            </tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('with_holding_tax'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->with_holding_tax;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('library_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->library_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('art_and_craft_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->art_and_craft_fee;  ?></td>
            </tr>

             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('extra_activity_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->extra_activity_charges;  ?></td>
            </tr>

            <tr>
                <td style="padding-left: 50" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align="center"><?php echo $row['amount']; ?></td>
            </tr>
    
            <tr>
                <td style="padding-left: 50" width="80%"><?php echo get_phrase('paid_amount'); ?> :</td>
                <td align="center" width="80%"><?php echo $row['amount_paid']; ?></td>
            </tr>
            <tr>
                <td style="padding-left: 50"><p>Payable After Due Date : </p></td> 
                <td align="center"><p>500</p></td>
         
            </tr>
           
            <?php if ($row['due'] != 0):?>
            <tr>
                <td align="right" width="80%"><h4><?php echo get_phrase('due'); ?> :</h4></td>
                <td align="right"><h4><?php echo $row['due']; ?></h4></td>
            </tr>
            <?php endif;?>
        </table>

        <hr>

        <!-- payment history -->
       <center> <h4><?php echo get_phrase('payment_history'); ?></h4></center>
        <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <thead>
                <tr>
                    <th><?php echo get_phrase('date'); ?></th>
                    <th><?php echo get_phrase('amount'); ?></th>
                    <th><?php echo get_phrase('method'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $payment_history = $this->db->get_where('payment', array('invoice_id' => $row['invoice_id']))->result_array();
                foreach ($payment_history as $row2):
                    ?>
                    <tr>
                        <td><?php echo date("d M, Y", $row2['timestamp']); ?></td>
                        <td><?php echo $row2['amount']; ?></td>
                        <td>
                            <?php 
                                if ($row2['method'] == 1)
                                    echo get_phrase('cash');
                                if ($row2['method'] == 2)
                                    echo get_phrase('check');
                                if ($row2['method'] == 3)
                                    echo get_phrase('card');
                                if ($row2['method'] == 'paypal')
                                    echo 'paypal';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tbody>

        </table>


     </td> <td>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp </td><td>  


<table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <tr>
                <td>
              
                    <h5 align="right" style="padding-right:20 "><?php echo get_phrase('creation_date'); ?> : <?php echo date('d, M,Y', $row['creation_timestamp']);?></h5>
                   <!--  <h5> <?php echo get_phrase('Account no : 9875564442763');  ?></h5> -->
                    <h2 align="center" style="border-collapse:collapse;" ><strong> Hayat-ul-Islam Girls College</strong> </h2>
                    <center><img src="\management\assets\images\hayat.png" width="50" height="50" ></center>
                    <h5 style="padding-left: 50"><?php echo get_phrase('title'); ?> : <?php echo $row['title'];?></h5>
                    <h5 style="padding-left: 50"><?php echo get_phrase('description'); ?> : <?php echo $row['description'];?></h5>


 <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <tr>
               <td ><center><h4><strong>member Name</strong> </h4></center></td>
                <td ><center><h4><strong>Father Name</strong> </h4></center></td>
            </tr>

            <tr>
                <td>
                  <h4 align="center">   <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?> </h4>
                     
                    <!-- <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>             -->
                </td>
                <td>  <h4 align="center">  <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->father_name; ?></td> </h4>
            </tr>



                 
                
                  <h4 align="right" style="padding-right:20 "><?php echo get_phrase('payable_due_date'); ?> 
                  
                  <?php
                    $dt = date('d M,Y', $row['creation_timestamp']);

                    $month = date("m", $row['creation_timestamp']);
                    $month++;
                    $year = date("Y" , $row['creation_timestamp']);

                    echo  date("Y-m-05", mktime(0,0,0,$month, $dt)); 
                           ?>
                  
                </td>
                
            </tr>
            <!-- <tr>
            <td ><?php echo get_phrase('member_name'); ?></td>
          <td>
          
            <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?><br>
            </td>
            </tr> -->
            <!-- <tr>
            <td align="" width="80%"><?php echo get_phrase('father_name'); ?> :</td>
            <td> <?php  echo get_phrase('') . ' ' . $this->db->get_where('member', array('member_id' => $member_id))->row()->father_name;  ?></td>
            </tr> -->
        </table>
        <hr>
        <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <tr>
                <td ><center><h5><strong>Payment to</strong> </h5></center></td>
                <td ><center><h5><strong>Bill to</strong> </h5></center></td>
            </tr>

            <tr>
                <td align="center">
                    <?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?><br>
                    <?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?><br>
                    <!-- <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>             -->
                </td>
                <td align="center">
                    <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?><br>
                    <?php 
                        $class_id = $this->db->get_where('enroll' , array(
                            'member_id' => $row['member_id'],
                                'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
                        ))->row()->class_id;
                        echo get_phrase('class') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
                    ?><br>
                </td>
            </tr>
        </table>
        <hr>

        <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <!-- <tr>
                <td align="" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align=""><?php echo $row['amount']; ?></td>
            </tr> -->
            <h4 align="center"><b>Fee Payable</b></h4>

             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('annual_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->annual_charges;  ?></td>
            
           
            </tr>
            
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('medical_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->medical_fee;  ?></td>
            
           
            </tr>
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('utility_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->utility_charges;  ?></td>
            
           
            </tr>
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('tution_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->tution_fee;  ?></td>
            
           
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('examination_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->examination_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('games_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->games_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('computer_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->computer_fee;  ?></td>
            </tr>
            <tr>
                 <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('lab_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->lab_charges;  ?></td>
            
           
            </tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('with_holding_tax'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->with_holding_tax;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('library_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->library_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('art_and_craft_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->art_and_craft_fee;  ?></td>
            </tr>

             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('extra_activity_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->extra_activity_charges;  ?></td>
            </tr>

            <tr>
                <td style="padding-left: 50" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align="center"><?php echo $row['amount']; ?></td>
            </tr>
    
            <tr>
                <td style="padding-left: 50" width="80%"><?php echo get_phrase('paid_amount'); ?> :</td>
                <td align="center" width="80%"><?php echo $row['amount_paid']; ?></td>
            </tr>
            <tr>
                <td style="padding-left: 50"><p>Payable After Due Date : </p></td> 
                <td align="center"><p>500</p></td>
         
            </tr>
           
            <?php if ($row['due'] != 0):?>
            <tr>
                <td align="right" width="80%"><h4><?php echo get_phrase('due'); ?> :</h4></td>
                <td align="right"><h4><?php echo $row['due']; ?></h4></td>
            </tr>
            <?php endif;?>
        </table>

        <hr>

        <!-- payment history -->
       <center> <h4><?php echo get_phrase('payment_history'); ?></h4></center>
        <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <thead>
                <tr>
                    <th><?php echo get_phrase('date'); ?></th>
                    <th><?php echo get_phrase('amount'); ?></th>
                    <th><?php echo get_phrase('method'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $payment_history = $this->db->get_where('payment', array('invoice_id' => $row['invoice_id']))->result_array();
                foreach ($payment_history as $row2):
                    ?>
                    <tr>
                        <td><?php echo date("d M, Y", $row2['timestamp']); ?></td>
                        <td><?php echo $row2['amount']; ?></td>
                        <td>
                            <?php 
                                if ($row2['method'] == 1)
                                    echo get_phrase('cash');
                                if ($row2['method'] == 2)
                                    echo get_phrase('check');
                                if ($row2['method'] == 3)
                                    echo get_phrase('card');
                                if ($row2['method'] == 'paypal')
                                    echo 'paypal';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tbody>

        </table>

     </td><td>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp </td>
     <td>

<table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <tr>
                <td>
              
                    <h5 align="right" style="padding-right:20 "><?php echo get_phrase('creation_date'); ?> : <?php echo date('d, M,Y', $row['creation_timestamp']);?></h5>
                   <!--  <h5> <?php echo get_phrase('Account no : 9875564442763');  ?></h5> -->
                    <h2 align="center" style="border-collapse:collapse;" ><strong> Hayat-ul-Islam Girls College</strong> </h2>
                    <center><img src="\management\assets\images\hayat.png" width="50" height="50" ></center>
                    <h5 style="padding-left: 50"><?php echo get_phrase('title'); ?> : <?php echo $row['title'];?></h5>
                    <h5 style="padding-left: 50"><?php echo get_phrase('description'); ?> : <?php echo $row['description'];?></h5>


 <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <tr>
               <td ><center><h4><strong>member Name</strong> </h4></center></td>
                <td ><center><h4><strong>Father Name</strong> </h4></center></td>
            </tr>

            <tr>
                <td>
                  <h4 align="center">   <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?> </h4>
                     
                    <!-- <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>             -->
                </td>
                <td>  <h4 align="center">  <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->father_name; ?></td> </h4>
            </tr>



                 
                
                  <h4 align="right" style="padding-right:20 "><?php echo get_phrase('payable_due_date'); ?> 
                  
                  <?php
                    $dt = date('d M,Y', $row['creation_timestamp']);

                    $month = date("m", $row['creation_timestamp']);
                    $month++;
                    $year = date("Y" , $row['creation_timestamp']);

                    echo  date("Y-m-05", mktime(0,0,0,$month, $dt)); 
                           ?>
                  
                </td>
                
            </tr>
            <!-- <tr>
            <td ><?php echo get_phrase('member_name'); ?></td>
          <td>
          
            <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?><br>
            </td>
            </tr> -->
            <!-- <tr>
            <td align="" width="80%"><?php echo get_phrase('father_name'); ?> :</td>
            <td> <?php  echo get_phrase('') . ' ' . $this->db->get_where('member', array('member_id' => $member_id))->row()->father_name;  ?></td>
            </tr> -->
        </table>
        <hr>
        <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <tr>
                <td ><center><h5><strong>Payment to</strong> </h5></center></td>
                <td ><center><h5><strong>Bill to</strong> </h5></center></td>
            </tr>

            <tr>
                <td align="center">
                    <?php echo $this->db->get_where('settings', array('type' => 'system_name'))->row()->description; ?><br>
                    <?php echo $this->db->get_where('settings', array('type' => 'address'))->row()->description; ?><br>
                    <!-- <?php echo $this->db->get_where('settings', array('type' => 'phone'))->row()->description; ?><br>             -->
                </td>
                <td align="center">
                    <?php echo $this->db->get_where('member', array('member_id' => $row['member_id']))->row()->name; ?><br>
                    <?php 
                        $class_id = $this->db->get_where('enroll' , array(
                            'member_id' => $row['member_id'],
                                'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
                        ))->row()->class_id;
                        echo get_phrase('class') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
                    ?><br>
                </td>
            </tr>
        </table>
        <hr>

        <table  class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">    
            <!-- <tr>
                <td align="" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align=""><?php echo $row['amount']; ?></td>
            </tr> -->
            <h4 align="center"><b>Fee Payable</b></h4>

             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('annual_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->annual_charges;  ?></td>
            
           
            </tr>
            
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('medical_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->medical_fee;  ?></td>
            
           
            </tr>
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('utility_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->utility_charges;  ?></td>
            
           
            </tr>
             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('tution_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->tution_fee;  ?></td>
            
           
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('examination_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->examination_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('games_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->games_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('computer_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->computer_fee;  ?></td>
            </tr>
            <tr>
                 <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('lab_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->lab_charges;  ?></td>
            
           
            </tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('with_holding_tax'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->with_holding_tax;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('library_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->library_fee;  ?></td>
            </tr>
            <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('art_and_craft_fee'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->art_and_craft_fee;  ?></td>
            </tr>

             <tr>
            <td style="padding-left: 50" width="80%"><?php echo get_phrase('extra_activity_charges'); ?> :</td>
            <td align="center"> <?php  echo get_phrase('') . ' ' . $this->db->get_where('class', array('class_id' => $class_id))->row()->extra_activity_charges;  ?></td>
            </tr>

            <tr>
                <td style="padding-left: 50" width="80%"><?php echo get_phrase('total_amount'); ?> :</td>
                <td align="center"><?php echo $row['amount']; ?></td>
            </tr>
    
            <tr>
                <td style="padding-left: 50" width="80%"><?php echo get_phrase('paid_amount'); ?> :</td>
                <td align="center" width="80%"><?php echo $row['amount_paid']; ?></td>
            </tr>
            <tr>
                <td style="padding-left: 50"><p>Payable After Due Date : </p></td> 
                <td align="center"><p>500</p></td>
         
            </tr>
           
            <?php if ($row['due'] != 0):?>
            <tr>
                <td align="right" width="80%"><h4><?php echo get_phrase('due'); ?> :</h4></td>
                <td align="right"><h4><?php echo $row['due']; ?></h4></td>
            </tr>
            <?php endif;?>
        </table>

        <hr>

        <!-- payment history -->
       <center> <h4><?php echo get_phrase('payment_history'); ?></h4></center>
        <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
            <thead>
                <tr>
                    <th><?php echo get_phrase('date'); ?></th>
                    <th><?php echo get_phrase('amount'); ?></th>
                    <th><?php echo get_phrase('method'); ?></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $payment_history = $this->db->get_where('payment', array('invoice_id' => $row['invoice_id']))->result_array();
                foreach ($payment_history as $row2):
                    ?>
                    <tr>
                        <td><?php echo date("d M, Y", $row2['timestamp']); ?></td>
                        <td><?php echo $row2['amount']; ?></td>
                        <td>
                            <?php 
                                if ($row2['method'] == 1)
                                    echo get_phrase('cash');
                                if ($row2['method'] == 2)
                                    echo get_phrase('check');
                                if ($row2['method'] == 3)
                                    echo get_phrase('card');
                                if ($row2['method'] == 'paypal')
                                    echo 'paypal';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
            <tbody>

        </table>
     </td>
 </tr>
        


        </table>
    </div>
</center>
<?php endforeach; ?>


<script type="text/javascript">

    // print invoice function
    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'invoice', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Invoice</title>');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        var is_chrome = Boolean(mywindow.chrome);
        if (is_chrome) {
            setTimeout(function() {
                mywindow.print();
                mywindow.close();

                return true;
            }, 250);
        }
        else {
            mywindow.print();
            mywindow.close();

            return true;
        }

        return true;
    }

</script>