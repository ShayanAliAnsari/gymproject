<?php 
$edit_data		=	$this->db->get_where('class' , array('class_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					<?php echo get_phrase('edit_member');?>
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(site_url('admin/classes/do_update/'.$row['class_id']) , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('numeric_name');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name_numeric" value="<?php echo $row['name_numeric'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('annual_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="annual_charges" value="<?php echo $row['annual_charges'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('tution_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="tution_fee" value="<?php echo $row['tution_fee'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('examination_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="examination_fee" value="<?php echo $row['examination_fee'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('games_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="games_fee" value="<?php echo $row['games_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('lab_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="lab_charges" value="<?php echo $row['lab_charges'];?>"/>
                        </div>
                    </div>
                     

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('library_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="library_fee" value="<?php echo $row['library_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('medical_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="medical_fee" value="<?php echo $row['medical_fee'];?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('computer_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="computer_fee" value="<?php echo $row['computer_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('utility_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="utility_charges" value="<?php echo $row['utility_charges'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('with_holding_tax');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="with_holding_tax" value="<?php echo $row['with_holding_tax'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('art_and_craft_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="art_and_craft_fee" value="<?php echo $row['art_and_craft_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('extra_activity_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="extra_activity_charges" value="<?php echo $row['extra_activity_charges'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('teacher');?></label>
                        <div class="col-sm-5">
                            <select name="teacher_id" class="form-control">
                                <option value=""><?php echo get_phrase('select');?></option>
                                <?php 
                                $teachers = $this->db->get('teacher')->result_array();
                                foreach($teachers as $row2):
                                ?>
                                    <option value="<?php echo $row2['teacher_id'];?>"
                                        <?php if($row['teacher_id'] == $row2['teacher_id'])echo 'selected';?>>
                                            <?php echo $row2['name'];?>
                                                </option>
                                <?php
                                endforeach;
                                ?>
                            </select>
                        </div>
                    </div>
            		<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info"><?php echo get_phrase('edit_class');?></button>
						</div>
					</div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>


