<hr />
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					<?php echo get_phrase('program_list');?>
                    	</a></li>
			<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					<?php echo get_phrase('add_program');?>
                    	</a></li>
		</ul>
    	<!------CONTROL TABS END------>
        
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
				
                <table class="table table-bordered datatable" id="">
                	<thead>
                		<tr>
                    		<th><div>#</div></th>
                    		<th><div><?php echo get_phrase('program_name');?></div></th>
                    		<th><div><?php echo get_phrase('numeric_name');?></div></th>
                            <th><div><?php echo get_phrase('trainer');?></div></th>
                            
                    		<th><div><?php echo get_phrase('options');?></div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($classes as $row):?>
                        <tr>
                            <td><?php echo $count++;?></td>
							<td><?php echo $row['name'];?></td>
							<td><?php echo $row['name_numeric'];?></td>
							<td>
                                <?php
                                    if($row['teacher_id'] != '' || $row['teacher_id'] != 0) 
                                        echo $this->crud_model->get_type_name_by_id('teacher',$row['teacher_id']);
                                ?>
                            </td>
							<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_edit_class/'.$row['class_id']);?>');">
                                            <i class="entypo-pencil"></i>
                                                <?php echo get_phrase('edit');?>
                                            </a>
                                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo site_url('admin/classes/delete/'.$row['class_id']);?>');">
                                            <i class="entypo-trash"></i>
                                                <?php echo get_phrase('delete');?>
                                            </a>
                                                    </li>
                                </ul>
                            </div>
        					</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(site_url('admin/classes/create') , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                        <div class="padded">
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('name');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('name_numeric');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name_numeric"/>
                                </div>
                            </div>


         
             <!-----triobyte add class update ------->
   <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('annual_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="annual_charges" value="<?php echo $row['annual_charges'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('tution_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="tution_fee" value="<?php echo $row['tution_fee'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('examination_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="examination_fee" value="<?php echo $row['examination_fee'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('games_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="games_fee" value="<?php echo $row['games_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('lab_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="lab_charges" value="<?php echo $row['lab_charges'];?>"/>
                        </div>
                    </div>
                   

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('library_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="library_fee" value="<?php echo $row['library_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('medical_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="medical_fee" value="<?php echo $row['medical_fee'];?>"/>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('computer_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="computer_fee" value="<?php echo $row['computer_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('utility_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="utility_charges" value="<?php echo $row['utility_charges'];?>"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('with_holding_tax');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="with_holding_tax" value="<?php echo $row['with_holding_tax'];?>"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('art_and_craft_fee');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="art_and_craft_fee" value="<?php echo $row['art_and_craft_fee'];?>"/>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('extra_activity_charges');?></label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="extra_activity_charges" value="<?php echo $row['extra_activity_charges'];?>"/>
                        </div>
                    </div>

                    <!---update close --->

                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('teacher');?></label>
                                <div class="col-sm-5">
                                    <select name="teacher_id" class="form-control select2" style="width:100%;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                        <option value=""><?php echo get_phrase('select_teacher');?></option>
                                    	<?php 
										$teachers = $this->db->get('teacher')->result_array();
										foreach($teachers as $row):
										?>
                                    	<option value="<?php echo $row['teacher_id'];?>"><?php echo $row['name'];?></option>
                                        <?php
										endforeach;
										?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info"><?php echo get_phrase('add_class');?></button>
                              </div>
							</div>
                    </form>                
                </div>                
			</div>
			<!----CREATION FORM ENDS-->
		</div>
	</div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable();
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>